import {getRepository, getConnection} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { Store } from "../models/Store";
import { StoreAddress } from "../models/StoreAddress";
import { RatingHistory } from "../models/RatingHistory";
import { Classification } from "../models/Classification";
import { StoreNonGreener } from "../models/StoreNonGreener";
import { Sector } from "../models/Sector";
import getDistanceFromLatLonInM from '../helpers/common';
import { RatingHelper } from "../helpers/rating";
import { Helper as GlobalHelper } from "../helpers/helper";

const geolib = require('geolib');

export class TransactionController {

    static past3MonthsActivity = async(request: Request, response: Response, next: NextFunction) => {
        const type = request.query.type ? request.query.type : 'greenr';
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const today_90 = GlobalHelper.formatDate(new Date());
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        let data = [];
        let data2 = [];

        if(type === 'not_greenr'){
            data =  await getRepository(RatingHistory)
                .createQueryBuilder("rate")
                .where("rate.calculation_date BETWEEN '"+fromDate_90+"' and '"+today_90+"' and rate.user_id = :user_id and rate.store_name != '' and rate.for_recommendation IS TRUE", { user_id: loginName})
                .distinctOn(["rate.id"])
                .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
                .leftJoinAndSelect(StoreNonGreener, "store", "LOWER(store.name) = LOWER(rate.store_name)")
                .groupBy("store.name")
                .addGroupBy("rate.id")
                .addGroupBy("class.id")
                .addGroupBy("store.id")
                .orderBy("rate.id")
                .addOrderBy("rate.rating", "DESC")
                .getRawMany();

            data2 = [];
        }
        else
        {
            data =  await getRepository(RatingHistory)
                .createQueryBuilder("rate")
                .where("rate.calculation_date BETWEEN '"+fromDate_90+"' and '"+today_90+"' and rate.user_id = :user_id and rate.store_name != '' and rate.after_link IS TRUE and rate.for_recommendation IS FALSE", { user_id: loginName})
                .distinctOn(["rate.id"])
                .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
                .innerJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(rate.store_name)")
                .leftJoinAndSelect("store.subsectors", "subsector")
                .orderBy("rate.id")
                .addOrderBy("rate.rating", "DESC")
                .getRawMany();
                
            data2 =  await getRepository(RatingHistory)
                .createQueryBuilder("rate")
                .distinctOn(["rate.id"])
                .where("rate.calculation_date BETWEEN '"+fromDate_90+"' and '"+today_90+"' and rate.user_id = :user_id and rate.store_name != '' and rate.after_link IS FALSE and rate.for_recommendation IS FALSE", { user_id: loginName})
                .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
                .innerJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(rate.store_name)")
                .leftJoinAndSelect("store.subsectors", "subsector")
                .orderBy("rate.id")
                .addOrderBy("rate.rating", "DESC")
                .getRawMany();
        }

        let final_ar = [];
        let final_ar2 = [];

        for(let i = 0; i < data.length; i++) {
            const logo = data[i].store_logo;
            let store_name = data[i].store_name;
            let store_id = '';

            // classification lookup
            let rating = (data[i].rate_spend * data[i].class_emission_factor_gc02 / 1000);
            rating = (rating*(365/90)/40000*10);
            
            if (type !== 'not_greenr'){ 
                store_id = data[i].store_id;
            }else{
                if(data[i].store_name === null || data[i].store_name === undefined) store_name = data[i].rate_store_name;
                store_name = store_name.replace(/(^\w|\s\w)/g, m => m.toUpperCase());

                // skip if greener store
                let count = await getRepository(Store).createQueryBuilder("store") .where("LOWER(store.name) = LOWER(:name)", { name: store_name }).getCount();
                if (count !== 0) continue;
            }

            final_ar.push({
                'store_id': store_id,
                'name': store_name,
                'sector_id': data[i].store_sectorId ? data[i].store_sectorId : data[i].class_sector_id,
                'subsector_id': (data[i].subsector_id !== undefined) ? data[i].subsector_id : '',
                'rating': rating,
                'logo': logo
            });
        }

        final_ar = RatingHelper.findOcc(final_ar, 'name');

        for(let i = 0; i < data2.length; i++) {
            let logo = data2[i].store_logo;
            let store_name = data2[i].store_name;
            let store_id = '';

            // classification lookup
            let rating = (data2[i].rate_spend * data2[i].class_emission_factor_gc02 / 1000);
            rating = (rating*(365/90)/40000*10);
            
            if (type !== 'not_greenr'){ 
                store_id = data2[i].store_id;
            }else{
                if(data[i].store_name === null || data[i].store_name === undefined) store_name = data[i].rate_store_name;
                store_name = store_name.replace(/(^\w|\s\w)/g, m => m.toUpperCase());

                // skip if greener store
                let count = await getRepository(Store).createQueryBuilder("store") .where("LOWER(store.name) = LOWER(:name)", { name: store_name }).getCount();
                if (count !== 0) continue;
            }

            final_ar2.push({
                'store_id': store_id,
                'name': store_name,
                'sector_id': data2[i].store_sectorId ? data2[i].store_sectorId : data2[i].class_sector_id,
                'subsector_id': data2[i].subsector_id ? data2[i].subsector_id : '',
                'rating': rating,
                'logo': logo
            });
        }

        final_ar2 = RatingHelper.findOcc(final_ar2, 'name');

        return response.send([{
            'stores': final_ar,
            'pre_linked': final_ar2
        }]);
    }

    static transactionDetail = async(request: Request, response: Response, next: NextFunction) => {
        const type = request.query.type ? request.query.type : 'greenr';
        const store = request.query.store ? request.query.store : false;
        const sector_id = request.query.sector_id ? request.query.sector_id : false;
        const latitude:any = request.query.latitude ? request.query.latitude : '';
        const longitude:any = request.query.longitude ? request.query.longitude : '';
        const range:any = request.query.range ? request.query.range : 5000;
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        if(type === 'not_greenr'){
            let sector = await getRepository(Sector)
                .createQueryBuilder("sector")
                .where("sector.id = :sector_id", { sector_id: sector_id })
                .getOne();

            let data = await getRepository(StoreAddress)
                .createQueryBuilder("StoreAddress")
                .distinctOn(["StoreAddress.address"])
                .leftJoinAndSelect("StoreAddress.store", "store")
                .leftJoinAndSelect("store.subsectors", "subsectors")
                .leftJoinAndSelect("subsectors.info", "info")
                .where("store.sectorId = :sector_id", { sector_id: sector_id })
                .orderBy("StoreAddress.address")
                .getMany();

            const nearIds = [];
            const storesData = [];

            for(let i = 0; i < data.length; i++) {
                storesData.push(data[i]);
            }

            for(let b = 0; b < storesData.length; b++) {
                const lat1 = storesData[b].latitude;
                const lon1 = storesData[b].longitude;
                const lat2 = latitude;
                const lon2 = longitude;
    
                const distance_meter = getDistanceFromLatLonInM({ lat1, lon1, lat2, lon2, });
                const distance_in_km = (distance_meter / 1000);
    
                // check if current within the requested kilometer range
                const point = geolib.isPointWithinRadius(
                    { latitude: lat1, longitude: lon1 }, // store address
                    { latitude: lat2, longitude: lon2 },  // current lat/lon
                    range //range in KM default 5000/5km
                );
        
                if(point) {
                    nearIds.push({
                        'id': storesData[b].id,
                        'storeId': storesData[b].storeId,
                        'address': storesData[b].address,
                        'latitude': storesData[b].latitude,
                        'longitude': storesData[b].longitude,
                        'opening_hours': storesData[b].opening_hours,
                        'distance_kilometer': distance_in_km, 
                        'distance_meter': distance_meter,
                        'created_at': storesData[b].created_at,
                        'updated_at': storesData[b].updated_at,
                        "store": storesData[b].store
                    });
                }
            }
    
            const stores = nearIds.sort((a, b) => a.distance_kilometer - b.distance_kilometer);

            return response.send([{
                'sector': sector,
                'stores': stores
            }]);
        }
        else 
        {
            let data =  await getRepository(RatingHistory)
                .createQueryBuilder("rate")
                .where("lower(rate.store_name) = lower(:store) and rate.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and rate.user_id = :user_id and rate.after_link IS TRUE and rate.for_recommendation IS FALSE", { user_id: loginName, store: store})
                .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
                .innerJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(rate.store_name)")
                .leftJoinAndSelect("store.subsectors", "subsector")
                .orderBy("rate.calculation_date", "DESC")
                .getRawMany();

            let final_ar = [];

            for(let i = 0; i < data.length; i++) {
                let logo = data[i].store_logo;
                let store_name = data[i].store_name;
                let store_id = '';
    
                // classification lookup
                let rating = (data[i].rate_spend * data[i].class_emission_factor_gc02 / 1000);
                let kgc02 = rating;
                rating = (rating*(365/90)/40000*10);
                
                if (type !== 'not_greenr'){ 
                    store_id = data[i].store_id;
                }else{
                    if(data[i].store_name === null || data[i].store_name === undefined) store_name = data[i].rate_store_name;
                    store_name = store_name.replace(/(^\w|\s\w)/g, m => m.toUpperCase());
    
                    // skip if greener store
                    // let count = await getRepository(Store).createQueryBuilder("store") .where("LOWER(store.name) = LOWER(:name)", { name: store_name }).getCount();
                    // if (count !== 0) continue;
                }
    
                final_ar.push({
                    'store_id': store_id,
                    'subsector_id': data[i].subsector_id ? data[i].subsector_id : '',
                    'name': store_name,
                    'spend': data[i].rate_spend,
                    'rating': rating,
                    'kgc02': kgc02,
                    'logo': logo,
                    'transaction_date': data[i].rate_calculation_date
                });
            }
    
            // manual sort by rating cause DESC has been override by disticnt initial group by
            final_ar = final_ar.sort((a, b) => b.transaction_date - a.transaction_date);
    
            return response.send([{
                'records': final_ar
            }]);
        }
    }

}