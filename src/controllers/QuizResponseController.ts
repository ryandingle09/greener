import {getRepository, getConnection, Tree} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {QuizResponse} from "../models/QuizResponse";
import {QuizCategory} from "../models/QuizCategory";
import {QuizResponseRating} from "../models/QuizResponseRating";
import { QuizQuestion } from "../models/QuizQuestion";
import { QuizResponseBadge } from "../models/QuizResponseBadge";
// import { Account } from "../models/Account";
// import { RatingHistory } from "../models/RatingHistory";
import { User } from "../models/User";
// import { HashHelper } from "../helpers/hasher";
import * as jwt from "jsonwebtoken";
import { MixpanelHelper } from '../helpers/mixpanel';

const env = process.env;

export class QuizResponseController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(QuizResponse);
        let data = await repo
            .createQueryBuilder("QuizResponse")
            .leftJoinAndSelect("QuizResponse.question", "question")
            .leftJoinAndSelect("QuizResponse.category", "category")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static saveResponse = async(request: Request, response: Response, next: NextFunction) => {
		let repo = getRepository(QuizCategory);
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { responses } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let items = [];
        let catId:any;
		let userRepository = getRepository(User);
        let userInfo = await userRepository.findOneOrFail(userId);

        for(let i in responses) {
            catId = responses[i].categoryId;
            items.push({
                'userId': userId,
                'questionId': responses[i].questionId,
                'quizcategoryId': responses[i].categoryId,
                'answer': responses[i].answer,
                'choiceId': responses[i].choiceId
            });
        }
        
        // remove if there is existing to avoid duplicate answer per question category
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(QuizResponse)
            .where("userId = :user_id and quizcategoryId = :quizcategoryId", { user_id: userId, quizcategoryId: catId })
            .execute();

        // delete previous data for this specific user incase in was resubmitted to avoid duplicates
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(QuizResponseRating)
            .where("userId = :user_id and quizcategoryId = :quizcategoryId", { user_id: userId, quizcategoryId: catId })
            .execute();

        let rate_on_category = 0;
        let rate_on_category_flight = 0;
        let rate_over = 0;
        let rate_over2 = 0;
        let category = await repo.findOneOrFail(catId);

        // get questions for mapping the order
        let repo2 = getRepository(QuizQuestion);
        let data2 = await repo2
            .createQueryBuilder("QuizQuestion")
            .getMany();

        if(category.name === 'Home & Energy'){
            let q1:any;
            let q2:any;
            let q3:any;
            let q4:any;

            // remap order to avoid calculatio errors
            for(let q in data2) {
                if(data2[q].id === items[0].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qa') q1 = Number(items[0].answer);
                    if(code === 'qb') q2 = Number(items[0].answer);
                    if(code === 'qc') q3 = Number(items[0].answer);
                    if(code === 'qd') q4 = Number(items[0].answer);
                }

                if(data2[q].id === items[1].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qa') q1 = Number(items[1].answer);
                    if(code === 'qb') q2 = Number(items[1].answer);
                    if(code === 'qc') q3 = Number(items[1].answer);
                    if(code === 'qd') q4 = Number(items[1].answer);
                }

                if(data2[q].id === items[2].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qa') q1 = Number(items[2].answer);
                    if(code === 'qb') q2 = Number(items[2].answer);
                    if(code === 'qc') q3 = Number(items[2].answer);
                    if(code === 'qd') q4 = Number(items[2].answer);
                }

                if(data2[q].id === items[3].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qa') q1 = Number(items[3].answer);
                    if(code === 'qb') q2 = Number(items[3].answer);
                    if(code === 'qc') q3 = Number(items[3].answer);
                    if(code === 'qd') q4 = Number(items[3].answer);
                }
            }

            let q1_l = q1;

            if(q1 === 1) {
                q2 = 0;
            }
            if(q1 > 5) {
                q1_l = 4;
            }

            // formula = B6+((B79-B80-1)*B7)+(B80*B8)
            let household_size = Number( 1.0 + ((q1 - q2 - 1) * 0.5) + (q2 * 0.3) );

            // formula = INDEX(B13:B17,MATCH(B79,D13:D17,1))*SUMPRODUCT(B82:B85,B21:B24)
            let energylookup = [3675.5164344162,5773.2949548644,6843.86547911036,8228.31860661188,9270.81609543409];
            let energysourcelookup = [0,0.214117614379118,0.586382718108569,0.95864782183802];
            let electricity_footprint = Number(energylookup[q1_l - 1] * (1.0 * energysourcelookup[q3]));
            console.log(Number(energylookup[q1_l - 1]));
            console.log(energysourcelookup[q3]);

            //formula = INDEX(C13:C17,MATCH(B79,D13:D17,1))*B28*B86
            let gaslookup = [13791.2036313819,27013.167043969,29093.934134661,33519.855015408,37999.1846749635];
            let gas_footprint = (Number(gaslookup[q1_l - 1]) * (51.4+0.1+0.03) / 1000) * q4;

            console.log('household_size:', household_size);
            console.log('electricity_footprint:', electricity_footprint);
            console.log('gas_footprint:', gas_footprint);

            //formula = SUM(G80:G81)/1000
            rate_on_category = (electricity_footprint + gas_footprint) / 1000 / household_size;
            rate_over = 2.0;
            rate_over2 = 8.0;
            console.log('rate_on_category:', rate_on_category);
            rate_on_category = rate_over * ((rate_over2 - rate_on_category)/rate_over2);
            console.log('rate_on_category:', rate_on_category);

            let calc_rating = electricity_footprint + gas_footprint;

            // insert calculated
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseRating)
                .values([ {'userId': userId, 'quizcategoryId': catId, 'rating': rate_on_category, 'over': rate_over, 'calc_rating': calc_rating}])
                .execute();

            let mixData = {};
            mixData['Equivalised household size'] = household_size;
            mixData['Gas'] = (q4 === 1) ? true : false;
            MixpanelHelper.set(userInfo.encrypted_id, mixData);
        }
        
        if(category.name === 'Diet'){
            // Formula = SUMPRODUCT(B32:B35,B91:B94)
            let diet_lookup = [2420,1500,1430,1160];
            let diet_footprint = Number(1 * diet_lookup[Number(items[0].answer)]);

            // Formula = SUM(G90)/1000
            rate_on_category = (diet_footprint / 1000);
            rate_over = 1.25;
            rate_over2 = 5.0;
            console.log('rate_on_category:', rate_on_category);
            rate_on_category = rate_over * ((rate_over2 - rate_on_category)/rate_over2);
            console.log('rate_on_category:', rate_on_category);

            let calc_rating = diet_footprint;

                // insert calculated
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseRating)
                .values([ {'userId': userId, 'quizcategoryId': catId, 'rating': rate_on_category, 'over': rate_over, 'calc_rating': calc_rating}])
                .execute();
        }

        if(category.name === 'Travel'){
            // expected order
            let q1:any;
            let q2:any;
            let q3:any;
            let q4:any;
            let q5:any;
            let q6:any;
            let q7:any;

            // remap order to avoid calculatio errors
            for(let q in data2) {
                if(data2[q].id === items[0].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[0].answer);
                    if(code === 'qg') q2 = Number(items[0].answer);
                    if(code === 'qh') q3 = Number(items[0].answer);
                    if(code === 'qi') q4 = Number(items[0].answer);
                    if(code === 'qj') q5 = Number(items[0].answer);
                    if(code === 'qk') q6 = Number(items[0].answer);
                    if(code === 'ql') q7 = Number(items[0].answer);
                }

                if(data2[q].id === items[1].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[1].answer);
                    if(code === 'qg') q2 = Number(items[1].answer);
                    if(code === 'qh') q3 = Number(items[1].answer);
                    if(code === 'qi') q4 = Number(items[1].answer);
                    if(code === 'qj') q5 = Number(items[1].answer);
                    if(code === 'qk') q6 = Number(items[1].answer);
                    if(code === 'ql') q7 = Number(items[1].answer);
                }

                if(data2[q].id === items[2].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[2].answer);
                    if(code === 'qg') q2 = Number(items[2].answer);
                    if(code === 'qh') q3 = Number(items[2].answer);
                    if(code === 'qi') q4 = Number(items[2].answer);
                    if(code === 'qj') q5 = Number(items[2].answer);
                    if(code === 'qk') q6 = Number(items[2].answer);
                    if(code === 'ql') q7 = Number(items[2].answer);
                }

                if(data2[q].id === items[3].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[3].answer);
                    if(code === 'qg') q2 = Number(items[3].answer);
                    if(code === 'qh') q3 = Number(items[3].answer);
                    if(code === 'qi') q4 = Number(items[3].answer);
                    if(code === 'qj') q5 = Number(items[3].answer);
                    if(code === 'qk') q6 = Number(items[3].answer);
                    if(code === 'ql') q7 = Number(items[3].answer);
                }

                if(data2[q].id === items[4].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[4].answer);
                    if(code === 'qg') q2 = Number(items[4].answer);
                    if(code === 'qh') q3 = Number(items[4].answer);
                    if(code === 'qi') q4 = Number(items[4].answer);
                    if(code === 'qj') q5 = Number(items[4].answer);
                    if(code === 'qk') q6 = Number(items[4].answer);
                    if(code === 'ql') q7 = Number(items[4].answer);
                }

                if(data2[q].id === items[5].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[5].answer);
                    if(code === 'qg') q2 = Number(items[5].answer);
                    if(code === 'qh') q3 = Number(items[5].answer);
                    if(code === 'qi') q4 = Number(items[5].answer);
                    if(code === 'qj') q5 = Number(items[5].answer);
                    if(code === 'qk') q6 = Number(items[5].answer);
                    if(code === 'ql') q7 = Number(items[5].answer);
                }

                if(data2[q].id === items[6].questionId){
                    let code = data2[q].code.toString();

                    if(code === 'qf') q1 = Number(items[6].answer);
                    if(code === 'qg') q2 = Number(items[6].answer);
                    if(code === 'qh') q3 = Number(items[6].answer);
                    if(code === 'qi') q4 = Number(items[6].answer);
                    if(code === 'qj') q5 = Number(items[6].answer);
                    if(code === 'qk') q6 = Number(items[6].answer);
                    if(code === 'ql') q7 = Number(items[6].answer);
                }
            }

            // travel
            // Formula = (SUMPRODUCT(B99:B101,B39:B41)/1000*B43*B102*52)
            let car_type_lookup = [180,100,0];
            let cla = car_type_lookup[q2];

            // =(SUMPRODUCT(B99:B101,B39:B41)/1000*B43*B102*52)
            let vehicle_emission = Number(1 * cla) / 1000 * 50 * q1 * 52;
            console.log('vehicle_emission:', Number(vehicle_emission));

            // Formula = B46/1000*B48*2*B103*52
            let public_transit_emission = Number(15 /1000 * 10 * 2 * q3 * 52);
            console.log('public_transit_emission:', Number(public_transit_emission));

            // Formula = SUM(G98:G99)/1000
            rate_on_category = (Number(vehicle_emission) + Number(public_transit_emission)) / 1000;
            console.log('rate_on_category:', Number(rate_on_category));
            // flights
            // Formula = SUMPRODUCT(B53:B56,B107:B110)*2/(1-B60)
            let flight_lookup = [70.4,211.5,409.1,(666.3 + (745.1 + 355.2)) / 2];
            console.log('flight_lookup: ', flight_lookup);

            let flight_emission =  (Number(flight_lookup[0] * q4) + Number(flight_lookup[1] * q5) + Number(flight_lookup[2] * q6) + Number(flight_lookup[3] * q7)) * 2 / (1 - 0.50);
            console.log('flight_emission: ', flight_emission);

            // Formula = SUM(G107)/1000
            rate_on_category_flight = Number(flight_emission) / 1000;
            console.log('rate_on_category_flight:', Number(rate_on_category_flight));

            // rate_on_category = rate_on_category + rate_on_category_flight;
            rate_over = 1.5;
            rate_over2 = 6.0;
            rate_on_category = (vehicle_emission + public_transit_emission + flight_emission) / 1000;
            console.log('rate_on_category:', rate_on_category);
            rate_on_category = rate_over * ((rate_over2 - rate_on_category)/rate_over2);

            let transport = rate_on_category;
            let calc_rating = vehicle_emission + public_transit_emission + flight_emission;

            if(rate_on_category < 0) rate_on_category = 0;

            // insert calculated
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseRating)
                .values([ {'userId': userId, 'quizcategoryId': catId, 'rating': rate_on_category, 'over': rate_over, 'transport': transport,'flight': rate_on_category_flight, 'calc_rating': calc_rating}])
                .execute();
        }

        if(category.name === 'Lifestyle'){
            // no calculation needed - display badges only

            // insert calculated
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseRating)
                .values([ {'badge_count': items.length, 'userId': userId, 'quizcategoryId': catId, 'rating': rate_on_category, 'over': rate_over}])
                .execute();
        }

        if(category.name === 'Other daily spend & bills'){
            // Formula = PRODUCT(B66:C66,52)/1000
            // Formula = PRODUCT(B67:C67,52)/1000
            let is_average = false;
            let average_spend_high = ((316.35*.50) * 375 * 52)/1000;
            let average_spend_low = (((316.35*1.5) * 2) * 375 * 52)/1000;

            rate_on_category = average_spend_high;

            if(parseInt(items[0].answer) === 1) {
                is_average = true;
                rate_on_category = average_spend_low;
            }
            rate_over = 5.25;
            rate_over2 = 21.0;

            let calc_rating = rate_on_category;
            
            console.log('rate_on_category:', Number(rate_on_category));

            average_spend_low = (average_spend_low / 1000);
            average_spend_high = (average_spend_high / 1000);
            console.log('average_spend_high: ', average_spend_high);
            console.log('average_spend_low: ', average_spend_low);

            rate_on_category = rate_over * ((rate_over2 - (rate_on_category / 1000))/rate_over2);
            console.log('rate_on_category:', rate_on_category);

            average_spend_low = rate_over * ((rate_over2 - average_spend_low)/rate_over2);
            average_spend_high = rate_over * ((rate_over2 - average_spend_high)/rate_over2);
            
            let repoR = getRepository(QuizResponseRating);
            let travel_r = await repoR.createQueryBuilder("rating").leftJoinAndSelect("rating.category","category").where("category.name = :name and rating.userId = :userId", { name: "Travel", userId: userId }).getOne();
            console.log(travel_r.rating);

            if(travel_r.rating === 0.0){
                rate_on_category += travel_r.transport;
                average_spend_low += travel_r.transport;
                average_spend_high += travel_r.transport;

                console.log("Detected negative value! Need to add to this category!");
            }

            // insert calculated
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseRating)
                .values([ {'userId': userId, 'quizcategoryId': catId, 'rating': rate_on_category, 'high_rating': average_spend_high, 'low_rating': average_spend_low, 'over': rate_over, 'calc_rating': calc_rating, 'is_average': is_average}])
                .execute();
        }

        try {
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponse)
                .values(items)
                .execute();

            let mixData = {};
            let qrs = getRepository(QuizResponse);
            let res = await qrs.createQueryBuilder("QuizResponse")
                .leftJoinAndSelect("QuizResponse.category", "category")
                .leftJoinAndSelect("QuizResponse.question", "question")
                .leftJoinAndSelect("QuizResponse.choice", "choice")
                .where("QuizResponse.userId = :userId", { userId: userId })
                .getMany();

            for(let r in res) {
                if(res[r].question.code === 'qa') {
                    mixData['Household size'] = res[r].answer;
                }
                if(res[r].question.code === 'qb') {
                    mixData['Children'] = res[r].answer;
                }
                if(res[r].question.code === 'qc') {
                    mixData['Energy type'] = res[r].choice.title;
                }
                if(res[r].question.code === 'qe') {
                    mixData['Diet'] = res[r].choice.title;
                }
                if(res[r].question.code === 'qf') {
                    mixData['Car hours'] = res[r].answer;
                }
                if(res[r].question.code === 'qg') {
                    mixData['Car type'] = res[r].choice.title;
                }
                if(res[r].question.code === 'qb') {
                    mixData['Public transport days'] = res[r].answer;
                }
                if(res[r].question.code === 'qi') {
                    mixData['Flights domestic short'] = res[r].answer;
                }
                if(res[r].question.code === 'qj') {
                    mixData['Flights domestic long'] = res[r].answer;
                }
                if(res[r].question.code === 'qk') {
                    mixData['Flights international short'] = res[r].answer;
                }
                if(res[r].question.code === 'ql') {
                    mixData['Flights international long'] = res[r].answer;
                }
                if(res[r].question.code === 'qm') {
                    if(res[r].choice.value === '0') {
                        mixData['Reusable cup'] = true;
                    }
                    if(res[r].choice.value === '1') {
                        mixData['Reusable bags'] = true;
                    }
                    if(res[r].choice.value === '2') {
                        mixData['Home compost'] = true;
                    }
                    if(res[r].choice.value === '3') {
                        mixData['Recycling bins'] = true;
                    }
                }
            }
            
            MixpanelHelper.set(userInfo.encrypted_id, mixData);    
        } catch (e) {
            return response.sendStatus(422).send({'error': 'The response id might be existing already!', 'message': e});
        }

        return response.send({
            'score_rating': rate_on_category.toFixed(2),
            'score_over': rate_over
        });
    }

    static rating = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(QuizResponseRating);
        let data = await repo
            .createQueryBuilder("QuizResponseRating")
            .leftJoinAndSelect("QuizResponseRating.category", "category")
            .where("QuizResponseRating.userId = :userId", {userId: userId})
            .orderBy("category.sort_order","ASC")
            .getMany();

        let green_l = 0;
        let green_h = 0;
        let energy = 0;

        for(let i in data) {
            if(data[i].category.name === 'Home & Energy'){
                energy = data[i].calc_rating;
            }

            if(data[i].category.name === 'Other daily spend & bills'){
                green_h += data[i].high_rating;
            }
            else
            {
                green_h += data[i].rating;
            }

            green_l += data[i].rating;
        }

        green_l = Math.max(0, green_l);
        green_h = Math.max(0, green_h);

        let gray = (10 - green_l);

        return response.send({
            'break_down': data,
            'green': green_l.toFixed(2),
            'gray': gray.toFixed(2),
            'high': green_h.toFixed(2),
            'low': green_l.toFixed(2),
        });
    }

    static saveEarnedBadge = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { badges } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let items = [];
        let catId:any;

        for(let i in badges) {
            catId = badges[i].categoryId;
            items.push({
                'userId': userId,
                'questionId': badges[i].questionId,
                'quizcategoryId': badges[i].categoryId,
                'choiceId': badges[i].choiceId,
                'badge': badges[i].badge
            });
        }
        
        // remove if there is existing to avoid duplicate answer per question category
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(QuizResponseBadge)
            .where("userId = :user_id and quizcategoryId = :quizcategoryId", { user_id: userId, quizcategoryId: catId })
            .execute();

        try {
            await getConnection()
                .createQueryBuilder()
                .insert()
                .into(QuizResponseBadge)
                .values(items)
                .execute();
        } catch (e) {
            return response.sendStatus(422).send({'error': 'The badge might be existing already!', 'message': e});
        }

        return response.send({'message': 'Successfully saved!'});
    }

    static badges = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(QuizResponseBadge);
        let data = await repo
            .createQueryBuilder("QuizResponseBadge")
            .where("QuizResponseBadge.userId = :userId", {userId: userId})
            .getMany();

        return response.send(data);
    }
}