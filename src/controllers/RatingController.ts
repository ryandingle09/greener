import {getRepository, getConnection} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { RatingHistory } from "../models/RatingHistory";
import { Store } from "../models/Store";
import { Classification } from "../models/Classification";
import { StoreNonGreener } from "../models/StoreNonGreener";
import { RatingHelper } from "../helpers/rating";
import { Helper as GlobalHelper } from "../helpers/helper";

export class RatingController {

    static getHaloCalculation = async(request: Request, response: Response, next: NextFunction) => {
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        try {
            let halo = await RatingHelper.ThreeMonths(loginName);
            let halo_7 = await RatingHelper.ThreeMonths(loginName);

            return response.send({
                "halo_90": halo.rating.green,
                'halo_7': halo.rating.green - halo_7.rating.green,
                'trend': '',
                'tc02': ''
            });
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."});
        }
    }

    static getImpactRating = async(request: Request, response: Response, next: NextFunction) => {
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test)loginName = user_id_test;

        try {
            let data = await RatingHelper.ThreeMonths(loginName, userId);
            return response.send(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."});
        }
    }

    static pastWeekActivity = async(request: Request, response: Response, next: NextFunction) => {
        const type = request.query.type ? request.query.type : 'greenr';
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const fromDate = GlobalHelper.getDateRange(6).fromDate;
        const toDate = GlobalHelper.getDateRange(6).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        let data =  await getRepository(RatingHistory)
            .createQueryBuilder("rate")
            .distinctOn(["rate.store_name"])
            .select("rate.store_name, store.name, COUNT(rate.store_name) as purchase_count, SUM(rate.rating) as rating, class.sector_id, rate.category_code")
            .where("rate.calculation_date BETWEEN '"+fromDate+"' and '"+toDate+"' and rate.user_id = :user_id and rate.store_name != ''", { user_id: loginName})
            .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
            .innerJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(rate.store_name)")
            .groupBy("rate.store_name")
            .addGroupBy("class.sector_id")
            .addGroupBy("class.id")
            .addGroupBy("store.id")
            .addGroupBy("rate.category_code")
            .orderBy("rate.store_name", "DESC")
            .addOrderBy("rating", "DESC")
            .getRawMany();

        if(type === 'not_greenr'){
            data =  await getRepository(RatingHistory)
                .createQueryBuilder("rate")
                .distinctOn(["rate.store_name"])
                .select("rate.store_name, store.name, COUNT(rate.store_name) as purchase_count, SUM(rate.rating) as rating, class.sector_id, rate.category_code")
                .where("rate.calculation_date BETWEEN '"+fromDate+"' and '"+toDate+"' and rate.user_id = :user_id and rate.store_name != ''", { user_id: loginName})
                .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
                .leftJoinAndSelect(StoreNonGreener, "store", "LOWER(store.name) = LOWER(rate.store_name)")
                .groupBy("rate.store_name")
                .addGroupBy("class.sector_id")
                .addGroupBy("class.id")
                .addGroupBy("store.id")
                .addGroupBy("rate.category_code")
                .orderBy("rate.store_name", "DESC")
                .addOrderBy("rating", "DESC")
                .getRawMany();
        }

        // manual sort by rating cause DESC has been override by disticnt initial group by
        data = data.sort((a, b) => a.purchase_count - b.purchase_count);

        const halo_reduced = 0;
        const final_ar = [];

        for(let i = 0; i < data.length; i++) {
            const logo = data[i].store_logo;
            let store_name = data[i].name;
            let store_id = '';

            if (type !== 'not_greenr'){
                store_id = data[i].store_id;
            }else{
                if(data[i].name === null) store_name = data[i].store_name;

                // skip if greener store
                const count = await getRepository(Store).createQueryBuilder("store") .where("LOWER(store.name) = LOWER(:name)", { name: store_name }).getCount();

                if (count !== 0) continue;
            }

            let swaps = await getRepository(Store)
                .createQueryBuilder("store")
                .where("store.sectorId = :sector_id", { sector_id: data[i].sector_id })
                .limit(3)
                .getRawMany();

            if(type !== 'not_greenr') swaps = [];

            final_ar.push({
                'store_id': store_id,
                'sector_id': data[i].sector_id,
                'name': store_name.replace(/(^\w|\s\w)/g, m => m.toUpperCase()),
                'purchase_number_count': data[i].purchase_count,
                'improved_by': 0,
                'logo': logo,
                'swaps': swaps
            });
        }
        return response.send([{
            'percentage': halo_reduced.toFixed(0),
            'stores': final_ar
        }]);
    }

    static topRecommendation = async(request: Request, response: Response, next: NextFunction) => {
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        let data =  await getRepository(RatingHistory)
            .createQueryBuilder("rate")
            .distinctOn(["rate.store_name"])
            .select("rate.store_name, store.name, COUNT(rate.store_name) as purchase_count, SUM(rate.rating) as rating, class.sector_id, rate.category_code")
            .where("rate.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and rate.user_id = :user_id and rate.store_name != ''", { user_id: loginName})
            .leftJoinAndSelect(Classification, "class", "class.anzic_code = rate.category_code")
            .leftJoinAndSelect(StoreNonGreener, "store", "LOWER(store.name) = LOWER(rate.store_name)")
            .groupBy("rate.store_name")
            .addGroupBy("class.sector_id")
            .addGroupBy("class.id")
            .addGroupBy("store.id")
            .addGroupBy("rate.category_code")
            .orderBy("rate.store_name", "DESC")
            .addOrderBy("rating", "DESC")
            .getRawMany();

        // manual sort by rating cause DESC has been override by disticnt initial group by
        data = data.sort((a, b) => a.rating - b.rating);

        const final_ar = [];
        let count = 0;

        for(let i = 0; i < data.length; i++) {
            if(count === 5) break;

            const logo = data[i].store_logo;
            let store_name = data[i].name;

            if(data[i].name === null) store_name = data[i].store_name;

            // skip if greener store
            const count_g = await getRepository(Store).createQueryBuilder("store") .where("LOWER(store.name) = LOWER(:name)", { name: store_name }).getCount();

            if (count_g !== 0) continue;

            const swaps = await getRepository(Store)
                .createQueryBuilder("store")
                .where("store.sectorId = :sector_id", { sector_id: data[i].sector_id })
                .limit(2)
                .getRawMany();

            if(swaps.length === 0) continue;
            else  count += 1;

            final_ar.push({
                'name': store_name,
                'purchase_number_count': data[i].purchase_count,
                'logo': logo,
                'improved_by': 0,
                'recommends': swaps
            });
        }

        return response.send(final_ar);
    }

    static topSwap = async(request: Request, response: Response, next: NextFunction) => {
        const page:any = request.query.page ? request.query.page : 1;
        const limit:any = request.query.limit ? request.query.limit : 3;
        const offset = (page * limit) - limit;
        const store_id:any = request.query.store_id ? request.query.store_id : false;
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        const st_cat = await getRepository(Store).findOne(store_id);
        const data =  await getRepository(Store)
            .createQueryBuilder("st")
            .where("sectorId = :category and id != :store_id", { store_id: store_id, category: st_cat.sectorId})
            .skip(offset)
            .take(limit)
            .orderBy("rate.rating", "DESC")
            .getRawMany();

        const rating_data = [];

        for(let u = 0; u < data.length; u++) {
            rating_data.push(data[u].rate_store_name);
        }

        let group = rating_data.reduce((r,c) => (r[c] = (r[c] || 0) + 1, r), {})
        let final_ar = [];

        for(let i = 0; i < group.length; i++) {
            let logo = '';
            const data = await getRepository(Store)
                .createQueryBuilder("store")
                .select("store.logo", "logo")
                .where("LOWER(store.name) = LOWER(:name)", { name: i })
                .getRawMany();

            if (data.length !== 0) logo = data[0].logo;

            final_ar.push({
                'name': i,
                'logo': logo
            });
        }

        return response.send(final_ar);
    }

    static getReforestation = async(request: Request, response: Response, next: NextFunction) => {
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        try {
            let { emissions } = await getRepository(RatingHistory)
                .createQueryBuilder("calc")
                .select("SUM(calc.rating)", "emissions")
                .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id", { id: loginName })
                .getRawOne();

            let tc02 = emissions * (365/90) * 100;

            if(emissions == null && emissions == 0) tc02 = 0;

            return response.send({
                'kgc02e': tc02
            });

        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."});
        }
    }

    static getGreenerOffset = async(request: Request, response: Response, next: NextFunction) => {
        let user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        try {
            const data = await RatingHelper.setGreenerOffset(loginName);

            return response.send({
                'status': data.status,
                'kgc02e': data.trees,
                'info': data.greener_store
            });

        } catch (e) {
            return response.status(422).send({
                "message": "No data associated with the given parameter.",
                "error": e
            });
        }
    }

    static getTreesPlanted = async(request: Request, response: Response, next: NextFunction) => {
        const user_id_test = request.query.user_id_test ? request.query.user_id_test : false;
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);

        let loginName:any = user.basiq_id;

        if(user_id_test) loginName = user_id_test;

        try {
            const data = await getRepository(RatingHistory)
                .createQueryBuilder("calc")
                .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.after_link IS TRUE and calc.user_id = :id and calc.for_recommendation IS FALSE ", { id: loginName })
                .getMany();

            // get lookup table data for rating computation
            const lookup = await getRepository(Classification).createQueryBuilder("lookup").getMany();
            let trees = 0;

            for(let d = 0; d < data.length; d++) {
                let base = (375 / 1000000);
                const life_style = 1;

                // classification lookup
                for(let b = 0; b < lookup.length; b++) {
                    if(data[d].category_code === lookup[b].anzic_code) base = (lookup[b].emission_factor_gc02 / 1000000);
                }

                trees += (base * 1 * life_style * data[d].spend) * 2;
            }

            return response.send({
                'trees': trees
            });

        } catch (e) {
            return response.status(422).send({
                "message": "No data associated with the given parameter.",
                "error": e
            });
        }
    }

    static getHaloAndLinkedAccounts = async(request: Request, response: Response, next: NextFunction) => {

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);

        let loginName:any = user.basiq_id;

        let halo = await RatingHelper.ThreeMonths(loginName);
        let accounts = await RatingHelper.linkedAccounts(user.id);

        return response.send({
            "rating": halo.rating.green,
            'number_of_linked_accounts': accounts
        });
    }

}
