import {NextFunction, Request, Response} from "express";
import { BasiqHelper } from "../../helpers/basiq";
import { logger } from "../../helpers/logger";
import { MixpanelHelper } from '../../helpers/mixpanel';
import { Helper as GlobalHelper } from "../../helpers/helper";

const env = process.env;

export class BasiqConnection {

    static getToken = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const api = new BasiqHelper();

        let token;
        try{
            token = await api.generateTokenFrontEnd();
        } catch(e) {
            let er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            const event_name = 'Get token error';
            const data = {
                'distinct_id': user.encrypted_id,
                'error_data': e
            }

            MixpanelHelper.track(event_name, data);

            return response.status(422).send(er);
        }

        return response.send({"message": "Success", "token": token});
    }

    static createConnection = async(request: Request, response: Response, next: NextFunction) => {
        const api = new BasiqHelper();
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);

        let data;
        try{
            data = await api.createConnection(user.basiq_id);
        } catch(e) {
            const er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            const event_name = 'Create connection error';
            const data = {
                'distinct_id': user.encrypted_id,
                'error_data': e
            }

            MixpanelHelper.track(event_name, data);

            return response.status(422).send(er);
        }

        return response.send({"message": "Success", "data": data});
    }

}