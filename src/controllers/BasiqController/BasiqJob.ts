import {NextFunction, Request, Response} from "express";
import { BasiqHelper } from "../../helpers/basiq";
import { FirebaseHelper } from "../../helpers/firebase";
import { getRepository } from "typeorm";
import { User } from "../../models/User";
import { Job } from "../../models/Job";
import { JobHelper as GlobalJobHelper } from "../../helpers/job";
import { JobHelper } from "./helpers/job";
import { MixpanelHelper } from '../../helpers/mixpanel';
import { Helper as GlobalHelper } from "../../helpers/helper";
import { logger } from "../../helpers/logger";

const env = process.env;

export class BasiqJob {

    static checkConnection = async(request: Request, response: Response, next: NextFunction) => {
        const connection_id:any = request.query.connectionId;
        const jobId:any = request.query.jobId;
        const fcm_token:any = request.query.fcmToken;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        
        await JobHelper.checkConnection(user, connection_id, jobId, fcm_token);

        return response.send({"status": "ok", "message": "Will notify user device once the bank connection is done."});
    }

    static checkJobConnection = async(request: Request, response: Response, next: NextFunction) => {
        const jobs = await getRepository(Job).createQueryBuilder("jobs").orderBy("jobs.created_at", "ASC").getMany();

        for(let i = 0; i < jobs.length; i++) {
            try {
                const connection_id = jobs[i].connection_id;
                const jobId = jobs[i].job_id;
                const fcm_token = jobs[i].fcm_token;
                const userId = jobs[i].user_id;
                const step = jobs[i].step;// step 1,2  = 1 hour, step 3,4 = 24 hrs - fire checker is the created_at column field
                const user = await getRepository(User).findOne(userId);
                const hubsPotId = user.hubspot_id;
                const created_date = new Date(jobs[i].created_at);
                const now = new Date();
                const diff = now.valueOf() - created_date.valueOf();
                let duration:any = diff/1000/60/60; // Convert milliseconds to hours
                duration = duration.toFixed(0);

                // hour checker
                if(step === 1 || step === 2 && duration === 1) {
                    await GlobalJobHelper.checkJobStatus(userId, user, connection_id, jobId, fcm_token, hubsPotId, step, jobs[i].id);
                }

                // 24 hrs checker
                if(step === 3 || step === 4 && duration === 24) {
                   await GlobalJobHelper.checkJobStatus(userId, user, connection_id, jobId, fcm_token, hubsPotId, step, jobs[i].id);
                }

                // else ignore
            } catch (e) {
                console.log("ignore error for user no hubspot id", e);
            }
        }

        return response.send({"status": "ok", "message": "Will notify user device once the bank connection is done."});
    }

    static checkInitailJob = async(request: Request, response: Response, nexts: NextFunction) => {
        const jobId:any = request.query.jobId;
        const api = new BasiqHelper();

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const devices = user.device_ids;

        await setInterval(async function() {
            const job = await api.getJob(user.basiq_id, jobId);
            const payload = {
                notification: {
                    title: "Bank Linking Error",
                    body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
                },
                data: {
                    user_id: ''+userId+'',
                    status: '1',
                    message: 'failed',
                    isInitialTransaction: '1'
                }
            };

            const options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            for(let j = 0; j < job.steps.length; j++) {
                const status = job.steps[j].status.toString();
                const event_name = 'Bank Linking Failed';
                const data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'detail': job.steps[j].result,
                    'data': {
                        'job': job
                    }
                }

                if(job.steps[j].title.toString() === 'retrieve-accounts') {
                    if(status === 'failed') {
                        clearInterval(this);
                        MixpanelHelper.track(event_name, data);

                        if(devices !== null) {
                            if(devices['android'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['android'], payload, options);
                            }

                            if(devices['ios'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                            }
                        }

                        break;
                    }
                }

                if(job.steps[j].title.toString() === 'retrieve-transactions') {
                    if(status === 'failed') {
                        clearInterval(this);
                        MixpanelHelper.track(event_name, data);

                        if(devices !== null) {
                            if(devices['android'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['android'], payload, options);
                            }

                            if(devices['ios'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                            }
                        }

                        break;
                    }

                    if(status === 'success') {
                        clearInterval(this);
                    }

                    break;
                }

            }


        }, 10000); // 10 seconds

        return response.send({"status": "ok"});
    }

    static getJobFromConnection = async(request: Request, response: Response, next: NextFunction) => {
        const connection_id:any = request.query.connectionId;
        const api = new BasiqHelper();

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);

        // refresh the connection
        await api.refreshConnection(user.basiq_id, connection_id);

        try {
            let jobDetail = await api.getJobFromConnection(user.basiq_id, connection_id);
            return response.send({
                'status': 1,
                'detail': jobDetail
            });
        } catch (e) {
            return response.send({
                'status': 0,
                'error': e
            });
        }
    }

}