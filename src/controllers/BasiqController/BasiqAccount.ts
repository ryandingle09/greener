import {getConnection, getManager, getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { Account } from "../../models/Account";
import { BasiqHelper } from "../../helpers/basiq";
import { FirebaseHelper } from "../../helpers/firebase";
import { User } from "../../models/User";
import { Transaction } from "../../models/Transaction";
import { Classification } from "../../models/Classification";
import { Store } from "../../models/Store";
import { HashHelper } from "../../helpers/hasher";
import { TransactionHelper } from '../../helpers/transaction';
import { RatingHistory }  from '../../models/RatingHistory';
// import { HubSpotHelper } from "../../helpers/hubspot";
import { MixpanelHelper } from '../../helpers/mixpanel';
import { RatingHelper } from "../../helpers/rating";
import { Helper as GlobalHelper } from "../../helpers/helper";
import { QuizHelper } from "../../helpers/quiz";
import { logger } from "../../helpers/logger";
import { QuizResponse } from "../../models/QuizResponse";
import { BasiqHelper as BasiqTransactionHelper } from "./helpers/basiq"; 
import { CommonHelper } from "./helpers/common"; 
import { HubspotHelper } from "./helpers/hubspot"; 

const env = process.env;

export class BasiqAccount {

    static getAccounts = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        const data = [];
		const accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i = 0; i < accounts.length; i++) {
            try {
                let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                if(decryptId === userId) {
                    data.push(accounts[i]);
                }
            } catch (e) {
                continue;
            }
        }

        return response.send(data);
    }

    static getAccountFromBasic = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const api = new BasiqHelper();

        let accounts = await api.syncAccounts(user.basiq_id);

        accounts =  accounts.data.data;

        let accounts_arr = [];

        for(let t = 0; t < accounts.length; t++) {
            accounts_arr.push({
                'account_id': accounts[t].id,
                'account_name': accounts[t].name,
                'bank_code': accounts[t].institution,
                'connection': accounts[t].connection
            });
        }

        return response.send(accounts_arr);
    }

    static getAccount = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        let data;

        try{
            data = await getRepository(Account)
                .createQueryBuilder("account")
                .where("account.id = :id", { id: request.params.accountId })
                .getRawMany();
        } catch(e) {
            let er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            return response.status(422).send(er);
        }

        return response.send(data);
    }

    static deleteAccount = async(request: Request, response: Response, next: NextFunction) => {
        // get request parameters
        const accounts_delete:any = request.query.accounts_delete;
        const accounts_selected:any = request.query.accounts_selected;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const devices = user.device_ids;

        // get lookup for rating computation
        const lookup = await GlobalHelper.getStoreLookUps();

        // get store list
        const stList = await BasiqTransactionHelper.getStoreList();

        // record in mixpanel
        await BasiqTransactionHelper.accountDeleteMixpanelTracker(user.basiq_id, accounts_selected, accounts_delete);

        try {
            // delete accounts user wants to delete
            await BasiqTransactionHelper.deleteAccount(accounts_delete);

            // delete users rating history records
            await BasiqTransactionHelper.deleteRating(user.basiq_id);

            // user selected accounts
            const acAr = await BasiqTransactionHelper.getUserSelectedAccounts(accounts_selected);

            // get user quiz response for home and energy
            const quizResponse = await QuizHelper.getHomeEnergyResponse(user.id);

            // getting user transactions to resync
            await BasiqTransactionHelper.resyncUserTransactions(acAr, user, stList, lookup, quizResponse);

            // notify device
            await BasiqTransactionHelper.notifyUserDevice(userId, devices);

            // resync updated rating into hubspot user contact
            await RatingHelper.syncRatingToHubspotAndMixpanel(user);

        } catch (e) {
            logger.log('error', e);
            return response.status(422).send({"message": "No data associated with the given parameter."});
        }

        logger.log('info', 'Successfully Deleted and Resync Your Halo Calculation.');
        return response.send({"message": "Successfully Deleted and Resync Your Halo Calculation."});
    }

    static setJoinAccount = async(request: Request, response: Response, next: NextFunction) => {
        const account_id:any = request.query.account_id;
        const is_join_account:any = request.query.is_join_account;

        let data;

        try {
            data = await getRepository(Account).findOneOrFail({'account_id': account_id});
        } catch (e) {
            return response.status(422).send({"message": "Data not found at the given ID."});
        }

        data.is_join_account = (is_join_account === '1') ? true : false;

        try {
            await getRepository(Account).save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data. Please contact software developer."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }
    // this function syncAccounts is not used anymore
    static syncAccounts = async(request: Request, response: Response, next: NextFunction) => {
        const api = new BasiqHelper();

        // get users
        const users = await getRepository(User).find({select: ["basiq_id","id"] });

        // truncate accounts table
        const entityManager = getManager();
        await entityManager.query(`TRUNCATE table ${env.TYPEORM_SCHEMA}."accounts";`);

        for(let u = 0; u < users.length; u++) {
          const loginName = users[u].basiq_id;

            if(loginName === null){
                continue;
            }

            try{
                let accounts = await api.syncAccounts(loginName);

                accounts =  accounts.data.data;

                console.log(accounts);

                for(let t = 0; t < accounts.length; t++) {
                    let data = new Account();

                    data.user_id = users[u].id;
                    data.account_id = accounts[t].id;
                    data.account_name = accounts[t].name;
                    data.bank_code = accounts[t].institution;

                    await getRepository(Account).save(data);
                }
            }catch(e) {
                // do nothing
            }
        }

        return response.send('Done');
    }

    static createAccount = async(request: Request, response: Response, next: NextFunction) => {
        const { account_id, account_name, bank_code } = request.body;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);
		const account = new Account();

        const encrypt_userId = await HashHelper.encrypt(userId);

        account.user_id = encrypt_userId;
        account.account_id = account_id;
        account.account_name = account_name;
        account.bank_code = bank_code;

        try {

          // Check if account entry exists for this account ID
          let accountEntry = await getRepository(Account).findOne({ account_id: account.account_id });

          if ( accountEntry ) {

            // Update
            accountEntry.user_id = account.user_id;
            getRepository(Account).save(accountEntry);

          } else {

            // Insert
            getRepository(Account).save(account);

          }

        } catch (e) {
          return response.sendStatus(422).send({'error': 'The account id might be existing already!', 'message': e});
        }

        return response.send({'message': 'account successfully saved!'});
    }

    static syncUserAccounts = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const api = new BasiqHelper();
        let accounts = await api.syncAccounts(user.basiq_id);

        accounts =  accounts.data.data;

        for(let t = 0; t < accounts.length; t++) {
            let data = new Account();

            data.user_id = user.id;
            data.account_id = accounts[t].id;
            data.account_name = accounts[t].name;
            data.bank_code = accounts[t].institution;

            await getRepository(Account).save(data);
        }

        return response.send({'message': 'user accounts successfully saved!'});
    }

    static syncInitialTransactions = async(request: Request, response: Response, nextf: NextFunction) => {
        const accounts:any = request.query.accounts ? request.query.accounts : false;
        const api = new BasiqHelper();
        const jobId:any = request.query.jobId;
        const fcm_token:any = request.query.fcmToken;

        const fromDate = GlobalHelper.getDateRange(365).fromDate;
        const toDate = GlobalHelper.getDateRange(365).toDate;

        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);
        const encrypt_userId = user.encrypted_id;
        const mixData = {};

        // Initilize transaction
        const loginName = user.basiq_id;
        const encrypt_loginName = await HashHelper.encrypt(loginName);
        const first_name = user.first_name;

        // get lookup table data for rating computation
        const lookup = await getRepository(Classification).createQueryBuilder("lookup").getRawMany();

        // get store list
        const stList = await BasiqTransactionHelper.getStoreList();

        // get user accounts
        const acAr = await CommonHelper.getAccounts(userId, accounts);

        // get quiz results
        const quizResponse = await QuizHelper.getHomeEnergyResponse(user.id);

        mixData['number_linked_accounts'] = acAr.length;

        const initialCall = async () => {
            try {
                await BasiqTransactionHelper.syncRawTransactions(2000, response, user, jobId, fcm_token, accounts, fromDate, toDate, lookup, first_name, encrypt_loginName, encrypt_userId, stList, acAr, quizResponse);
            } catch (e) {

                const event_name = 'Bank Linking Failed';
                const data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'error_details': e
                }
                
                MixpanelHelper.track(event_name, data);

            }
        }

        // Trigger sync of transactions
        await initialCall();

        // Get all accounts
        const allAccounts = await getRepository(Account).createQueryBuilder('acc').distinctOn(["acc.account_id"]).getRawMany();

        // Determine which accounts belong to current user
        const userAccounts =  await BasiqTransactionHelper.getrelevantAccountsAndUsers(allAccounts, [user]);
        
        // Delete rating histories
        await getConnection().createQueryBuilder().delete().from(RatingHistory).where("user_id = :basiq_id", { basiq_id: user.basiq_id }).execute();

        // Map through accounts and create ratings
        await BasiqTransactionHelper.saveTransaction(userAccounts, lookup, stList, fromDate, toDate);

        // populate HS fields
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;

        const properties = [];

        properties.push({ property: 'g_greener_member_lifecycle_stage', value: 'Greener App User - Bank connected' });

        //for bank hs fields
        const halo = await RatingHelper.ThreeMonths(loginName);
        const bankList = await BasiqTransactionHelper.getUserBankList(userId);
        const distinctOnBankCode = await BasiqTransactionHelper.distinctOnBankCode(bankList);

        // populate hubspot fields
        await HubspotHelper.poPulateUserDataUponbankLink(properties, user, loginName, distinctOnBankCode, fromDate_90, toDate_90, halo);

        //set
        mixData['halo_rating'] = halo.rating.green.toFixed(2);
        mixData['is_onboarded'] = user.is_onboarded;

	      // set user alias
        MixpanelHelper.set(encrypt_userId, mixData);

        // set accounts_sync=TRUE
        await getConnection().createQueryBuilder().update(Account).set({ account_synced: true }).where("account_id IN (:...account_ids)", { account_ids: accounts.split(",") }).execute();

        const event_name = 'Bank Linking Selected Account';
        const data = {
            'distinct_id': user.encrypted_id,
            'first_name': user.first_name,
            'accounts': accounts
        }

        MixpanelHelper.track(event_name, data);

        const payload = {
            notification: {
                title: "Your rating is calculated",
                body: "See how green you are, and how to improve."
            },
            data: {
                user_id: ''+userId+'',
                status: '1',
                message: 'success',
                isInitialTransaction: '1'
            }
        };

        const options = {
          priority: "normal",
          timeToLive: 60 * 60
        };

        FirebaseHelper.notifyDevice(fcm_token, payload, options);

        return response.send({'status': 'ok'});
    }

    static getConnectedBankStatus = async(request: Request, response: Response, next: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);
        const data = [];
		const accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i = 0; i < accounts.length; i++) {
            try {
                const decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                if(decryptId === userId) {
                    data.push(accounts[i]);
                }
            } catch (e) {
                continue;
            }
        }

        let status = 1;
        let message = 'User already linked a bank account.';

        if(data.length === 0) {
            status = 0;
            message = 'User has no associated linked account yet.';
        }

        return response.send({
            'message': message,
            'status': status
        });
    }

}