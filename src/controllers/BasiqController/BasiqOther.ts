import {getConnection, getManager} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { Account } from "../../models/Account";
import { User } from "../../models/User";
import { BasiqHelper } from "../../helpers/basiq";
import { getRepository } from "typeorm";
import { HashHelper } from "../../helpers/hasher";
import { GoogleHelper } from '../../helpers/google';
import { StoreAddressDraft }  from '../../models/StoreAddressDraft';
import { logger } from "../../helpers/logger";
import { Helper as GlobalHelper } from "../../helpers/helper";

const env = process.env;

export class BasiqOther {

    static getUser =  async(request: Request, response: Response, nextf: NextFunction) => {
        // get user id from token
        const userId = await GlobalHelper.getIDfromToken(request, response);

        // get user data
        const user = await GlobalHelper.getUserData(userId);

        if(user.basiq_id !== null) {
            return response.send({"basiq_id" : user.basiq_id });
        }

		// create also this user in yodlee
        let api = new BasiqHelper();
		let basiqUsertoCreate = {
            mobile: user.mobile_number,
            email: user.email
        }

        let responseUser;

		try{
            responseUser = await api.createUser(basiqUsertoCreate);
        } catch(e) {
            let er = {
				'message_high': 'Error adding user to basiq.',
                'message': e.response.statusText,
                'info': e.response.data
			}

            return response.status(422).send(er);
        }

		try{
			user.basiq_id = responseUser.id;
			await getRepository(User).save(user);

		} catch (e) {
			let er = {
				'message_high': 'Error adding user to basiq.',
				'error_code_text': 'LimitExceededException',
				'statusText': responseUser,
				'info': responseUser,
                'er': e
			}

			return response.status(422).send(er);
		}

        return response.send({"basiq_id" : responseUser.id });
    }

    static addressSearchViaGmaps = async(request: Request, response: Response, nextf: NextFunction) => {
		const addRarr: StoreAddressDraft[] = [];
        const { addresstext, city_list } = request.body;
        const stores = addresstext.toString().split(",");
        const cities = city_list.toString().split(",");

        for(let s = 0; s < stores.length; s++) {
            let store_name = stores[s].toString().replace("%20"," ");
            let api = new GoogleHelper();

            if(cities.length !== 0) {
                for(let c = 0; c < cities.length; c++) {
                    let city = cities[c].toString().replace("%20"," ");
                    let text_search = ''+store_name+' in '+city+' Australia AU, AUS';
                    let result = await api.getStoreAddresses(text_search);
                    let addresses =  result.results;
                    let next = result.next_page_token;
                    let status = result.status;

                    while (next !== undefined){
                        let result_next = await api.getStoreAddressesNext(text_search, next);
                        let nextLink = result_next.next_page_token;
                        status = result_next.status;
                        addresses = addresses.concat(result_next.results);

                        if(result_next.results.length !== 0) {
                            next = nextLink;
                        }

                        if(status === 'ZERO_RESULTS') {
                            next = nextLink;
                        }
                    }

                    for(let a in addresses){
                        let rat = new StoreAddressDraft();
                        rat.store_name = store_name;
                        rat.address = addresses[a].formatted_address;
                        rat.latitude = addresses[a].geometry.location.lat;
                        rat.longitude = addresses[a].geometry.location.lng;
                        addRarr.push(rat);
                    }
                }
            } else {
                let text_search = ''+store_name+' in Australia AU, AUS';
                let result = await api.getStoreAddresses(text_search);
                let addresses =  result.results;
                let next = result.next_page_token;
                let status = result.status;

                while (next !== undefined){
                    let result_next = await api.getStoreAddressesNext(text_search, next);
                    let nextLink = result_next.next_page_token;
                    status = result_next.status;
                    addresses = addresses.concat(result_next.results);

                    if(result_next.results.length !== 0) {
                        next = nextLink;
                    }

                    if(status === 'ZERO_RESULTS') {
                        next = nextLink;
                    }
                }

                for(let a = 0; a < addresses.length; a++) {
                    let rat = new StoreAddressDraft();
                    rat.store_name = store_name;
                    rat.address = addresses[a].formatted_address;
                    rat.latitude = addresses[a].geometry.location.lat;
                    rat.longitude = addresses[a].geometry.location.lng;
                    addRarr.push(rat);
                }
            }
        }

        let connection = getConnection();
        await connection.manager.save(addRarr, { chunk: 10000 });

        return response.send({'status': 'ok','addresses': addRarr});
    }

    // not needed anymore - this function need to run only once.
    static encryptUserIdsInAccount =  async(request: Request, response: Response, nextf: NextFunction) => {
		const accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i = 0; i < accounts.length; i++) {
            const encryptId = await HashHelper.encrypt(accounts[i].acc_user_id);
            const data = await getRepository(Account).findOneOrFail(accounts[i].acc_id);
            data.user_id = encryptId;

            await getRepository(Account).save(data);
        }

        return response.send({'status': 'ok'});
    }


}