import {getConnection, getRepository} from "typeorm";
import { Transaction } from "../../../models/Transaction";
import { RatingHistory } from "../../../models/RatingHistory";
import { StoreNonGreener } from "../../../models/StoreNonGreener";
import { Sector } from "../../../models/Sector";
import * as AWS from 'aws-sdk';
import { MixpanelHelper } from '../../../helpers/mixpanel';
import { QuizHelper } from "../../../helpers/quiz";
import { logger } from "../../../helpers/logger";
import { Helper as GlobalHelper } from "../../../helpers/helper";

const moment = require('moment');
const env = process.env;

export class TransactionHelper{

    static SyncTransaction = async(transactions: any[], encrypt_loginName, lookup, storeList: any[], loginName, quizResponse, userId, first_name, isDailySync=true) => {
        const transArray = [];
        const transRejectArray = [];
        const ratingArray = [];
        const mixpanelEvents = [];
    
        await Promise.all(transactions.map(async(transaction) => {
            try {

                const categoryClass = transaction.enrich.category.anzsic.class.code || null; 
                const transactionDate = transaction.postDate || null; 
                const storeName = transaction.enrich.merchant.businessName || '';
                const afterLink = (new Date(transactionDate) >= new Date(transaction.acct_created_at)) ? true : false;
    
                // checking if transaction has no category code to be matched up
                const categoryCode = transaction.enrich.category.anzsic.class.code; 
                if(categoryCode === '0' || categoryCode === null) {
                    transRejectArray.push(await TransactionHelper.getRejectObject(transaction));
                }
    
                // Update mixPanel
                const categoryGroup = transaction.enrich.category.anzsic.group.code || null; 
                const events = await TransactionHelper.getMixPanelData(transaction.amount, lookup, categoryClass, storeName, storeList, categoryGroup, transactionDate, transaction.id, afterLink, userId, first_name);
                mixpanelEvents.push(events);
    
                // Push to transaction
                const transactionObject = await TransactionHelper.getTransactionObject(transaction, encrypt_loginName);
                transArray.push(transactionObject);
    
                // Push to rating
                if(isDailySync === true) {
                    const ratingObject = await TransactionHelper.getRating(loginName, storeName, categoryClass, transaction.amount.toString().replace("-",""), lookup, afterLink, transactionDate, storeList);
                    ratingArray.push(ratingObject);
                }

            } catch(e){
                // Do something here, or continue to the next transaction
                logger.log('error', e);
                logger.log('error', `Failed transaction!, ${JSON.stringify(transactions)}`);
            }
        }));
    
        if(transRejectArray.length !== 0){
            const today = GlobalHelper.formatDate(new Date());
            const file_name = new Date()+'.json';
    
            AWS.config.update({
                accessKeyId: env.AWS_ACCESS_KEY,
                secretAccessKey: env.AWS_SECRET_KEY,
                region: env.AWS_REGION
            });
    
            const s3 = new AWS.S3();
    
            // Setting up S3 upload parameters
            const params = {
                Bucket: 'greener-wallet',
                Key: 'transactions/rejected/'+today+'/'+file_name+'',
                Body: JSON.stringify(transRejectArray),
                ContentType: 'application/json',
            };
    
            // Uploading files to the bucket
            s3.upload(params, async(err, data) => {
                if (err) {
                    console.log("s3 upload error: ", err);
                }
    
                // UPLOADED
            });
        }
    
        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Transaction)
            .values(transArray)
            .onConflict(`("transaction_id") DO NOTHING`)
            .execute();

        if(isDailySync === true) 
            await getConnection().createQueryBuilder().insert().into(RatingHistory).values(ratingArray).execute();

        MixpanelHelper.import_batch(mixpanelEvents);
    
        return {
            'events': mixpanelEvents,
            'total_transaction_from_basiq': transactions.length,
            'transaction_saved_count': transArray.length,
            'transaction_rating_history_saved_count': ratingArray.length,
            'transaction_failed_saved_count': transRejectArray.length,
        }
    }
    
    static getRejectObject = async(transaction: any) => {
        return {
            'status': transaction.status || null,
            'description': transaction.description || null,
            'connection': transaction.connection || null,
            'postDate': transaction.postDate || null,
            'subClass': transaction.subClass || null,
            'enrich': transaction.enrich || null,
        }
    }

    static getTransactionObject = async(transaction: any, encrypt_loginName: any) => {
        const location = transaction.enrich.location;
        const links = transaction.enrich.links;

        return {
            'basiq_id': transaction.id,
            'transaction_id': transaction.id,
            'basiq_name_encrypted': encrypt_loginName,
            'amount': transaction.amount && transaction.amount.replace("-", "") || null,
            'transaction_date': transaction.postDate || null,
            'account_id': transaction.account || null,
            'description': transaction.description || null,
            'merchant_name': transaction.enrich.merchant.businessName || '',
            'merchant_url': transaction.enrich.merchant.website || null,
            'merchant_route_no': location && location.routeNo || null,
            'merchant_route': location && location.route || null,
            'merchant_postal_code':location && location.postalCode || null,
            'merchant_suburb': location && location.suburb || null,
            'merchant_state': location && location.state || null,
            'merchant_country': location && location.country || null,
            'merchant_formatted_address': location && location.formattedAddress || null,
            'merchant_geometry_lan': location && location.geometry.lat || null,
            'merchant_geometry_lon': location && location.geometry.lng || null,
            'sub_class': transaction.subClass.code || null,
            'category_division': transaction.enrich.category.anzsic.division.code || null,
            'category_subdivision': transaction.enrich.category.anzsic.subdivision.code || null,
            'category_group': transaction.enrich.category.anzsic.group.code || null,
            'category_class': transaction.enrich.category.anzsic.class.code || null,
            'institution': transaction.institution || null,
            'logo_master': links && transaction.enrich.links['logo-master'] || null
        }
    }

    static getRating = async(loginName: string, storeName: string, categoryClass: string, amount: number, classificationLookup: any[], transactionAfterLink: boolean, transactionDate, storeList) => {
        const lifeStyle = 1;
        let greener = 1;
        let is_recommended = true;

        for(let i in storeList) {
            if(storeList[i].name.toString().toLowerCase() === storeName.toString().toLowerCase() || storeList[i].secondary_names !== null && storeList[i].secondary_names.includes(''+storeName.toString().toLowerCase()+'') === true) {
                is_recommended = false;
                
                if(transactionAfterLink) greener = 0;

                break;
            }
        }

        const getLookUp = classificationLookup.find(item => item.lookup_anzic_code === categoryClass);
        const isInLookup = (getLookUp === undefined) ? false : true;
        const base = (isInLookup !== false) ? (getLookUp.lookup_emission_factor_gc02 / 1000000) : (375 / 1000000);
        const rating = base * greener * lifeStyle * amount;
        
        return {
            'user_id' : loginName,
            'store_name' : storeName.toLowerCase(),
            'spend' : amount,
            'rating' : rating,
            'calculation_date' : transactionDate,
            'for_recommendation' : is_recommended,
            'category_code' : categoryClass,
            'after_link' : transactionAfterLink,
            'celebrated' : false
        }
    }

    static getMixPanelData = async(amount: number, classificationLookup, categoryClass, storeName: string, storeList: any[], categoryGroup, transactionDate: string, basiqId:string, linkStatus, userId, first_name) => {
        let sectorName:any = false;
        let merchantId:any = false;
        
        // Set carbon by classification
        const isInClassificationLookup = classificationLookup.find(item => item.anzic_code === categoryClass);
        const carbon = isInClassificationLookup ? (amount * isInClassificationLookup.emission_factor_gc02 / 1000) : 0;
        let carbonOffset = (carbon*(365/90)/40000*10);


        const isGreenerStore = storeList.find(store => store.name.toLowerCase() === storeName.toLowerCase());
        if(isGreenerStore){
            let sector = await getRepository(Sector).findOne(isGreenerStore.sectorId);
            sectorName = sector.name;
            merchantId = isGreenerStore.id;
        }

        const isStoreInList = storeList.find(store => store.name.toLowerCase() === storeName.toLowerCase());
        const isPartner = isStoreInList ? true: false;


        if(sectorName === false) {
            // classification lookup
            const classificationItem = classificationLookup.find(item => categoryClass === item.anzic_code);
            if(classificationItem){
                const sector = await getRepository(Sector).findOne(classificationItem.sector_id);
                sectorName = sector.name;
                const ngSt = await getRepository(StoreNonGreener).findOne({ where: {name: storeName}});
                merchantId = ngSt ? ngSt.id : merchantId;
            }
        }

        const dateRange = GlobalHelper.getTransactionDateRange(transactionDate);
        const currentDate = new Date(transactionDate);
        const dateUTC = new Date(moment.utc(currentDate));
        const readableTDate = GlobalHelper.formatDate(currentDate);
        const event_name = 'New Transaction';
        const data = {
            'distinct_id': userId,
            '$insert_id': basiqId,
            'first_name': first_name,
            'date': readableTDate,
            'merchantName': storeName,
            'Merchant ID': merchantId,
            'Merchant UX category': sectorName,
            'Merchant ANZSIC class': categoryClass,
            'Merchant ANZSIC group': categoryGroup,
            'Merchant Greener Partner': isPartner,
            'Value': amount,
            'Carbon': carbonOffset,
            'Last 30 days': dateRange.is_30,
            'Last 60 days': dateRange.is_60,
            'Last 90 days': dateRange.is_90,
            'time': dateUTC,
            'Pre-link status': linkStatus,
            'environment': env.ENVIRONMENT
        }

        return {
            event: event_name,
            properties: data
        }
    }
}