import { getRepository} from "typeorm";
import { Account } from "../../../models/Account";
import { HashHelper } from "../../../helpers/hasher";

export class CommonHelper{

    static getAccounts = async(userId, accounts) => {
        const acAr = [];

        if (accounts !== false) {
            let acList = accounts.split(",");
            for(let i = 0; i < acList.length; i++) {
                acAr.push({'account_id': acList[i]});
            }
        } else {
            const accountss = await getRepository(Account).createQueryBuilder("acc").getRawMany();

            for (let i in accountss) {
                try {
                    const decryptId = await HashHelper.decrypt(accountss[i].acc_user_id);
                    if(decryptId === userId) {
                        acAr.push({'account_id': accountss[i].acc_account_id});
                    }
                } catch (e) {
                continue;
                }
            }
        }

        return acAr;
    }

    static arrayUnique = async(arr, uniqueKey) => {const uniqueList = []
        return arr.filter(function(item) {
            if (uniqueList.indexOf(item[uniqueKey]) === -1) {
                uniqueList.push(item[uniqueKey])
                return true
            }
        });
    }

}