import { getConnection, getManager, getRepository } from "typeorm";
import { Transaction } from "../../../models/Transaction";
import { Account } from "../../../models/Account";
import { RatingHistory } from "../../../models/RatingHistory";
import { Store } from "../../../models/Store";
import { logger } from "../../../helpers/logger";
import { MixpanelHelper } from '../../../helpers/mixpanel';
import { FirebaseHelper } from "../../../helpers/firebase";
import { TransactionHelper } from '../../../helpers/transaction';
import { BasiqHelper as BasiqGlobalHelper } from "../../../helpers/basiq";
import { TransactionHelper as BasiqTransactionHelper } from "./transaction";
import { QuizHelper } from "../../../helpers/quiz";
import { HashHelper } from "../../../helpers/hasher";
import { BasiqHelper as GlobalBasiqHelper } from "../../../helpers/basiq";

const env = process.env;

export class BasiqHelper{

    static getTransactionTableByAccount = async(account) => {
        const transaction = await getRepository(Transaction).createQueryBuilder()
            .distinctOn(["basiq_id"])
            .where("account_id = :account_id and transaction_id IS NOT null", { account_id: account.account_id })
            .getMany();

        logger.log('info', 'transactions count '+transaction.length+' for account '+account.account_id+'');

        // bind the account created at date to the transaction
        for(let i = 0; i < transaction.length; i++) {
            transaction[i]['acct_created_at'] = account.created_at;
        }

        return transaction;
    }

    static deleteAccount = async(accounts_delete) => {
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Account)
            .where("account_id IN (:...accounts_delete)", { accounts_delete: accounts_delete.split(",") })
            .execute();

        logger.log('info', 'accounts has been deleted: %s', accounts_delete);
    }

    static deleteRating = async(loginName) => {
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(RatingHistory)
            .where("user_id = :basiq_id", { basiq_id: loginName })
            .execute();
        
        logger.log('info', 'user rating historries has been deleted');
    }

    static getUserSelectedAccounts = async(accounts_selected) => {
        let acAr = [];
        const accounts = await getRepository(Account)
            .createQueryBuilder("acc")
            .where("acc.account_id IN (:...accounts_selected)", { accounts_selected: accounts_selected.split(",") })
            .getRawMany();

        for(let i = 0; i < accounts.length; i++) {
            acAr.push({'account_id': accounts[i].acc_account_id, 'created_at': accounts[i].acc_created_at});
        }

        return acAr;
    }

    static getStoreList = async() => {
        const stList = await getRepository(Store).createQueryBuilder("stores").getMany();
        return stList;
    }

    static accountDeleteMixpanelTracker = async(user, accounts_selected, accounts_delete) => {
        logger.log('info', 'accounts_selected: %s', accounts_selected);
        logger.log('info', 'accounts_delete: %s', accounts_delete);

        const event_name = 'Bank Linking Selected Account';
        const data = {
            'distinct_id': user.encrypted_id,
            'first_name': user.first_name,
            'accounts_selected': accounts_selected,
            'accounts_delete': accounts_delete
        }

        MixpanelHelper.track(event_name, data);

        return;
    }

    static notifyUserDevice = async(userId, devices) => {
        const payload = {
            notification: {
                title: "Bank account removed",
                body: "Your rating will no longer include this account."
            },
            data: {
                user_id: ''+userId+'',
                status: '1',
                message: 'success',
                isInitialTransaction: '1'
            }
        };

        const options = {
            priority: "normal",
            timeToLive: 60 * 60
        };

        if(devices !== null) {
            if(devices['android'] !== undefined) {
                FirebaseHelper.notifyDevice(devices['android'], payload, options);
            }

            if(devices['ios'] !== undefined) {
                FirebaseHelper.notifyDevice(devices['ios'], payload, options);
            }
        }

        return;
    }

    static resyncUserTransactions = async(acAr, user, stList, lookup, quizResponse) => {
        logger.log('info', 'getting user transactions to resync');
        await Promise.all(acAr.map(async(account)=>{
            const transaction = await BasiqHelper.getTransactionTableByAccount(account);

            logger.log('info', 'Start now the resync');
            await TransactionHelper.resyncHaloCalculateTransactions(transaction, user.basiq_id, stList, lookup, quizResponse);
            logger.log('info', 'Done now the resync');
        }));

        return;
    }

    static getTransactionByAccountFromBasiq = async(loginName, accountId, fromDate, toDate, acct_created_at) => {
        const api = new BasiqGlobalHelper();
        const transactions_list = await api.syncTransactionsByAccountId(loginName, accountId, false, fromDate, toDate);

        let transactions =  transactions_list.data.data;
        let next = transactions_list.data.links.next;
        while (next !== undefined){
            let transactions_next = await api.syncTransactionsByAccountId(loginName, accountId, next, fromDate, toDate);
            let nextLink = transactions_next.data.links.next;
            transactions = transactions.concat(transactions_next.data.data);
            next = nextLink;
        }

        for(let i = 0; i < transactions.length; i++) {
            transactions[i]['acct_created_at'] = acct_created_at;
        }

        return transactions;
    }

    static saveTransaction = async(relevantAccountsAndUsers, lookup, stList, fromDate, toDate, isDailySync=true) => {
        await Promise.all(relevantAccountsAndUsers.map(async(accountAndUser)=>{
            const loginName = accountAndUser.user.basiq_id;
            const accountId = accountAndUser.account.acc_account_id;
            const acct_created_at = accountAndUser.account.acc_created_at;
            const encrypt_userId = accountAndUser.user.encrypted_id;
            const first_name = accountAndUser.user.first_name;
            const quizResponse = await QuizHelper.getHomeEnergyResponse(accountAndUser.user.id);
            const transactions = await BasiqHelper.getTransactionByAccountFromBasiq(loginName, accountId, fromDate, toDate, acct_created_at);
            let encrypt_loginName = await HashHelper.encrypt(loginName);
            await BasiqTransactionHelper.SyncTransaction(transactions, encrypt_loginName, lookup, stList, loginName, quizResponse, encrypt_userId, first_name, isDailySync);
        }));

        return;
    }

    static getrelevantAccountsAndUsers = async(accounts, relevantUsers) => {
        const relevantAccountsAndUsers = [];
        await Promise.all(accounts.map(async(account)=> {
            const decryptId = await HashHelper.decrypt(account.acc_user_id);
            const accountUser = relevantUsers.find(user => decryptId === user.id);
            if(accountUser){
                relevantAccountsAndUsers.push(
                    {
                        account,
                        user: accountUser
                    }
                );
            }
        }));

        return relevantAccountsAndUsers;
    }

    static getUserBankList = async(userId) => {
        let banks = await getRepository(Account).createQueryBuilder("acc").select("acc.bank_code, acc.created_at, acc.user_id").orderBy("acc.created_at", "ASC").getRawMany();
            banks.sort((a, b) => Number(a.created_at) - Number(b.created_at));

        const bankList = [];

        for(let i = 0; i < banks.length; i++) {
            try {
                let decryptId = await HashHelper.decrypt(banks[i].user_id);
                if(decryptId === userId) {
                    bankList.push({ 'bank_code': banks[i].bank_code, 'created_at': banks[i].created_at });
                }
            } catch (e) {
                continue;
            }
        }

        return bankList;
    }
    
    static syncRawTransactions = async(ms, response, user, jobId, fcm_token, accounts, fromDate, toDate, lookup, first_name, encrypt_loginName, encrypt_userId, stList, acAr, quizResponse) => {
        const api = new GlobalBasiqHelper();

        const synctransactions = async (ms) => {
            return new Promise((resolve, reject) => {
                const interval = setInterval(async () => {
                    const job = await api.getJob(user.basiq_id, jobId);

                    for(let j = 0; j < job.steps.length; j++) {
                        if(job.steps[j].title.toString() === 'retrieve-transactions') {

                        const status = job.steps[j].status.toString(); // set the job status

                        if (status === 'failed') {

                            clearInterval(interval);
                            resolve(0);

                            await BasiqHelper.trackInMixPanel(job, j, user);

                            await BasiqHelper.notifyDevice(user, fcm_token);

                            // delete inserted accounts from the beginning if has
                            await getConnection().createQueryBuilder().delete().from(Account).where("account_id IN (:...account_ids)", { account_ids: accounts.split(",") }).execute();

                            return response.send({
                                'status': 'ok',
                                'message': 'Unable to proceed for fetching transactions due to bank link error!',
                                'error': job
                            });

                        } else if (status === 'success') {

                            // Clear the interval
                            clearInterval(interval);

                            // Map through accounts
                            await BasiqHelper.saveTransactionsFromAccounts(acAr, fromDate, toDate, encrypt_loginName, lookup, stList, user, quizResponse, encrypt_userId, first_name);

                            // End the loop
                            resolve(0);

                        }

                        break;

                        }
                    }
                }, ms);
            });
        }

        await synctransactions(ms);

        return;
    }

    static saveTransactionsFromAccounts = async(acAr, fromDate, toDate, encrypt_loginName, lookup, stList, user, quizResponse, encrypt_userId, first_name) => {
        const api = new GlobalBasiqHelper();

        await Promise.all(acAr.map(async (ac) => {

            // Get first page
            const transactions_list = await api.syncTransactionsByAccountId(user.basiq_id, ac.account_id, false, fromDate, toDate);
            const transactions =  transactions_list.data.data;
            // await TransactionHelper.SyncInitialTransactionV2(transactions, encrypt_loginName, lookup, user.basiq_id, stList, encrypt_userId, first_name);
            await BasiqTransactionHelper.SyncTransaction(transactions, encrypt_loginName, lookup, stList, user.basiq_id, quizResponse, encrypt_userId, first_name, false);

            // Get additional pages
            let next = transactions_list.data.links.next;

            while (next !== undefined) {
                let transactions_next = await api.syncTransactionsByAccountId(user.basiq_id, ac.account_id, next, fromDate, toDate);
                let nextLink = transactions_next.data.links.next;

                // await TransactionHelper.SyncInitialTransactionV2(transactions_next.data.data, encrypt_loginName, lookup, user.basiq_id, stList, encrypt_userId, first_name);
                await BasiqTransactionHelper.SyncTransaction(transactions, encrypt_loginName, lookup, stList, user.basiq_id, quizResponse, encrypt_userId, first_name, false);

                next = nextLink;
            }

        }));
    }

    static trackInMixPanel = async(job, j, user) => {
        const event_name = 'Bank Linking Failed';
        const data = {
            'distinct_id': user.encrypted_id,
            'first_name': user.first_name,
            'detail': job.steps[j].result,
            'data': {
                'job': job
            }
        }

        MixpanelHelper.track(event_name, data);

        return;
    }

    static notifyDevice = async(user, fcm_token) => {
        const payload = {
            notification: {
                title: "Bank Linking Error",
                body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
            },
            data: {
                user_id: ''+user.id+'',
                status: '1',
                message: 'failed',
                isInitialTransaction: '1'
            }
        };

        const options = {
            priority: "normal",
            timeToLive: 60 * 60
        };

        FirebaseHelper.notifyDevice(fcm_token, payload, options);

        return;
    }

    static distinctOnBankCode = async(bankList) => {

        const distinctOnBankCode = bankList.reduce((acc, current) => {
            const x = acc.find(item => item.bank_code === current.bank_code);
            if (!x) {
                return acc.concat([current]);
            } else {
                return acc;
            }
        }, []);

        return distinctOnBankCode;
    }
}