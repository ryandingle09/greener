import { getRepository} from "typeorm";
import { MixpanelHelper } from '../../../helpers/mixpanel';
import { BasiqHelper } from "../../../helpers/basiq";
import { FirebaseHelper } from "../../../helpers/firebase";
import { HubSpotHelper } from "../../../helpers/hubspot";
import { Job } from "../../../models/Job";

const options = {
    priority: "normal",
    timeToLive: 60 * 60
};

export class JobHelper{

    static checkConnection = async(user, connection_id, jobId, fcm_token) => {
        const api = new BasiqHelper();

        let count = 0;

        setInterval(async function() {
            count += 1;

            const check = await api.getConnection(user.basiq_id, connection_id);
            const job = await api.getJob(user.basiq_id, jobId);

            if(job.steps[1].status.toString() === 'success' && job.steps[2].status.toString() === 'success') {
                clearInterval(this);

                const payload = payloadData("Finish set-up now to discover your Rating", "Success! Your bank is now connected.", check, '1', 'success');
                const event_name = 'Bank Linking Success';
                const data = notifyMixpanel(user, job, check);

                await MixpanelHelper.track(event_name, data);

                await FirebaseHelper.notifyDevice(fcm_token, payload, options);
            }

            // maximum retry count
            if(count === 3 && job.steps[1].status.toString() != 'success' && job.steps[2].status.toString() != 'success') {
                clearInterval(this);

                const event_name = 'Bank Linking Failed';
                const data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'data': {
                        'job': job,
                        'connection': check
                    }
                }

                MixpanelHelper.track(event_name, data);

                try{
                    const jobRes = job.steps;
                    await checkJobSteps(jobRes, check);
                    
                } catch (e) {
                    // do nothing
                }
            }

            if(count > 3) {
                clearInterval(this);
            }
        }, (1000 * 60) * 0.1); // 10 minutes total of 30 minutes

        function payloadData(title, body, check, status, message) {
            return {
                notification: {
                    title: title,//"Finish set-up now to discover your Rating",
                    body: body//"Success! Your bank is now connected."
                },
                data: {
                    user_id: ''+user.id+'',
                    institution_id: ''+check.institution.id+'',
                    status: ''+status+'',
                    message: ''+message+''
                }
            }
        }

        function notifyMixpanel(user, job, check) {
            return {
                'distinct_id': user.encrypted_id,
                'first_name': user.first_name,
                'data': {
                    'job': job,
                    'connection': check
                }
            }
        }

        async function checkJobSteps(jobRes, check) {
            for(let j = 0; j < jobRes.length; j++) {
                if(jobRes[j].result.code === 'invalid-credentials') {

                    const payload = payloadData("Opps!!! Invalid Bank Login Credentials", "Looks like you've put invalid Bank Login credentials!, Please try again to Link your Bank Account.", check, '0', 'falied');
                    FirebaseHelper.notifyDevice(fcm_token, payload, options);

                } else {
                    const bankDetail = await api.getInstitution(user.basiq_id, check.institution.id);
                    const properties = [];
                    const payload = payloadData("We'll just try connecting again in 24 hours.", "Looks like your bank outage is still going. There's nothing you need to do. We've got it covered!", check, '0', 'falied');

                    properties.push({ property: 'g_basiq_user_id', value: user.basiq_id });
                    properties.push({ property: 'g_basiq_connection_id', value: connection_id });
                    properties.push({ property: 'g_basiq_job_id', value: jobId });
                    properties.push({ property: 'g_basiq_connection_error_bank_name', value: bankDetail.name });
                    properties.push({ property: 'g_basiq_connection_error_trigger', value: 'TRUE' });

                    const hub = new HubSpotHelper();
                    let hubReturn;
                    const hubsObject = { properties: properties }

                    try{
                        hubReturn = await hub.updateContact(user.hubspot_id, hubsObject);
                    } catch(e) {
                        // do nothing, usually happen in dev test if user is not yet there
                    }

                    const newJob = new Job();
                        newJob.user_id = user.id;
                        newJob.connection_id = connection_id;
                        newJob.job_id = jobId;
                        newJob.fcm_token = fcm_token;
                        newJob.step = 1;

                    await getRepository(Job).save(newJob);

                    FirebaseHelper.notifyDevice(fcm_token, payload, options);
                }
            }
        }
    }

}