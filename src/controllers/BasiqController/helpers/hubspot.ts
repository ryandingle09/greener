import { getConnection, getManager, getRepository } from "typeorm";
import { RatingHistory } from "../../../models/RatingHistory";
import { Store } from "../../../models/Store";
import { BasiqHelper as BasiqGlobalHelper } from "../../../helpers/basiq";
import { HubSpotHelper as GlobalHubSpotHelper } from "../../../helpers/hubspot";

export class HubspotHelper{

    static poPulateUserDataUponbankLink = async(properties, user, loginName, distinctOnBankCode, fromDate_90, toDate_90, halo) => {
        const hub = new GlobalHubSpotHelper();
        const api = new BasiqGlobalHelper();

        properties = await HubspotHelper.synBankDetailsInHubspot(distinctOnBankCode, loginName, properties);
        properties.push({ property: 'g_greener_rating_current', value: halo.rating.green.toFixed(2) });

        try{
            const contactDetails = await hub.getContactbyId(user.hubspot_id);
            const first_rating = contactDetails.properties.g_greener_rating_when_first_connected_bank.value;

            if(first_rating === null || first_rating === undefined || first_rating === '') {
                properties = await HubspotHelper.setStoreDetailUponFirstBankLink(properties, loginName);
                properties.push({ property: 'g_greener_rating_when_first_connected_bank', value: halo.rating.green.toFixed(2) });
            }

        } catch(e) {
            properties = await HubspotHelper.setStoreDetailUponFirstBankLink(properties, loginName);
            properties.push({ property: 'g_greener_rating_when_first_connected_bank', value: halo.rating.green.toFixed(2) });
        }

        const greener_stores = await getRepository(RatingHistory)
            .createQueryBuilder("calc")
            .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id and calc.store_name != ''", { id: loginName })
            .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect("stores.sector", "sector")
			.leftJoinAndSelect("stores.subsectors", "subsectors")
			.leftJoinAndSelect("subsectors.info", "subsector_info")
            .orderBy("calc.spend", "DESC")
            .limit(3)
            .getRawMany();

        greener_stores.sort((a, b) => Number(b.calc_spend) - Number(a.calc_spend));

        // for greener store name and logo
		const storePositions = ['','second_','third_'];
        properties = await HubspotHelper.setGreenerStoresInHubspot(greener_stores, storePositions, properties);

        const hubsObject = { properties: properties }

        try {
             await hub.updateContact(user.hubspot_id, hubsObject);
        } catch(e) {
             console.log(e);
        }
    }

    static setGreenerStoresInHubspot = async(greener_stores, storePositions, properties) => {
        for(let i = 0; i < greener_stores.length; i++) {
			properties.push({ property: `g_greener_store_name_${storePositions[i]}highest_CO2_avoided_previous_90_days`, value: greener_stores[i].calc_store_name }); 
			properties.push({ property: `g_greener_store_logo_${storePositions[i]}highest_CO2_avoided_previous_90_days`, value: greener_stores[i].stores_logo }); 
		}

        const check = (property) => {
            return properties.some(function(el) {
                return el.property === property;
            });
        }

        for(let sp = 0; sp < storePositions.length; sp++) {    
			if(check('g_greener_store_name_'+storePositions[sp]+'highest_CO2_avoided_previous_90_days') === false) 
				properties.push({ property: `g_greener_store_name_${storePositions[sp]}highest_CO2_avoided_previous_90_days`, value: '' });
			if(check('g_greener_store_logo_'+storePositions[sp]+'highest_CO2_avoided_previous_90_days') === false) 
				properties.push({ property: `g_greener_store_logo_${storePositions[sp]}highest_CO2_avoided_previous_90_days`, value: '' });
        }

        return properties;
    }

    static setStoreDetailUponFirstBankLink = async(properties, loginName) => {
        // for first greener store purchase
        const first_greener_store = await getRepository(RatingHistory)
            .createQueryBuilder("calc")
            .where("calc.user_id = :id", { id: loginName })
            .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
            .groupBy("calc.id")
            .addGroupBy("stores.id")
            .orderBy("calc.calculation_date", "ASC")
            .getRawOne();

        if(first_greener_store !== undefined) {
            properties.push({ property: 'g_greener_store_first_purchase', value: first_greener_store.calc_store_name });
            properties.push({ property: 'g_greener_store_logo_first_purchase', value: first_greener_store.stores_logo });
        }

        return properties;
    }

    static synBankDetailsInHubspot = async(distinctOnBankCode, loginName, properties) => {
        const api = new BasiqGlobalHelper();
        const positions = ['','_02','_03'];

        for(let i = 0; i < distinctOnBankCode.length; i++) {
            const bankDetail = await api.getInstitution(loginName, distinctOnBankCode[i].bank_code);

			properties.push({ property: `g_connected_bank_account_name${positions[i]}`, value: bankDetail.name }); 
			properties.push({ property: `g_connected_bank_account_logo${positions[i]}`, value: bankDetail.logo.links.square }); 
		}

        return properties;
    }

}