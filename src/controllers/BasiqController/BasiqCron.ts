import {getConnection, getManager} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { Account } from "../../models/Account";
import { FirebaseHelper } from "../../helpers/firebase";
import { getRepository } from "typeorm";
import { User } from "../../models/User";
import { Transaction } from "../../models/Transaction";
import { Classification } from "../../models/Classification";
import { Store } from "../../models/Store";
import { StoreAddress } from "../../models/StoreAddress";
import { StoreNonGreener } from "../../models/StoreNonGreener";
import { HashHelper } from "../../helpers/hasher";
import { Helper as GlobalHelper } from "../../helpers/helper";
import { GoogleHelper } from '../../helpers/google';
import { RatingHistory }  from '../../models/RatingHistory';
import { DailyFailedJob }  from '../../models/DailyFailedJob';
import { HubSpotHelper } from "../../helpers/hubspot";
import { IsNull, Not } from "typeorm";
import { RatingHelper } from "../../helpers/rating";
import * as AWS from 'aws-sdk';
import fetch from 'node-fetch';
import { BasiqHelper as BasiqTransactionHelper } from "./helpers/basiq"; 

const svgToImg = require("svg-to-img");
const moment = require('moment');
const env = process.env;

export class BasiqCron {
    
    static syncDailyTransactions = async(request: Request, response: Response, next: NextFunction) => {
        // for override dates - if needed to sync with specific date ranges
        const from:any = request.query.from ? request.query.from : false;
        const to:any = request.query.to ? request.query.to : false;

        let fromDate = GlobalHelper.getDateRange(7).fromDate;
        let toDate = GlobalHelper.getDateRange(7).toDate;

        if(from !== false && to !== false) {
            fromDate = from;
            toDate = to;
        }

        // get users
        let users = await getRepository(User).find({ select: ["basiq_id","id","encrypted_id","first_name"],where: { basiq_id: Not(IsNull()) }});

        // remove transaction from the given date range to avoid double insertion in case this has been called multiple times, and reinsert it
        await getConnection().createQueryBuilder().delete().from(RatingHistory).where("calculation_date BETWEEN '"+fromDate+" 00:00:00' and '"+toDate+" 23:59:59'").execute();

        // get lookup table data for rating computation
        const lookup = await getRepository(Classification).createQueryBuilder("lookup").getRawMany();

        // get store list
        const stList = await BasiqTransactionHelper.getStoreList();

        // get accounts
        let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        // Get relevant uses
        const relevantUsers: any[] = users.filter(user => {return user.basiq_id !== null});

        // Prepare relevant accounts with user
        const relevantAccountsAndUsers = await BasiqTransactionHelper.getrelevantAccountsAndUsers(accounts, relevantUsers);
        
        // Save transactions
        await BasiqTransactionHelper.saveTransaction(relevantAccountsAndUsers, lookup, stList, fromDate, toDate);

        //set celebrated=true for past greener transactions that has been celebrated already
        await RatingHelper.setCelebrated();

        //========================================================================
        // WIP do not remove ------------------------------------------------
            // let jobList = await api.getJobs(loginName);
            
            // if(jobList === undefined) continue;
            // if(jobList.size === 0) continue;

            // // check each job for each bank connected
            // for(let j in jobList['data']) {
            //     let institution = jobList['data'][j].institution.id;
            //     let bank_name = await api.getInstitution(loginName, institution);
            //     let jobId = jobList['data'][j].id;

            //     // default 7 days jobs will get and need to loop each job not just 1, user has 1 or more bank accounts link
            //     for(let s in jobList['data'][j].steps) {
            //         let status = jobList['data'][j].steps[s].status.toString();

            //         if(jobList['data'][j].steps[s].title.toString() === 'verify-credentials') {
            //             if(status === 'failed') {
            //                 await getConnection()
            //                     .createQueryBuilder()
            //                     .insert()
            //                     .into(DailyFailedJob)
            //                     .values([{ 'bank_name': bank_name, 'job_id' : jobId,  'user_id' : users[u].id,'details': jobList['data'][j] }])
            //                     .execute();

            //                 let event_name = 'Daily Cron create connection error';
            //                 let data = {
            //                     'distinct_id': users[u].encrypted_id,
            //                     'first_name': users[u].first_name,
            //                     'detail': jobList['data'][j].steps[s].result,
            //                     'data': {
            //                         'job': jobList['data'][j]
            //                     }
            //                 }

            //                 MixpanelHelper.track(event_name, data);

            //                 break;
            //             }
            //         }
            //     }
            // }
        //-------------------------------------------------------------------------

        return response.send({'status': 'ok'});
    }

    static syncNonGreenerStores = async(request: Request, response: Response, nextf: NextFunction) => {
        await getConnection().query(`DELETE FROM ${env.TYPEORM_SCHEMA}.stores_non_greener WHERE id IN(SELECT id FROM(SELECT  id,ROW_NUMBER() OVER(PARTITION BY "name" ORDER BY id) AS row_num FROM greener.stores_non_greener) t WHERE t.row_num > 1);`);

        let greeer = await getRepository(Store)
                .createQueryBuilder("s")
                .select("s.name")
                .getMany();
        let existingStores = [];

        for(let s = 0; s < greeer.length; s++) {
            existingStores.push(greeer[s].name.toLowerCase());
        }

        let non_greeer = await getRepository(StoreNonGreener)
                .createQueryBuilder("s")
                .select("s.name")
                .getMany();

        for(let s = 0; s < non_greeer.length; s++) {
            existingStores.push(non_greeer[s].name.toLowerCase());
        }

        let stores = await getRepository(Transaction)
                .createQueryBuilder("t")
                .select("t.merchant_name, t.logo_master")
                .distinctOn(["t.merchant_name"])
                .where("t.merchant_name != '' or t.merchant_name IS NOT NULL or t.logo_master IS NOT NULL and lower(t.merchant_name) NOT IN (:...greeer)", {greeer: existingStores})
                .getRawMany();

        let storesFoundLogo = [];

        for(let s = 0; s < stores.length; s++) {
            let logo = stores[s].logo_master;

            if(logo !== null && logo.toString().endsWith(".svg") === true) {
                let url = logo;
                let name = stores[s].merchant_name.toString().toLowerCase().replace(/\s/g, "-");

                await fetch(url).then(function (response) {
                    // The API call was successful!
                    return response.text();
                }).then(function (html) {
                    (async () => {
                        const image = await svgToImg.from(html).toPng();

                        AWS.config.update({
                            accessKeyId: env.AWS_ACCESS_KEY,
                            secretAccessKey: env.AWS_SECRET_KEY,
                            region: env.AWS_REGION
                        });

                        let s3 = new AWS.S3();

                        // Setting up S3 upload parameters
                        let params = {
                            Bucket: 'greener-wallet',
                            Key: 'images/'+name+'.png',
                            Body: image,
                            ContentType: 'image/png',
                        };

                        // Uploading files to the bucket
                        s3.upload(params, async(err, data) => {
                            if (err) {
                                console.log(err);
                            }
                        });
                        console.log(image);
                    })();
                }).catch(function (err) {
                    // ignore this
                });

                logo = "https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/"+name+".png";
            }

            storesFoundLogo.push({
                'name': stores[s].merchant_name,
                'full_address': stores[s].merchant_formatted_address,
                'latitude': stores[s].merchant_geometry_lan,
                'longitude': stores[s].merchant_geometry_lon,
                'logo': logo
            });
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreNonGreener)
            .values(storesFoundLogo)
            .execute();

        return response.send({'status': 'ok'});
    }

    static syncPriceLevel = async(request: Request, response: Response, nextf: NextFunction) => {
        let entityManager = getManager();
        let stores = await getRepository(Store)
            .createQueryBuilder("Store")
            .distinctOn(["Store.name"])
            .leftJoinAndSelect("Store.storeaddresses", "address")
            .getRawMany();

        let queryStr = ` `;
        let sets = [];

        for(let s in stores) {

            let price_level = 0;
            let api = new GoogleHelper();
            let result = await api.getPriceLevel(''+stores[s].Store_name+' '+stores[s].address_address+'');

            try {
                price_level = result.candidates[0].price_level ? result.candidates[0].price_level : 0;
            } catch (e){
                price_level = 0;
            }

            sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET "rating"=${price_level} WHERE id='${stores[s].Store_id}' `);
        }

        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        await entityManager.query(finalStr.toString());

        return response.send({'status': 'ok'});
    }

    static syncStoreAddress = async(request: Request, response: Response, nextf: NextFunction) => {
        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today);
        yesterday.setDate(yesterday.getDate() - 1);
        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);

        let stores = await getRepository(Store)
            .createQueryBuilder("Store")
            .distinctOn(["Store.name"])
            .getRawMany();

        let seed = [];
        let count = 0;
        for(let s = 0; s < stores.length; s++) {
            count += 1;
            let text_search = ''+stores[s].Store_name+' in Australia AU, AUS';
            let api = new GoogleHelper();
            let result = await api.getStoreAddresses(text_search);
            let addresses =  result.results;
            let next = result.next_page_token;
            let status = result.status;

            while (next !== undefined){
                let result_next = await api.getStoreAddressesNext(text_search, next);
                let nextLink = result_next.next_page_token;
                status = result_next.status;
                addresses = addresses.concat(result_next.results);

                if(result_next.results.length !== 0) {
                    next = nextLink;
                }

                if(status === 'ZERO_RESULTS') {
                    next = nextLink;
                }
            }

            for(let a = 0; a < addresses.length; a++) {
                seed.push({
                    'storeId': stores[s].Store_id,
                    'address': addresses[a].formatted_address,
                    'latitude': addresses[a].geometry.location.lat,
                    'longitude': addresses[a].geometry.location.lng
                });
            }
        }

        //save new
        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreAddress)
            .values(seed)
            .execute();

        //delete new
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(StoreAddress)
            .where("created_at < :date", { date: last })
            .execute();

        return response.send({'status': 'ok','count': seed.length, 'data': seed});
    }

    static HSGreenerRating =  async(request: Request, response: Response, nextf: NextFunction) => {
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;
        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const today_90 = GlobalHelper.formatDate(new Date());
        const users = await getRepository(User).find({select: ["id", "basiq_id","id","hubspot_id"] });

        for(let u = 0; u < users.length; u++) {
            try {
                let loginName = users[u].basiq_id;
                let hubsPotId = users[u].hubspot_id;
                let properties = [];

                if(loginName === null || hubsPotId === null){
                    // skipping null basic IDS
                    continue;
                }

                let halo_90 = await RatingHelper.ThreeMonths(loginName);

                // daily greener rating update
                properties.push({ property: 'g_greener_rating_current', value: halo_90.rating.green.toFixed(2) });

                // get user accounts
                let accountArray = [];
                let accounts = await getRepository(Account).createQueryBuilder("acc").orderBy('created_at','ASC').getRawMany();

                for(let i in accounts) {
                    try {
                        let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                        if(decryptId === users[u].id) {
                            accountArray.push({'created_at': accounts[i].acc_created_at, 'account_id': accounts[i].acc_account_id});
                        }
                    } catch (e) {
                        continue;
                    }
                }

                if(accountArray.length !== 0) {
                    let count = 0;
                    for(let ac in accountArray) {
                        count += 1;
                        if(count !== 1) continue;

                        let created_at = accountArray[ac].created_at;
                            created_at = new Date(created_at);
                            created_at = created_at.getFullYear()+'-'+("0"+(created_at.getMonth()+1)).slice(-2)+'-'+("0"+(created_at.getDate())).slice(-2);

                        let a1 = moment([today_90.slice(0,4), today_90.slice(6,7), today_90.slice(9,10)]);
                        let a2 = moment([created_at.slice(0,4), created_at.slice(5,7), created_at.slice(8,10)]);

                        let adiff = a1.diff(a2, 'days');
                        console.log("adiff: ", adiff);

                        // making sure that this will run once just after exact 7 days
                        if(adiff === 8) {
                            properties.push({ property: 'g_greener_rating_after_7_days', value: halo_90.rating.green.toFixed(2) });
                        }

                        // making sure that this will once run just after exact 30 days
                        if(adiff === 31) {
                            properties.push({ property: 'g_greener_rating_after_30_days', value: halo_90.rating.green.toFixed(2) });
                        }
                    }
                }

                let greener_stores = await getRepository(RatingHistory)
                    .createQueryBuilder("calc")
                    .distinctOn(["calc.store_name"])
                    .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id", { id: loginName })
                    .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
                    .groupBy("calc.id")
                    .addGroupBy("stores.id")
                    .orderBy("calc.store_name")
                    .addOrderBy("calc.spend", "DESC")
                    .limit(3)
                    .getRawMany();

                greener_stores.sort((a, b) => Number(b.calc_spend) - Number(a.calc_spend));

                // for greener store name and logo
                let count = 0;
                for(let i in greener_stores) {
                    count += 1;

                    if(count === 3){
                        properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }

                    if(count === 2){
                        properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }

                    if(count === 1){
                        properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }
                }

                let check = (property) => {
                    return properties.some(function(el) {
                        return el.property === property;
                    });
                }

                if(check('g_greener_store_name_third_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_third_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_name_second_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_second_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_name_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: '' });

                let hub = new HubSpotHelper();
                let hubReturn;
                let hubsObject = { properties: properties }

                try{
                    hubReturn = await hub.updateContact(hubsPotId, hubsObject);
                } catch(e) {
                    // do nothing, usually happen in dev test if user is not yet there
                }
            } catch (e) {
                // do nothing, to not stop the execution
            }
        }

        return response.send({'status': 'ok'});
    }

    static HSGreenerWeeklyRating =  async(request: Request, response: Response, nextf: NextFunction) => {
        // dates for past week
        const toDate = GlobalHelper.getDateRange(6).toDate;
        const fromDate = GlobalHelper.getDateRange(6).fromDate;
        const users = await getRepository(User).find({select: ["id", "basiq_id","id","hubspot_id"] });

        for(let u = 0; u < users.length; u++) {
            try {
                const loginName = users[u].basiq_id;
                const hubsPotId = users[u].hubspot_id;

                if(loginName === null || hubsPotId === null){
                    // skipping null basic IDS
                    continue;
                }

                const halo_90 = await RatingHelper.ThreeMonths(loginName);
                const halo_7 = await RatingHelper.PastSevenDays(loginName);
                const change_over_last_week = halo_90.rating.green - halo_7.rating.green;

                await RatingHelper.setHubsPotTopThreeMerchants(loginName, hubsPotId, change_over_last_week, fromDate, toDate);
                
            } catch (e) {
                console.log("error on loop: ", e);
            }
        }

        return response.send({'status': 'ok'});
    }

    static notifyUsersNeedBankRelogin = async(request: Request, response: Response, next: NextFunction) => {
        const failed_list = await getRepository(DailyFailedJob).createQueryBuilder("DailyFailedJob").getMany();

        for(let ujl = 0; ujl < failed_list.length; ujl++) {
            const user_id = failed_list[ujl].user_id;
            const user = await getRepository(User).findOne(user_id);
            const devices = user.device_ids;

            const payload = {
                notification: {
                    title: "Bank Linking Error",
                    body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
                },
                data: {
                    user_id: ''+user_id+'',
                    status: '1',
                    message: 'failed',
                    isNeedToReAuthenticate: '1'
                }
            };

            const options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            if(devices !== null) {
                if(devices['android'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['android'], payload, options);
                }

                if(devices['ios'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                }
            }
        }

        return response.send({'status': 'ok'});
    }

    static convertSVGtoPNGImages = async(request: Request, response: Response, next: NextFunction) => {
        await getConnection().query(`DELETE FROM ${env.TYPEORM_SCHEMA}.stores_non_greener WHERE id IN(SELECT id FROM(SELECT  id,ROW_NUMBER() OVER(PARTITION BY "name" ORDER BY id) AS row_num FROM greener.stores_non_greener) t WHERE t.row_num > 1);`);

        const non_greener_stores = await getRepository(StoreNonGreener).createQueryBuilder("stores").where("logo like '%.svg'").getMany();
        const queryStr = ` `;
        const listToUpdate = [];

        for(let i = 0; i < non_greener_stores.length; i++) {
            const url = non_greener_stores[i].logo;
            const name = non_greener_stores[i].name.toString().toLowerCase().replace(/\s/g, "-");

            await fetch(url).then(function (response) {
                // The API call was successful!
                return response.text();
            }).then(function (html) {
                (async () => {
                    const image = await svgToImg.from(html).toPng();

                    AWS.config.update({
                        accessKeyId: env.AWS_ACCESS_KEY,
                        secretAccessKey: env.AWS_SECRET_KEY,
                        region: env.AWS_REGION
                    });

                    const s3 = new AWS.S3();

                    // Setting up S3 upload parameters
                    const params = {
                        Bucket: 'greener-wallet',
                        Key: 'images/'+name+'.png',
                        Body: image,
                        ContentType: 'image/png',
                    };

                    // Uploading files to the bucket
                    s3.upload(params, async(err, data) => {
                        if (err) {
                            console.log(err);
                        }
                    });
                })();
            }).catch(function (err) {
                // There was an error
            });

            const logo = "https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/"+name+".png";
            listToUpdate.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores_non_greener SET "logo"='${logo}' WHERE id::uuid='${non_greener_stores[i].id}'`);
        }

        const finalList = listToUpdate.join(';');
        const finalStr = queryStr+finalList;

        await getConnection().query(finalStr.toString());

        return response.send({
            'status': 'ok',
            'message': 'All svg image has been coverted into png and has been uploaded to s3 buket.'
        });
    }

}