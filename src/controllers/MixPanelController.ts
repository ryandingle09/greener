import {NextFunction, Request, Response} from "express"
import { getRepository } from "typeorm";
import { User } from "../models/User";
import * as jwt from "jsonwebtoken";
import { MixpanelHelper } from '../helpers/mixpanel';
import { RatingHelper } from "../helpers/rating";

const env = process.env;

export class MixPanelController {

    static trackEvent = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { event_name, data } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
		let userRepository = getRepository(User);
		let user = await userRepository.findOne(userId);

        const halo = await RatingHelper.ThreeMonths(user.basiq_id);

        //set
        let mixData = {};
        mixData['first_name'] = user.first_name;
        mixData['is_onboarded'] = user.is_onboarded;
		// set user alias
        MixpanelHelper.set(user.encrypted_id, mixData);

        data['distinct_id'] = user.encrypted_id;
        data['first_name'] = user.first_name;
        data['Registration date'] = user.created_at;
        data['environment'] = env.ENVIRONMENT;
        data['greenerRating'] = halo.rating.green;
        MixpanelHelper.track(event_name, data);

        let tracked_item = {
            'status': 'ok',
            'message': 'event has been tracked to mixpanel',
            'event_name': event_name,
            'data': data
        };

        return response.send(tracked_item);
    }

    static setBranchUserProfileEvent = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { feature, channel, campaign } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
		let userRepository = getRepository(User);
		let user = await userRepository.findOne(userId);

        //set
        let mixData = {};
        mixData['branch_acquisition_feature'] = feature;
        mixData['branch_acquisition_channel'] = channel;
        mixData['branch_acquisition_campaign'] = campaign;
		// set user alias
        MixpanelHelper.set_once(user.encrypted_id, mixData);

        let tracked_item = {
            'status': 'ok',
            'message': 'event has been tracked to mixpanel user profile',
            'data': request.body
        };

        return response.send(tracked_item);
    }

}