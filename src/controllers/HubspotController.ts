import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { HubSpotHelper } from "../helpers/hubspot";
import { User } from "../models/User";
import * as jwt from "jsonwebtoken";

const env = process.env;

export class HubspotController {

    static AppOpen = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);
        let hub = new HubSpotHelper();

        //get contact
        let contactDetails;
        let total_tally:any = false;
        let properties = [];

        try{
            contactDetails = await hub.getContactbyId(user.hubspot_id);
            total_tally = parseInt(contactDetails.properties.g_greener_app_opens_total_tally.value);
            total_tally += 1;
        } catch(e) {
            let er = {
                'error': e,
                'message': 'Upon get.'
            }

            total_tally = 1;
            console.log(er);
        }

        console.log(total_tally);

        properties.push({ property: 'g_greener_app_opens_total_tally', value: total_tally });
        properties.push({ property: 'g_greener_app_opens_most_recent_date', value: new Date() });
        properties.push({ property: 'greener_app_opens_trigger', value: 'TRUE ' });

        let hubReturn;
        let hubsObject = { properties: properties }

        try{
            hubReturn = await hub.updateContact(user.hubspot_id, hubsObject);
        } catch(e) {
            let er = {
                'error': e,
                'message': 'Upon save'
            }

            console.log(er);
        }

        return response.send({
            'status': 'ok'
        });
    }

    static setBranchUserProfileEvent = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { feature, channel, campaign } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        const { userId } = jwtPayload;
        const repo = getRepository(User);
        const user = await repo.findOne(userId);
        let hub = new HubSpotHelper();
        let properties = [];
        
        const props = ['g_app_member_acquisition_feature','g_app_member_acquisition_channel','g_app_member_acquisition_campaign'];
        const propsToCheck = [
            {'g_app_member_acquisition_feature': feature},
            {'g_app_member_acquisition_channel': channel},
            {'g_app_member_acquisition_campaign': campaign}
        ];

        try{
            const contactDetails = await hub.getContactbyId(user.hubspot_id);

            for(let p in props) {
                const checkProps = contactDetails.properties[props[p]].value;

                if(checkProps === null || checkProps === undefined || checkProps === '')
                    properties.push({ property: props[p], value: Object.values(propsToCheck[p])[0] });
            }

        const hubsObject = { properties: properties }
        await hub.updateContact(user.hubspot_id, hubsObject);

        } catch(e) {
            // normally happens in dev test if user is not exists.
            // to avoid code stopping
        }

        return response.send({
            'status': 'ok'
        });
    }

}