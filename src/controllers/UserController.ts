import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../models/User";

export class UserController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(User);
        let data = await repo
            .createQueryBuilder("user")
            .orderBy("user.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let search = request.query.query ? request.query.query : '';
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(User);
        let data = await repo
            .createQueryBuilder("user")
            .orWhere("user.first_name like :first_name", { first_name:`%${search}%` })
            .orWhere("user.last_name like :last_name", { last_name:`%${search}%` })
            .orWhere("user.email like :email", { email:`%${search}%` })
            .orWhere("user.username like :username", { username:`%${search}%` })
            .orderBy("user.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static create = async(request: Request, response: Response, next: NextFunction) => {
        
        let { password, email, first_name, last_name } = request.body;
        let data = new User();

        data.email = email;
        data.first_name = first_name;
        data.last_name = last_name;
        data.password = password;
        data.role = '0';
        data.status = true;
        data.hashPassword();

        const repo = getRepository(User);

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Email already taken."});
        }

        return response.send({"message": "Successfully created."});
    }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);

        try {
            let data = await repo.findOneOrFail(request.params.id);
            return response.send(data);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {
        
        let { first_name, last_name, birth_date, address } = request.body;

        if(!(first_name && last_name && birth_date && address)) {
            return response.status(422).send({"message": "Please make sure first_name, last_name, birth_date, address is presend in the request."});
        }
        
        let repo = getRepository(User);
        let data = await repo.findOne(request.params.id);

        data.first_name = first_name;
        data.last_name = last_name;
        data.birth_date = birth_date;
        data.address = address;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated."});
    }

    static remove = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);
        let data = await repo.findOne(request.params.id);

        try {
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."}); 
        }

        return response.send({"message": "Successfully Deleted."}); 
    }

}