import {getConnection, getManager} from "typeorm";
import {NextFunction, Request, Response} from "express";
import { Account } from "../models/Account";
import { BasiqHelper } from "../helpers/basiq";
import { FirebaseHelper } from "../helpers/firebase";
import { getRepository } from "typeorm";
import { User } from "../models/User";
import { Transaction } from "../models/Transaction";
import { Classification } from "../models/Classification";
import { Store } from "../models/Store";
import { StoreAddress } from "../models/StoreAddress";
import { StoreNonGreener } from "../models/StoreNonGreener";
import { Job } from "../models/Job";
import * as jwt from "jsonwebtoken";
import { HashHelper } from "../helpers/hasher";
import { TransactionHelper } from '../helpers/transaction';
import { GoogleHelper } from '../helpers/google';
import { RatingHistory }  from '../models/RatingHistory';
import { StoreAddressDraft }  from '../models/StoreAddressDraft';
import { DailyFailedJob }  from '../models/DailyFailedJob';
import { HubSpotHelper } from "../helpers/hubspot";
import { JobHelper } from "../helpers/job";
import { MixpanelHelper } from '../helpers/mixpanel';
import { IsNull, Not } from "typeorm";
import { RatingHelper } from "../helpers/rating";
import { QuizHelper } from "../helpers/quiz";
import { logger } from "../helpers/logger";
import * as AWS from 'aws-sdk';
import fetch from 'node-fetch';
import { QuizResponse } from "../models/QuizResponse";

const svgToImg = require("svg-to-img");
const moment = require('moment');
const env = process.env;

export class BasiqController {

    static getToken = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let token;
        try{
            token = await api.generateTokenFrontEnd();
        } catch(e) {
            let er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            let { userId } = jwtPayload;
            let userRepository = getRepository(User);
            let loginName = await userRepository.findOne(userId);

            let event_name = 'Get token error';
            let data = {
                'distinct_id': loginName.encrypted_id,
                'error_data': e
            }

            MixpanelHelper.track(event_name, data);

            return response.status(422).send(er);
        }

        return response.send({"message": "Success", "token": token});
    }

    static createConnection = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let data;
        let { userId } = jwtPayload;
		let userRepository = getRepository(User);
        let loginName = await userRepository.findOne(userId);
        try{
            data = await api.createConnection(loginName.basiq_id);
        } catch(e) {
            let er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            let event_name = 'Create connection error';
            let data = {
                'distinct_id': loginName.encrypted_id,
                'error_data': e
            }

            MixpanelHelper.track(event_name, data);

            return response.status(422).send(er);
        }

        return response.send({"message": "Success", "data": data});
    }

    static getAccounts = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = [];
		let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i in accounts) {
            try {
                let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                if(decryptId === userId) {
                    data.push(accounts[i]);
                }
            } catch (e) {
                continue;
            }
        }

        return response.send(data);
    }

    static getAccountFromBasic = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let api = new BasiqHelper();
		    let userRepository = getRepository(User);
        let loginName = await userRepository.findOne(userId);
        let accounts = await api.syncAccounts(loginName.basiq_id);

        accounts =  accounts.data.data;

        let accounts_arr = [];

        for(let t in accounts) {
            accounts_arr.push({
                'account_id': accounts[t].id,
                'account_name': accounts[t].name,
                'bank_code': accounts[t].institution,
                'connection': accounts[t].connection
            });
        }

        return response.send(accounts_arr);
    }

    static getAccount = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data;

        try{
            data = await getRepository(Account)
                .createQueryBuilder("account")
                .where("account.id = :id", { id: request.params.accountId })
                .getRawMany();
        } catch(e) {
            let er = {
                'message': e.response.statusText,
                'info': e.response.data
            }

            return response.status(422).send(er);
        }

        return response.send(data);
    }

    static deleteAccount = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let accounts_delete:any = request.query.accounts_delete;
        let accounts_selected:any = request.query.accounts_selected;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);
        let devices = user.device_ids;

        // get lookup table data for rating computation
        let lookups = [];
        let repoLookUp = getRepository(Classification);
        let lookup = await repoLookUp.createQueryBuilder("lookup").getMany();

        for(let i in lookup) {
            lookups.push(lookup[i]);
        }

        // get store list
        let stRepo = getRepository(Store);
        let stList = await stRepo.createQueryBuilder("stores").getMany();
        let event_name = 'Bank Linking Selected Account';
        let data = {
            'distinct_id': user.encrypted_id,
            'first_name': user.first_name,
            'accounts_selected': accounts_selected,
            'accounts_delete': accounts_delete
        }

        MixpanelHelper.track(event_name, data);

        logger.log('info', 'accounts_selected: %s', accounts_selected);
        logger.log('info', 'accounts_delete: %s', accounts_delete);

        try {
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Account)
                .where("account_id IN (:...accounts_delete)", { accounts_delete: accounts_delete.split(",") })
                .execute();

            logger.log('info', 'accounts has been deleted: %s', accounts_delete);

            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(RatingHistory)
                .where("user_id = :basiq_id", { basiq_id: user.basiq_id })
                .execute();

            logger.log('info', 'user rating historries has been deleted');

            let acAr = [];
            let accounts = await getRepository(Account)
                .createQueryBuilder("acc")
                .where("acc.account_id IN (:...accounts_selected)", { accounts_selected: accounts_selected.split(",") })
                .getRawMany();

            for(let i in accounts) {
                acAr.push({'account_id': accounts[i].acc_account_id, 'created_at': accounts[i].acc_created_at});
            }

            // get user quiz response for home and energy
            const quizResponse = await QuizHelper.getHomeEnergyResponse(user.id);

            logger.log('info', 'getting user transactions to resync');
            for(let a in acAr) {
                // resync transactions and recalculate by account id
                let trans = getRepository(Transaction);
                let transaction = await trans.createQueryBuilder()
                    .distinctOn(["basiq_id"])
                    .where("account_id = :account_id and transaction_id IS NOT null", { account_id: acAr[a].account_id })
                    .getMany();

                logger.log('info', 'transactions count '+transaction.length+' for account '+acAr[a].account_id+'');

                // bind the account created at date to the transaction
                for(let i in transaction) {
                  transaction[i]['acct_created_at'] = acAr[a].created_at;
                }

                logger.log('info', 'done getting user transactions to resync');

                logger.log('info', 'Start now the resync');
                await TransactionHelper.resyncHaloCalculateTransactions(transaction, user.basiq_id, stList, lookup, quizResponse);
                logger.log('info', 'Done now the resync');
            }

            let payload = {
                notification: {
                    title: "Bank account removed",
                    body: "Your rating will no longer include this account."
                },
                data: {
                    user_id: ''+userId+'',
                    status: '1',
                    message: 'success',
                    isInitialTransaction: '1'
                }
            };

            let options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            if(devices !== null) {
                if(devices['android'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['android'], payload, options);
                }

                if(devices['ios'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                }
            }

            await RatingHelper.syncRatingToHubspotAndMixpanel(user);

        } catch (e) {
            console.log(e);
            logger.log('error', e);
            return response.status(422).send({"message": "No data associated with the given parameter."});
        }

        logger.log('info', 'Successfully Deleted and Resync Your Halo Calculation.');
        return response.send({"message": "Successfully Deleted and Resync Your Halo Calculation."});
    }

    static setJoinAccount = async(request: Request, response: Response, next: NextFunction) => {
        const account_id:any = request.query.account_id;
        const is_join_account:any = request.query.is_join_account;
        const repo = getRepository(Account);

        let data;

        try {
            data = await repo.findOneOrFail({'account_id': account_id});
        } catch (e) {
            return response.status(422).send({"message": "Data not found at the given ID."});
        }

        data.is_join_account = (is_join_account === '1') ? true : false;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data. Please contact software developer."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }
    // this function syncAccounts is not used anymore
    static syncAccounts = async(request: Request, response: Response, next: NextFunction) => {
        let api = new BasiqHelper();

        // get users
        let repo = getRepository(User);
        let users = await repo.find({select: ["basiq_id","id"] });

        // truncate accounts table
        let entityManager = getManager();
        await entityManager.query(`TRUNCATE table ${env.TYPEORM_SCHEMA}."accounts";`);

        // Initilize accounts
        let repo2 = getRepository(Account);

        for(let u in users) {
            let loginName = users[u].basiq_id;

            if(loginName === null){
                continue;
            }

            try{
                let accounts = await api.syncAccounts(loginName);

                accounts =  accounts.data.data;

                console.log(accounts);

                for(let t in accounts) {
                    let data = new Account();

                    data.user_id = users[u].id;
                    data.account_id = accounts[t].id;
                    data.account_name = accounts[t].name;
                    data.bank_code = accounts[t].institution;

                    await repo2.save(data);
                }
            }catch(e) {
                // do nothing
            }
        }

        return response.send('Done');
    }

    static createAccount = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let { account_id, account_name, bank_code } = request.body;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let accountRepository = getRepository(Account);
		    let account = new Account();

        let encrypt_userId = await HashHelper.encrypt(userId);

        account.user_id = encrypt_userId;
        account.account_id = account_id;
        account.account_name = account_name;
        account.bank_code = bank_code;

        try {

          // Check if account entry exists for this account ID
          let accountEntry = await accountRepository.findOne({ account_id: account.account_id });

          if ( accountEntry ) {

            // Update
            accountEntry.user_id = account.user_id;
            accountRepository.save(accountEntry);

          } else {

            // Insert
            accountRepository.save(account);

          }

        } catch (e) {
          return response.sendStatus(422).send({'error': 'The account id might be existing already!', 'message': e});
        }

        return response.send({'message': 'account successfully saved!'});
    }

    static syncUserAccounts = async(request: Request, response: Response, next: NextFunction) => {
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let api = new BasiqHelper();
        let repo2 = getRepository(Account);
		let userRepository = getRepository(User);
        let loginName = await userRepository.findOne(userId);
        let accounts = await api.syncAccounts(loginName.basiq_id);

        accounts =  accounts.data.data;

        for(let t in accounts) {
            let data = new Account();

            data.user_id = loginName.id;
            data.account_id = accounts[t].id;
            data.account_name = accounts[t].name;
            data.bank_code = accounts[t].institution;

            await repo2.save(data);
        }

        return response.send({'message': 'user accounts successfully saved!'});
    }

    static syncDailyTransactions = async(request: Request, response: Response, next: NextFunction) => {
        // for override dates - if needed to sync with specific date ranges
        let from:any = request.query.from ? request.query.from : false;
        let to:any = request.query.to ? request.query.to : false;

        let api = new BasiqHelper();
        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today)
        yesterday.setDate(yesterday.getDate() - 1);

        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);
        let days_90 = new Date(yesterday);
        days_90.setDate(yesterday.getDate() - 7);

        let days_90_last = days_90.getFullYear()+'-'+("0"+(days_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90.getDate())).slice(-2);
        let fromDate:any = days_90_last;
        let toDate:any = last;

        if(from !== false && to !== false) {
            fromDate = from;
            toDate = to;
        }

        // get users
        let repo = getRepository(User);
        let users = await repo.find({
            select: ["basiq_id","id","encrypted_id","first_name"],
            where: { basiq_id: Not(IsNull()) }
        });

        // remove transaction from the given date range to avoid double insertion in case this has been called multiple times, and reinsert it
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(RatingHistory)
            .where("calculation_date BETWEEN '"+fromDate+" 00:00:00' and '"+toDate+" 23:59:59'")
            .execute();

        // get lookup table data for rating computation
        let lookups = [];
        let repoLookUp = getRepository(Classification);
        let lookup = await repoLookUp.createQueryBuilder("lookup").getMany();

        for(let i in lookup) {
            lookups.push(lookup[i]);
        }

        // get store list
        let stRepo = getRepository(Store);
        let stList = await stRepo.createQueryBuilder("stores").getMany();
        let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        //=====================================================================
        // Get relevant uses
        const relevantUsers: any[] = users.filter(user => {
            return user.basiq_id !== null
        });
        // Prepare relevant accounts with user
        const relevantAccountsAndUsers = [];
        await Promise.all(accounts.map(async(account)=> {
            const decryptId = await HashHelper.decrypt(account.acc_user_id);
            const accountUser = relevantUsers.find(user => decryptId === user.id);
            if(accountUser){
                relevantAccountsAndUsers.push(
                    {
                        account,
                        user: accountUser
                    }
                );
            }
        }));
        // Save transactions
        await Promise.all(relevantAccountsAndUsers.map(async(accountAndUser)=>{
            const loginName = accountAndUser.user.basiq_id;
            const accountId = accountAndUser.account.acc_account_id;
            const acct_created_at = accountAndUser.account.acc_created_at;
            const encrypt_userId = accountAndUser.user.encrypted_id;
            const first_name = accountAndUser.user.first_name;
            const quizResponse = await QuizHelper.getHomeEnergyResponse(accountAndUser.user.id);
            const transactions = await getTransactions(loginName, accountId, fromDate, toDate, acct_created_at);
            let encrypt_loginName = await HashHelper.encrypt(loginName);
            await TransactionHelper.SyncDailyTransaction(transactions, encrypt_loginName, lookup, loginName, quizResponse, stList, encrypt_userId, first_name);
        }));
        // Get all transactions
        async function getTransactions(loginName, accountId, fromDate, toDate, acct_created_at) {
            let transactions_list = await api.syncTransactionsByAccountId(loginName, accountId, false, fromDate, toDate);
            let transactions =  transactions_list.data.data;
            let next = transactions_list.data.links.next;
            while (next !== undefined){
                let transactions_next = await api.syncTransactionsByAccountId(loginName, accountId, next, fromDate, toDate);
                let nextLink = transactions_next.data.links.next;
                transactions = transactions.concat(transactions_next.data.data);
                next = nextLink;
            }

            for(let i in transactions) {
                transactions[i]['acct_created_at'] = acct_created_at;
            }
            return transactions;
        }

        //set celebrated=true for past greener transactions that has been celebrated already
        await RatingHelper.setCelebrated();
        //========================================================================

        // WIP ------------------------------------------------
            // let jobList = await api.getJobs(loginName);
            
            // if(jobList === undefined) continue;
            // if(jobList.size === 0) continue;

            // // check each job for each bank connected
            // for(let j in jobList['data']) {
            //     let institution = jobList['data'][j].institution.id;
            //     let bank_name = await api.getInstitution(loginName, institution);
            //     let jobId = jobList['data'][j].id;

            //     // default 7 days jobs will get and need to loop each job not just 1, user has 1 or more bank accounts link
            //     for(let s in jobList['data'][j].steps) {
            //         let status = jobList['data'][j].steps[s].status.toString();

            //         if(jobList['data'][j].steps[s].title.toString() === 'verify-credentials') {
            //             if(status === 'failed') {
            //                 await getConnection()
            //                     .createQueryBuilder()
            //                     .insert()
            //                     .into(DailyFailedJob)
            //                     .values([{ 'bank_name': bank_name, 'job_id' : jobId,  'user_id' : users[u].id,'details': jobList['data'][j] }])
            //                     .execute();

            //                 let event_name = 'Daily Cron create connection error';
            //                 let data = {
            //                     'distinct_id': users[u].encrypted_id,
            //                     'first_name': users[u].first_name,
            //                     'detail': jobList['data'][j].steps[s].result,
            //                     'data': {
            //                         'job': jobList['data'][j]
            //                     }
            //                 }

            //                 MixpanelHelper.track(event_name, data);

            //                 break;
            //             }
            //         }
            //     }
            // }

        return response.send({'status': 'ok'});
    }

    static syncInitialTransactions = async(request: Request, response: Response, nextf: NextFunction) => {
		    let accounts:any = request.query.accounts ? request.query.accounts : false;
        let api = new BasiqHelper();
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jobId:any = request.query.jobId;
        let fcm_token:any = request.query.fcmToken;

        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today)
        yesterday.setDate(yesterday.getDate() - 1)

        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);
        let days_365 = new Date(yesterday);
        days_365.setDate(yesterday.getDate() - 365);

        let days_365_last = days_365.getFullYear()+'-'+("0"+(days_365.getMonth()+1)).slice(-2)+'-'+("0"+(days_365.getDate())).slice(-2);
        let fromDate:any = days_365_last;
        let toDate:any = last;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);
        let encrypt_userId = user.encrypted_id;
        let hub = new HubSpotHelper();
        let mixData = {};

        // Initilize transaction
        let loginName = user.basiq_id;
        let encrypt_loginName = await HashHelper.encrypt(loginName);
        let first_name = user.first_name;

        // get lookup table data for rating computation
        let lookups = [];
        let repoLookUp = getRepository(Classification);
        let lookup = await repoLookUp.createQueryBuilder("lookup").getMany();

        for(let i in lookup) {
            lookups.push(lookup[i]);
        }

        // get store list
        let stRepo = getRepository(Store);
        let stList = await stRepo.createQueryBuilder("stores").getMany();

        // get user accounts
        let acAr = [];

        if (accounts !== false) {
          let acList = accounts.split(",");
          for (let i in acList) {
            acAr.push({'account_id': acList[i]});
          }
        } else {
          let accountss = await getRepository(Account).createQueryBuilder("acc").getRawMany();

          for (let i in accountss) {
            try {
              let decryptId = await HashHelper.decrypt(accountss[i].acc_user_id);
              if(decryptId === userId) {
                acAr.push({'account_id': accountss[i].acc_account_id});
              }
            } catch (e) {
              continue;
            }
          }
        }

        mixData['number_linked_accounts'] = acAr.length;

        let counter = 0;

        let asyncInterval = async (ms) => {
          return new Promise((resolve, reject) => {
            let interval = setInterval(async () => {
              let job = await api.getJob(user.basiq_id, jobId);

              for(let j in job.steps) {
                if(job.steps[j].title.toString() === 'retrieve-transactions') {

                  let status = job.steps[j].status.toString(); // set the job status

                  if (status === 'failed') {

                    clearInterval(interval);
                    resolve(0);

                    let event_name = 'Bank Linking Failed';
                    let data = {
                      'distinct_id': user.encrypted_id,
                      'first_name': user.first_name,
                      'detail': job.steps[j].result,
                      'data': {
                        'job': job
                      }
                    }

                    MixpanelHelper.track(event_name, data);

                    let payload = {
                      notification: {
                        title: "Bank Linking Error",
                        body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
                      },
                      data: {
                        user_id: ''+userId+'',
                        status: '1',
                        message: 'failed',
                        isInitialTransaction: '1'
                      }
                    };

                      let options = {
                        priority: "normal",
                        timeToLive: 60 * 60
                      };

                      FirebaseHelper.notifyDevice(fcm_token, payload, options);

                      // delete inserted accounts from the beginning if has
                      await getConnection()
                          .createQueryBuilder()
                          .delete()
                          .from(Account)
                          .where("account_id IN (:...account_ids)", { account_ids: accounts.split(",") })
                          .execute();

                      return response.send({
                        'status': 'ok',
                        'message': 'Unable to proceed for fetching transactions due to bank link error!',
                        'error': job
                      });

                  } else if (status === 'success') {

                    // Clear the interval
                    clearInterval(interval);

                    // Map through accounts
                    await Promise.all(acAr.map(async (ac) => {

                      // Get first page
                      let transactions_list = await api.syncTransactionsByAccountId(loginName, ac.account_id, false, fromDate, toDate);
                      let transactions =  transactions_list.data.data;
                      await TransactionHelper.SyncInitialTransactionV2(transactions, encrypt_loginName, lookup, loginName, stList, encrypt_userId, first_name);

                      // Get additional pages
                      let next = transactions_list.data.links.next;
                      while (next !== undefined) {
                        let transactions_next = await api.syncTransactionsByAccountId(loginName, ac.account_id, next, fromDate, toDate);
                        let nextLink = transactions_next.data.links.next;

                        await TransactionHelper.SyncInitialTransactionV2(transactions_next.data.data, encrypt_loginName, lookup, loginName, stList, encrypt_userId, first_name);
                        next = nextLink;
                      }

                    }));

                    // End the loop
                    resolve(0);

                  }

                  break;

                }
              }
            }, ms);
          });
        }

        let initialCall = async () => {
          try {

            await asyncInterval(2000); // 2 secs

          } catch (e) {

            let event_name = 'Bank Linking Failed';
            let data = {
              'distinct_id': user.encrypted_id,
              'first_name': user.first_name,
              'error_details': e
            }
            MixpanelHelper.track(event_name, data);

          }
        }

        // Trigger sync of transactions
        await initialCall();

        // Get all accounts
        let allAccounts = await getRepository(Account).createQueryBuilder('acc').getRawMany();
        let userAccounts = [];
        let hasPreviousLinked = false;

        // Determine which accounts belong to current user
        await Promise.all(allAccounts.map(async (account) => {

          let decryptId = await HashHelper.decrypt(account.acc_user_id);
          if (decryptId === userId) {
            userAccounts.push({
              'account_id': account.acc_account_id,
              'created_at': account.acc_created_at
            });

            if(account.acc_account_synced === true) {
              hasPreviousLinked = true;
            }
          }

        }));

        // Delete duplicate user accounts
        function arrayUnique(arr, uniqueKey) {
          const uniqueList = []
          return arr.filter(function(item) {
            if (uniqueList.indexOf(item[uniqueKey]) === -1) {
              uniqueList.push(item[uniqueKey])
              return true
            }
          })
        }
        const uniqueAccounts = arrayUnique(userAccounts, 'account_id');

        // Delete rating histories
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(RatingHistory)
            .where("user_id = :basiq_id", { basiq_id: user.basiq_id })
            .execute();

        // get user quiz response for home and energy
        const quizResponse = await QuizHelper.getHomeEnergyResponse(user.id);

        // Map through accounts and create ratings
        await Promise.all(uniqueAccounts.map(async (account) => {

          // Get transactions by account ID
          let trans = getRepository(Transaction);
          let transaction = await trans.createQueryBuilder()
              .where("account_id = :account_id and transaction_id IS NOT null", { account_id: account.account_id })
              .distinctOn(["basiq_id"])
              .getMany();

          // Set the transaction created at date to the account created at date
          for(let i in transaction) {
            transaction[i]['acct_created_at'] = account.created_at;
          }
          // Recalculate Halo Score
          await TransactionHelper.resyncHaloCalculateTransactions(transaction, user.basiq_id, stList, lookup, quizResponse, hasPreviousLinked);

        }));

        // populate HS fields
        let today_90 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday_90 = new Date(today_90);
        yesterday_90.setDate(yesterday_90.getDate() - 1);
        let last_90 = yesterday_90.getFullYear()+'-'+("0"+(yesterday_90.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_90.getDate())).slice(-2);
        let days_90_90 = new Date(yesterday_90);
        days_90_90.setDate(days_90_90.getDate() - 90);
        let days_90_last_90 = days_90_90.getFullYear()+'-'+("0"+(days_90_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90_90.getDate())).slice(-2);
        let fromDate_90:any = days_90_last_90;
        let toDate_90:any = last_90;
        let properties = [];

        properties.push({ property: 'g_greener_member_lifecycle_stage', value: 'Greener App User - Bank connected' });

        //for bank hs fields
        let banks = await getRepository(Account)
            .createQueryBuilder("acc")
            .select("acc.bank_code, acc.created_at, acc.user_id")
            .orderBy("acc.created_at", "ASC")
            .getRawMany();

        banks.sort((a, b) => Number(a.created_at) - Number(b.created_at));

        let bankList = [];

        for(let i in banks) {
            try {
                let decryptId = await HashHelper.decrypt(banks[i].user_id);
                if(decryptId === userId) {
                    bankList.push({ 'bank_code': banks[i].bank_code, 'created_at': banks[i].created_at });
                }
            } catch (e) {
                continue;
            }
        }

        let distinctOnBankCode = bankList.reduce((acc, current) => {
            const x = acc.find(item => item.bank_code === current.bank_code);
            if (!x) {
              return acc.concat([current]);
            } else {
              return acc;
            }
        }, []);

        let countb = 0;

        for(let i in distinctOnBankCode) {
            countb += 1;

            let bankDetail = await api.getInstitution(loginName, distinctOnBankCode[i].bank_code);

            // first bank linking
            if(countb === 1){
                properties.push({ property: 'g_connected_bank_account_name', value: bankDetail.name });
                properties.push({ property: 'g_connected_bank_account_logo', value: bankDetail.logo.links.square });
            }

            if(countb === 2){
                properties.push({ property: 'g_connected_bank_account_name_02', value: bankDetail.name });
                properties.push({ property: 'g_connected_bank_account_logo_02', value: bankDetail.logo.links.square });
            }

            if(countb === 3){
                properties.push({ property: 'g_connected_bank_account_name_03', value: bankDetail.name });
                properties.push({ property: 'g_connected_bank_account_logo_03', value: bankDetail.logo.links.square });
            }
        }

        let halo = await RatingHelper.ThreeMonths(loginName);
        mixData['halo_rating'] = halo.rating.green.toFixed(2);
        properties.push({ property: 'g_greener_rating_current', value: halo.rating.green.toFixed(2) });

        //get contact
        try{
            let contactDetails = await hub.getContactbyId(user.hubspot_id);
            let first_rating = contactDetails.properties.g_greener_rating_when_first_connected_bank.value;

            if(first_rating === null || first_rating === undefined || first_rating === '') {
                properties.push({ property: 'g_greener_rating_when_first_connected_bank', value: halo.rating.green.toFixed(2) });

                // for first greener store purchase
                let first_greener_store = await getRepository(RatingHistory)
                    .createQueryBuilder("calc")
                    .where("calc.user_id = :id", { id: loginName })
                    .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
                    .groupBy("calc.id")
                    .addGroupBy("stores.id")
                    .orderBy("calc.calculation_date", "ASC")
                    .getRawOne();

                if(first_greener_store !== undefined) {
                    properties.push({ property: 'g_greener_store_first_purchase', value: first_greener_store.calc_store_name });
                    properties.push({ property: 'g_greener_store_logo_first_purchase', value: first_greener_store.stores_logo });
                }
            }

        } catch(e) {
            // set manually the property and value, it means the the roperty is not yet initialized in hubspot
            properties.push({ property: 'g_greener_rating_when_first_connected_bank', value: halo.rating.green.toFixed(2) });

            // for first greener store purchase
            let first_greener_store = await getRepository(RatingHistory)
                .createQueryBuilder("calc")
                .where("calc.user_id = :id", { id: loginName })
                .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
                .groupBy("calc.id")
                .addGroupBy("stores.id")
                .orderBy("calc.calculation_date", "ASC")
                .getRawOne();

            if(first_greener_store !== undefined) {
                properties.push({ property: 'g_greener_store_first_purchase', value: first_greener_store.calc_store_name });
                properties.push({ property: 'g_greener_store_logo_first_purchase', value: first_greener_store.stores_logo });
            }
        }

        let greener_stores = await getRepository(RatingHistory)
            .createQueryBuilder("calc")
            .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id and calc.store_name != ''", { id: loginName })
            .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
            .orderBy("calc.spend", "DESC")
            .limit(3)
            .getRawMany();

        greener_stores.sort((a, b) => Number(b.calc_spend) - Number(a.calc_spend));

        // for greener store name and logo
        let count = 0;
        for(let i in greener_stores) {
            count += 1;

            if(count === 3){
                properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
            }

            if(count === 2){
                properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
            }

            if(count === 1){
                properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
            }
        }

        let check = (property) => {
            return properties.some(function(el) {
                return el.property === property;
            });
        }

        if(check('g_greener_store_name_third_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: '' });

        if(check('g_greener_store_logo_third_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: '' });

        if(check('g_greener_store_name_second_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: '' });

        if(check('g_greener_store_logo_second_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: '' });

        if(check('g_greener_store_name_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: '' });

        if(check('g_greener_store_logo_highest_CO2_avoided_previous_90_days') === false)
            properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: '' });

        let hubsObject = { properties: properties }
        try {
          await hub.updateContact(user.hubspot_id, hubsObject);
        } catch(e) {
          console.log(e);
        }

        //set
        mixData['is_onboarded'] = user.is_onboarded;

	      // set user alias
        MixpanelHelper.set(encrypt_userId, mixData);

        // set accounts_sync=TRUE
        await getConnection()
            .createQueryBuilder()
            .update(Account)
            .set({ account_synced: true })
            .where("account_id IN (:...account_ids)", { account_ids: accounts.split(",") })
            .execute();

        let event_name = 'Bank Linking Selected Account';
        let data = {
            'distinct_id': user.encrypted_id,
            'first_name': user.first_name,
            'accounts': accounts
        }

        MixpanelHelper.track(event_name, data);

        let payload = {
          notification: {
            title: "Your rating is calculated",
            body: "See how green you are, and how to improve."
          },
          data: {
            user_id: ''+userId+'',
            status: '1',
            message: 'success',
            isInitialTransaction: '1'
          }
        };

        let options = {
          priority: "normal",
          timeToLive: 60 * 60
        };

        FirebaseHelper.notifyDevice(fcm_token, payload, options);

        return response.send({'status': 'ok'});
    }

    static syncNonGreenerStores = async(request: Request, response: Response, nextf: NextFunction) => {
        await getConnection().query(`DELETE FROM ${env.TYPEORM_SCHEMA}.stores_non_greener WHERE id IN(SELECT id FROM(SELECT  id,ROW_NUMBER() OVER(PARTITION BY "name" ORDER BY id) AS row_num FROM greener.stores_non_greener) t WHERE t.row_num > 1);`);

        let greeer = await getRepository(Store)
                .createQueryBuilder("s")
                .select("s.name")
                .getMany();
        let gs = [];

        for(let s in greeer) {
            gs.push(greeer[s].name.toLowerCase());
        }

        let non_greeer = await getRepository(StoreNonGreener)
                .createQueryBuilder("s")
                .select("s.name")
                .getMany();

        for(let s in non_greeer) {
            gs.push(non_greeer[s].name.toLowerCase());
        }

        let stores = await getRepository(Transaction)
                .createQueryBuilder("t")
                .select("t.merchant_name, t.logo_master")
                .distinctOn(["t.merchant_name"])
                .where("t.merchant_name != '' or t.merchant_name IS NOT NULL or t.logo_master IS NOT NULL and lower(t.merchant_name) NOT IN (:...greeer)", {greeer: gs})
                .getRawMany();

        let stAr = [];

        for(let s in stores) {
            let logo = stores[s].logo_master;

            if(logo !== null && logo.toString().endsWith(".svg") === true) {
                let url = logo;
                let name = stores[s].merchant_name.toString().toLowerCase().replace(/\s/g, "-");

                await fetch(url).then(function (response) {
                    // The API call was successful!
                    return response.text();
                }).then(function (html) {
                    (async () => {
                        const image = await svgToImg.from(html).toPng();

                        AWS.config.update({
                            accessKeyId: env.AWS_ACCESS_KEY,
                            secretAccessKey: env.AWS_SECRET_KEY,
                            region: env.AWS_REGION
                        });

                        let s3 = new AWS.S3();

                        // Setting up S3 upload parameters
                        let params = {
                            Bucket: 'greener-wallet',
                            Key: 'images/'+name+'.png',
                            Body: image,
                            ContentType: 'image/png',
                        };

                        // Uploading files to the bucket
                        s3.upload(params, async(err, data) => {
                            if (err) {
                                console.log(err);
                            }
                        });
                        console.log(image);
                    })();
                }).catch(function (err) {
                    // ignore this
                });

                logo = "https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/"+name+".png";
            }

            stAr.push({
                'name': stores[s].merchant_name,
                'full_address': stores[s].merchant_formatted_address,
                'latitude': stores[s].merchant_geometry_lan,
                'longitude': stores[s].merchant_geometry_lon,
                'logo': logo
            });
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreNonGreener)
            .values(stAr)
            .execute();

        return response.send({'status': 'ok'});
    }

    static syncPriceLevel = async(request: Request, response: Response, nextf: NextFunction) => {
        let entityManager = getManager();
        let stores = await getRepository(Store)
            .createQueryBuilder("Store")
            .distinctOn(["Store.name"])
            .leftJoinAndSelect("Store.storeaddresses", "address")
            .getRawMany();

        let queryStr = ` `;
        let sets = [];

        for(let s in stores) {

            let price_level = 0;
            let api = new GoogleHelper();
            let result = await api.getPriceLevel(''+stores[s].Store_name+' '+stores[s].address_address+'');

            try {
                price_level = result.candidates[0].price_level ? result.candidates[0].price_level : 0;
            } catch (e){
                price_level = 0;
            }

            sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET "rating"=${price_level} WHERE id='${stores[s].Store_id}' `);
        }

        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        await entityManager.query(finalStr.toString());

        return response.send({'status': 'ok'});
    }

    static syncStoreAddress = async(request: Request, response: Response, nextf: NextFunction) => {
        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today);
        yesterday.setDate(yesterday.getDate() - 1);
        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);

        console.log(last);

        let stores = await getRepository(Store)
            .createQueryBuilder("Store")
            .distinctOn(["Store.name"])
            .getRawMany();

        let seed = [];
        let count = 0;
        for(let s in stores) {
            count += 1;
            let text_search = ''+stores[s].Store_name+' in Australia AU, AUS';
            let api = new GoogleHelper();
            let result = await api.getStoreAddresses(text_search);
            let addresses =  result.results;
            let next = result.next_page_token;
            let status = result.status;

            while (next !== undefined){
                let result_next = await api.getStoreAddressesNext(text_search, next);
                let nextLink = result_next.next_page_token;
                status = result_next.status;
                addresses = addresses.concat(result_next.results);

                if(result_next.results.length !== 0) {
                    next = nextLink;
                }

                if(status === 'ZERO_RESULTS') {
                    next = nextLink;
                }
            }

            for(let a in addresses){
                seed.push({
                    'storeId': stores[s].Store_id,
                    'address': addresses[a].formatted_address,
                    'latitude': addresses[a].geometry.location.lat,
                    'longitude': addresses[a].geometry.location.lng
                });
            }
        }

        //save new
        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreAddress)
            .values(seed)
            .execute();

        //delete new
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(StoreAddress)
            .where("created_at < :date", { date: last })
            .execute();

        return response.send({'status': 'ok','count': seed.length, 'data': seed});
    }

    static addressSearchViaGmaps = async(request: Request, response: Response, nextf: NextFunction) => {
		let addRarr: StoreAddressDraft[] = [];
        let { addresstext, city_list } = request.body;
        let stores = addresstext.toString().split(",");
        let cities = city_list.toString().split(",");

        for(let s in stores) {
            console.log(stores[s]);
            let store_name = stores[s].toString().replace("%20"," ");
            let api = new GoogleHelper();

            if(cities.length !== 0) {
                console.log('im here c');
                for(let c in cities) {
                    console.log(cities[c]);
                    let city = cities[c].toString().replace("%20"," ");
                    let text_search = ''+store_name+' in '+city+' Australia AU, AUS';
                    let result = await api.getStoreAddresses(text_search);
                    let addresses =  result.results;
                    let next = result.next_page_token;
                    let status = result.status;

                    while (next !== undefined){
                        let result_next = await api.getStoreAddressesNext(text_search, next);
                        let nextLink = result_next.next_page_token;
                        status = result_next.status;
                        addresses = addresses.concat(result_next.results);

                        if(result_next.results.length !== 0) {
                            next = nextLink;
                        }

                        if(status === 'ZERO_RESULTS') {
                            next = nextLink;
                        }
                    }

                    for(let a in addresses){
                        let rat = new StoreAddressDraft();
                        rat.store_name = store_name;
                        rat.address = addresses[a].formatted_address;
                        rat.latitude = addresses[a].geometry.location.lat;
                        rat.longitude = addresses[a].geometry.location.lng;
                        addRarr.push(rat);
                    }
                }
            } else {
                console.log('im here');
                let text_search = ''+store_name+' in Australia AU, AUS';
                let result = await api.getStoreAddresses(text_search);
                let addresses =  result.results;
                let next = result.next_page_token;
                let status = result.status;

                while (next !== undefined){
                    let result_next = await api.getStoreAddressesNext(text_search, next);
                    let nextLink = result_next.next_page_token;
                    status = result_next.status;
                    addresses = addresses.concat(result_next.results);

                    if(result_next.results.length !== 0) {
                        next = nextLink;
                    }

                    if(status === 'ZERO_RESULTS') {
                        next = nextLink;
                    }
                }

                for(let a in addresses){
                    let rat = new StoreAddressDraft();
                    rat.store_name = store_name;
                    rat.address = addresses[a].formatted_address;
                    rat.latitude = addresses[a].geometry.location.lat;
                    rat.longitude = addresses[a].geometry.location.lng;
                    addRarr.push(rat);
                }
            }
        }

        let connection = getConnection();
        await connection.manager.save(addRarr, { chunk: 10000 });

        return response.send({'status': 'ok','addresses': addRarr});
    }

    static encryptUserIdsInAccount =  async(request: Request, response: Response, nextf: NextFunction) => {
        let repo = getRepository(Account);
		let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();
        for(let i in accounts) {
            let encryptId = await HashHelper.encrypt(accounts[i].acc_user_id);

            let data = await repo.findOneOrFail(accounts[i].acc_id);
            data.user_id = encryptId;

            await repo.save(data);
        }

        return response.send({'status': 'ok'});
    }

    static getUser =  async(request: Request, response: Response, nextf: NextFunction) => {
		let userRepository = getRepository(User);
        let jwtPayload;
        let Jwttoken = <string>request.headers["x-greener-auth-token"];

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);

        if(user.basiq_id !== null) {
            return response.send({"basiq_id" : user.basiq_id });
        }

		// create also this user in yodlee
        let api = new BasiqHelper();
		let basiqUsertoCreate = {
            mobile: user.mobile_number,
            email: user.email
        }

        let responseUser;

		try{
            responseUser = await api.createUser(basiqUsertoCreate);
        } catch(e) {
            let er = {
				'message_high': 'Error adding user to basiq.',
                'message': e.response.statusText,
                'info': e.response.data
			}

            return response.status(422).send(er);
        }

		try{
			user.basiq_id = responseUser.id;
			await userRepository.save(user);

		} catch (e) {
			let er = {
				'message_high': 'Error adding user to basiq.',
				'error_code_text': 'LimitExceededException',
				'statusText': responseUser,
				'info': responseUser,
                'er': e
			}

			return response.status(422).send(er);
		}

        return response.send({"basiq_id" : responseUser.id });
    }

    static HSGreenerRating =  async(request: Request, response: Response, nextf: NextFunction) => {
        let today_90 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday_90 = new Date(today_90);
        yesterday_90.setDate(yesterday_90.getDate() - 1);
        let last_90 = yesterday_90.getFullYear()+'-'+("0"+(yesterday_90.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_90.getDate())).slice(-2);
        let days_90_90 = new Date(yesterday_90);
        days_90_90.setDate(days_90_90.getDate() - 90);
        let days_90_last_90 = days_90_90.getFullYear()+'-'+("0"+(days_90_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90_90.getDate())).slice(-2);
        let fromDate_90:any = days_90_last_90;
        let toDate_90:any = last_90;

        let repo = getRepository(User);
        let users = await repo.find({select: ["id", "basiq_id","id","hubspot_id"] });

        for(let u in users) {
            try {
                let loginName = users[u].basiq_id;
                let hubsPotId = users[u].hubspot_id;
                let properties = [];

                if(loginName === null || hubsPotId === null){
                    // skipping null basic IDS
                    continue;
                }

                let halo_90 = await RatingHelper.ThreeMonths(loginName);

                // daily greener rating update
                properties.push({ property: 'g_greener_rating_current', value: halo_90.rating.green.toFixed(2) });

                // get user accounts
                let acAr = [];
                let accounts = await getRepository(Account).createQueryBuilder("acc").orderBy('created_at','ASC').getRawMany();

                for(let i in accounts) {
                    try {
                        let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                        if(decryptId === users[u].id) {
                            acAr.push({'created_at': accounts[i].acc_created_at, 'account_id': accounts[i].acc_account_id});
                        }
                    } catch (e) {
                        continue;
                    }
                }

                if(acAr.length !== 0) {
                    let count = 0;
                    for(let ac in acAr) {
                        count += 1;
                        if(count !== 1) continue;

                        let created_at = acAr[ac].created_at;
                            created_at = new Date(created_at);
                            created_at = created_at.getFullYear()+'-'+("0"+(created_at.getMonth()+1)).slice(-2)+'-'+("0"+(created_at.getDate())).slice(-2);

                        let a1 = moment([today_90.slice(0,4), today_90.slice(6,7), today_90.slice(9,10)]);
                        let a2 = moment([created_at.slice(0,4), created_at.slice(5,7), created_at.slice(8,10)]);

                        let adiff = a1.diff(a2, 'days');
                        console.log("adiff: ", adiff);

                        // making sure that this will run once just after exact 7 days
                        if(adiff === 8) {
                            properties.push({ property: 'g_greener_rating_after_7_days', value: halo_90.rating.green.toFixed(2) });
                        }

                        // making sure that this will once run just after exact 30 days
                        if(adiff === 31) {
                            properties.push({ property: 'g_greener_rating_after_30_days', value: halo_90.rating.green.toFixed(2) });
                        }
                    }
                }

                let greener_stores = await getRepository(RatingHistory)
                    .createQueryBuilder("calc")
                    .distinctOn(["calc.store_name"])
                    .where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id", { id: loginName })
                    .leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
                    .groupBy("calc.id")
                    .addGroupBy("stores.id")
                    .orderBy("calc.store_name")
                    .addOrderBy("calc.spend", "DESC")
                    .limit(3)
                    .getRawMany();

                greener_stores.sort((a, b) => Number(b.calc_spend) - Number(a.calc_spend));

                // for greener store name and logo
                let count = 0;
                for(let i in greener_stores) {
                    count += 1;

                    if(count === 3){
                        properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }

                    if(count === 2){
                        properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }

                    if(count === 1){
                        properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: greener_stores[i].calc_store_name });
                        properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: greener_stores[i].stores_logo });
                    }
                }

                let check = (property) => {
                    return properties.some(function(el) {
                        return el.property === property;
                    });
                }

                if(check('g_greener_store_name_third_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_third_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_name_second_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_second_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_name_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: '' });

                if(check('g_greener_store_logo_highest_CO2_avoided_previous_90_days') === false)
                    properties.push({ property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: '' });

                let hub = new HubSpotHelper();
                let hubReturn;
                let hubsObject = { properties: properties }

                try{
                    hubReturn = await hub.updateContact(hubsPotId, hubsObject);
                } catch(e) {
                    // do nothing, usually happen in dev test if user is not yet there
                }
            } catch (e) {
                // do nothing, to not stop the execution
            }
        }

        return response.send({'status': 'ok'});
    }

    static HSGreenerWeeklyRating =  async(request: Request, response: Response, nextf: NextFunction) => {
        // dates for past week
        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today);
        yesterday.setDate(yesterday.getDate() - 1);
        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);
        let days_90 = new Date(yesterday);
        days_90.setDate(days_90.getDate() - 6);
        let days_90_last = days_90.getFullYear()+'-'+("0"+(days_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90.getDate())).slice(-2);
        let fromDate:any = days_90_last;
        let toDate:any = last;

        let repo = getRepository(User);
        let users = await repo.find({select: ["id", "basiq_id","id","hubspot_id"] });

        for(let u in users) {
            try {
                let loginName = users[u].basiq_id;
                let hubsPotId = users[u].hubspot_id;

                if(loginName === null || hubsPotId === null){
                    // skipping null basic IDS
                    continue;
                }

                let halo_90 = await RatingHelper.ThreeMonths(loginName);
                let halo_7 = await RatingHelper.PastSevenDays(loginName);

                let change_over_last_week = halo_90.rating.green - halo_7.rating.green;

                await RatingHelper.setHubsPotTopThreeMerchants(loginName, hubsPotId, change_over_last_week, fromDate, toDate);
                
            } catch (e) {
                console.log("error on loop: ", e);
            }
        }

        return response.send({'status': 'ok'});
    }

    static checkConnection = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let connection_id:any = request.query.connectionId;
        let jobId:any = request.query.jobId;
        let fcm_token:any = request.query.fcmToken;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);
        let hubsPotId = user.hubspot_id;
        let count = 0;

        setInterval(async function() {
            count += 1;

            console.log('call me: ', count);
            let check = await api.getConnection(user.basiq_id, connection_id);
            let job = await api.getJob(user.basiq_id, jobId);

            if(job.steps[1].status.toString() === 'success' && job.steps[2].status.toString() === 'success') {
                clearInterval(this);
                let payload = {
                    notification: {
                        title: "Finish set-up now to discover your Rating",
                        body: "Success! Your bank is now connected."
                    },
                    data: {
                        user_id: ''+userId+'',
                        institution_id: ''+check.institution.id+'',
                        status: '1',
                        message: 'success'
                    }
                };

                let options = {
                    priority: "normal",
                    timeToLive: 60 * 60
                };

                let event_name = 'Bank Linking Success';
                let data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'data': {
                        'job': job,
                        'connection': check
                    }
                }

                MixpanelHelper.track(event_name, data);

                FirebaseHelper.notifyDevice(fcm_token, payload, options);
            }

            // maximum retry count
            if(count === 3 && job.steps[1].status.toString() != 'success' && job.steps[2].status.toString() != 'success') {
                clearInterval(this);

                let bankDetail = await api.getInstitution(user.basiq_id, check.institution.id);
                let properties = [];
                let payload = {
                    notification: {
                        title: "We'll just try connecting again in 24 hours.",
                        body: "Looks like your bank outage is still going. There's nothing you need to do. We've got it covered!"
                    },
                    data: {
                        user_id: ''+userId+'',
                        institution_id: ''+check.institution.id+'',
                        status: '0',
                        message: 'failed'
                    }
                }
                let options = {
                    priority: "normal",
                    timeToLive: 60 * 60
                };

                let event_name = 'Bank Linking Failed';
                let data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'data': {
                        'job': job,
                        'connection': check
                    }
                }

                MixpanelHelper.track(event_name, data);

                try{
                    let jobRes = job.steps;

                    for(let j in jobRes) {
                        if(jobRes[j].result.code === 'invalid-credentials') {
                            let payload = {
                                notification: {
                                    title: "Opps!!! Invalid Bank Login Credentials",
                                    body: "Looks like you've put invalid Bank Login credentials!, Please try again to Link your Bank Account."
                                },
                                data: {
                                    user_id: ''+userId+'',
                                    institution_id: ''+check.institution.id+'',
                                    status: '0',
                                    message: 'failed'
                                }
                            }

                            FirebaseHelper.notifyDevice(fcm_token, payload, options);
                        } else {
                            properties.push({ property: 'g_basiq_user_id', value: user.basiq_id });
                            properties.push({ property: 'g_basiq_connection_id', value: connection_id });
                            properties.push({ property: 'g_basiq_job_id', value: jobId });
                            properties.push({ property: 'g_basiq_connection_error_bank_name', value: bankDetail.name });
                            properties.push({ property: 'g_basiq_connection_error_trigger', value: 'TRUE' });

                            let hub = new HubSpotHelper();
                            let hubReturn;
                            let hubsObject = { properties: properties }

                            console.log('properties', properties);

                            try{
                                hubReturn = await hub.updateContact(hubsPotId, hubsObject);
                            } catch(e) {
                                // do nothing, usually happen in dev test if user is not yet there
                            }

                            let newJob = new Job();
                            newJob.user_id = userId;
                            newJob.connection_id = connection_id;
                            newJob.job_id = jobId;
                            newJob.fcm_token = fcm_token;
                            newJob.step = 1;
                            let repo2 = getRepository(Job);
                            await repo2.save(newJob);

                            FirebaseHelper.notifyDevice(fcm_token, payload, options);
                        }
                    }
                } catch (e) {
                    // do nothing
                }
            }
        }, (1000 * 60) * 0.1); // 10 minutes total of 30 minutes

        return response.send({"status": "ok", "message": "Will notify user device once the bank connection is done."});
    }

    static checkJobConnection = async(request: Request, response: Response, next: NextFunction) => {
        let repoJobs = getRepository(Job);
        let jobs = await repoJobs.createQueryBuilder("jobs").orderBy("jobs.created_at", "ASC").getMany();

        for(let i in jobs) {
            try {
                let connection_id = jobs[i].connection_id;
                let jobId = jobs[i].job_id;
                let fcm_token = jobs[i].fcm_token;
                let userId = jobs[i].user_id;
                let step = jobs[i].step;// step 1,2  = 1 hour, step 3,4 = 24 hrs - fire checker is the created_at column field
                let repo = getRepository(User);
                let user = await repo.findOne(userId);
                let hubsPotId = user.hubspot_id;
                let created_date = new Date(jobs[i].created_at);
                let now = new Date();
                let diff = now.valueOf() - created_date.valueOf();
                let duration:any = diff/1000/60/60; // Convert milliseconds to hours
                duration = duration.toFixed(0);

                // hour checker
                if(step === 1 || step === 2 && duration === 1) {
                    await JobHelper.checkJobStatus(userId, user, connection_id, jobId, fcm_token, hubsPotId, step, jobs[i].id);
                }

                // 24 hrs checker
                if(step === 3 || step === 4 && duration === 24) {
                   await JobHelper.checkJobStatus(userId, user, connection_id, jobId, fcm_token, hubsPotId, step, jobs[i].id);
                }

                // else ignore
            } catch (e) {
                console.log("ignore error for user no hubspot id", e);
            }
        }

        return response.send({"status": "ok", "message": "Will notify user device once the bank connection is done."});
    }

    static checkInitailJob = async(request: Request, response: Response, nexts: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let jobId:any = request.query.jobId;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);
        let devices = user.device_ids;

        await setInterval(async function() {
            let job = await api.getJob(user.basiq_id, jobId);
            let payload = {
                notification: {
                    title: "Bank Linking Error",
                    body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
                },
                data: {
                    user_id: ''+userId+'',
                    status: '1',
                    message: 'failed',
                    isInitialTransaction: '1'
                }
            };

            let options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            for(let j in job.steps) {

                let status = job.steps[j].status.toString();
                let event_name = 'Bank Linking Failed';
                let data = {
                    'distinct_id': user.encrypted_id,
                    'first_name': user.first_name,
                    'detail': job.steps[j].result,
                    'data': {
                        'job': job
                    }
                }

                if(job.steps[j].title.toString() === 'retrieve-accounts') {
                    if(status === 'failed') {
                        clearInterval(this);
                        MixpanelHelper.track(event_name, data);

                        if(devices !== null) {
                            if(devices['android'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['android'], payload, options);
                            }

                            if(devices['ios'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                            }
                        }

                        break;
                    }
                }

                if(job.steps[j].title.toString() === 'retrieve-transactions') {
                    if(status === 'failed') {
                        clearInterval(this);
                        MixpanelHelper.track(event_name, data);

                        if(devices !== null) {
                            if(devices['android'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['android'], payload, options);
                            }

                            if(devices['ios'] !== undefined) {
                                FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                            }
                        }

                        break;
                    }

                    if(status === 'success') {
                        clearInterval(this);
                    }

                    break;
                }

            }


        }, 10000); // 10 seconds

        return response.send({"status": "ok"});
    }

    static getConnectedBankStatus = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = [];
		let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i in accounts) {
            try {
                let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
                if(decryptId === userId) {
                    data.push(accounts[i]);
                }
            } catch (e) {
                continue;
            }
        }

        let status = 1;
        let message = 'User already linked a bank account.';

        if(data.length === 0) {
            status = 0;
            message = 'User has no associated linked account yet.';
        }

        return response.send({
            'message': message,
            'status': status
        });
    }

    static convertSVGtoPNGImages = async(request: Request, response: Response, next: NextFunction) => {
        await getConnection().query(`DELETE FROM ${env.TYPEORM_SCHEMA}.stores_non_greener WHERE id IN(SELECT id FROM(SELECT  id,ROW_NUMBER() OVER(PARTITION BY "name" ORDER BY id) AS row_num FROM greener.stores_non_greener) t WHERE t.row_num > 1);`);

        let repo = getRepository(StoreNonGreener);
        let non_greener_stores = await repo.createQueryBuilder("stores").where("logo like '%.svg'").getMany();
        let queryStr = ` `;
        let sets = [];

        for(let i in non_greener_stores) {
            let url = non_greener_stores[i].logo;
            let name = non_greener_stores[i].name.toString().toLowerCase().replace(/\s/g, "-");

            await fetch(url).then(function (response) {
                // The API call was successful!
                return response.text();
            }).then(function (html) {
                (async () => {
                    const image = await svgToImg.from(html).toPng();

                    AWS.config.update({
                        accessKeyId: env.AWS_ACCESS_KEY,
                        secretAccessKey: env.AWS_SECRET_KEY,
                        region: env.AWS_REGION
                    });

                    let s3 = new AWS.S3();

                    // Setting up S3 upload parameters
                    let params = {
                        Bucket: 'greener-wallet',
                        Key: 'images/'+name+'.png',
                        Body: image,
                        ContentType: 'image/png',
                    };

                    // Uploading files to the bucket
                    s3.upload(params, async(err, data) => {
                        if (err) {
                            console.log(err);
                        }
                    });
                })();
            }).catch(function (err) {
                // There was an error
            });

            let logo = "https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/"+name+".png";
            sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores_non_greener SET "logo"='${logo}' WHERE id::uuid='${non_greener_stores[i].id}'`);
        }

        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        await getConnection().query(finalStr.toString());

        return response.send({
            'status': 'ok',
            'message': 'All svg image has been coverted into png and has been uploaded to s3 buket.'
        });
    }

    static getJobFromConnection = async(request: Request, response: Response, next: NextFunction) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let connection_id:any = request.query.connectionId;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let repo = getRepository(User);
        let user = await repo.findOne(userId);

        // refresh the connection
        await api.refreshConnection(user.basiq_id, connection_id);

        try {
            let jobDetail = await api.getJobFromConnection(user.basiq_id, connection_id);
            return response.send({
                'status': 1,
                'detail': jobDetail
            });
        } catch (e) {
            return response.send({
                'status': 0,
                'error': e
            });
        }
    }

    static notifyUsersNeedBankRelogin = async(request: Request, response: Response, next: NextFunction) => {
        let repo_ser_failed_obs = getRepository(DailyFailedJob);
        let failed_list = await repo_ser_failed_obs.createQueryBuilder("DailyFailedJob").getMany();

        for(let ujl in failed_list) {
            let repo = getRepository(User);
            let user_id = failed_list[ujl].user_id;
            let user = await repo.findOne(user_id);
            let devices = user.device_ids;

            let payload = {
                notification: {
                    title: "Bank Linking Error",
                    body: "There has been a problem accessing your bank details, please try again later. Our support team have been informed of the issue"
                },
                data: {
                    user_id: ''+user_id+'',
                    status: '1',
                    message: 'failed',
                    isNeedToReAuthenticate: '1'
                }
            };

            let options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            if(devices !== null) {
                if(devices['android'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['android'], payload, options);
                }

                if(devices['ios'] !== undefined) {
                    FirebaseHelper.notifyDevice(devices['ios'], payload, options);
                }
            }
        }

        return response.send({'status': 'ok'});
    }
}
