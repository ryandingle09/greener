import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {QuizQuestion} from "../models/QuizQuestion";

export class QuizQuestionController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(QuizQuestion);
        let data = await repo
            .createQueryBuilder("QuizQuestion")
            .leftJoinAndSelect("QuizQuestion.quizcategory", "quizcategory")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }
}