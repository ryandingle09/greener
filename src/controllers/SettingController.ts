import {getRepository, getConnection, In} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../models/User";
import {UserSetting} from "../models/UserSetting";
import {Account} from "../models/Account";
import {Transaction} from "../models/Transaction";
import {RatingHistory} from "../models/RatingHistory";
import * as jwt from "jsonwebtoken";
import * as AWS from 'aws-sdk';
import { HubSpotHelper } from "../helpers/hubspot";
import { HashHelper } from "../helpers/hasher";
import { BasiqHelper } from "../helpers/basiq";

const env = process.env;

export class SettingController {

    static accountUpdate = async(request: Request, response: Response, next: NextFunction) => {
        
        let { email, mobile_number, pin } = request.body;
        let repo = getRepository(User);
        let data = await repo.findOne(request.params.id);

        if(data.email !== email){
            let count = await repo.count({ where: {email}});

            if(count !== 0){
                return response.status(422).send({'message': 'Email address already taken.'});
            }
            
            data.email = email;
        }

        if(data.mobile_number !== mobile_number){
            let count = await repo.count({ where: {mobile_number}});

            if(count !== 0){
                return response.status(422).send({'message': 'Mobile Number already taken.'});
            }
            
            data.mobile_number = mobile_number;
        }

        data.pin = pin;
        data.hashPin();

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static accountDetail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;

        try {
            let data = await repo.findOneOrFail(userId);
            return response.send(data);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }
    }

    static personalDetail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);
        let userSettingRepository = getRepository(UserSetting);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;

        try {
            let data = await repo.findOneOrFail(userId);
            let settings = await userSettingRepository.findOne({ where: { user_id: userId }, select: ['green_purchases_status', 'non_green_purchases_status', 'email_notification_status', 'location_services_status', 'face_id_status'] });
            
            return response.send({
                'message': 'successfully retrieved data.', 
                'user': data,
                'user_setting': settings
            });
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }
    }

    static updateInfo = async(request: Request, response: Response, next: NextFunction) => {
        
        let { first_name, last_name, address } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;

        let data = await repo.findOne(userId);
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;

        let hubsObject = {
			properties: 
			[   
                { property: 'firstname', value: data.first_name },
                { property: 'lastname', value: data.last_name },
                { property: 'address', value: data.address }
			] 
		}
		let hub = new HubSpotHelper();
		let hubReturn;
        let curdate = new Date();
		let curtime = curdate.toISOString();

		try{
        	hubReturn = await hub.updateContact(data.hubspot_id, hubsObject);
        } catch(e) {
            console.log(e);

            hubsObject = {
                properties: 
                [   
                    { property: 'email', value: data.email },
                    { property: 'firstname', value: data.first_name },
                    { property: 'lastname', value: data.last_name },
                    { property: 'phone', value: data.mobile_number },
                    { property: 'address', value: data.address },
                    { property: 'g_appprofilecreateddate', value: curtime },
					{ property: 'g_greener_member_profile_picture', value: '' }
                ] 
            }

            hubReturn = await hub.createContact(hubsObject);

            data.hubspot_id = hubReturn.vid;
        }

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updateEmail = async(request: Request, response: Response, next: NextFunction) => {
        let { email } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);

        if(data.email !== email){
            let count = await repo.count({ where: {email}});

            if(count !== 0){
                return response.status(422).send({'message': 'Email address already taken.'});
            }
            
            data.email = email;
        }

		let hubsObject = {
			properties: 
			[   
				{ property: 'email', value: data.email }
			] 
		}
		let hub = new HubSpotHelper();
		let hubReturn;
        let curdate = new Date();
		let curtime = curdate.toISOString();

		try{
        	hubReturn = await hub.updateContact(data.hubspot_id, hubsObject);
        } catch(e) {
            let er = {
                'message': e
			}

            hubsObject = {
                properties: 
                [   
                    { property: 'email', value: data.email },
                    { property: 'firstname', value: data.first_name },
                    { property: 'lastname', value: data.last_name },
                    { property: 'phone', value: data.mobile_number },
                    { property: 'address', value: data.address },
                    { property: 'g_appprofilecreateddate', value: curtime }
                ] 
            }

            hubReturn = await hub.createContact(hubsObject);

            data.hubspot_id = hubReturn.vid;
        }

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updateDeviceIds = async(request: Request, response: Response, next: NextFunction) => {
        let device_ids = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);
            data.device_ids = device_ids;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updateMobile = async(request: Request, response: Response, next: NextFunction) => {
        let { mobile_number } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);

        if(data.mobile_number !== mobile_number){
            let count = await repo.count({ where: {mobile_number}});

            if(count !== 0){
                return response.status(422).send({'message': 'Mobile Number already taken.'});
            }
            
            data.mobile_number = mobile_number;
        }

        let hubsObject = {
			properties: 
			[   
				{ property: 'phone', value: data.mobile_number }
			] 
		}
		let hub = new HubSpotHelper();
		let hubReturn;
        let curdate = new Date();
		let curtime = curdate.toISOString();

		try{
        	hubReturn = await hub.updateContact(data.hubspot_id, hubsObject);
        } catch(e) {
            let er = {
                'message': e
			}

            hubsObject = {
                properties: 
                [   
                    { property: 'email', value: data.email },
                    { property: 'firstname', value: data.first_name },
                    { property: 'lastname', value: data.last_name },
                    { property: 'phone', value: data.mobile_number },
                    { property: 'address', value: data.address },
                    { property: 'g_appprofilecreateddate', value: curtime }
                ] 
            }

            hubReturn = await hub.createContact(hubsObject);

            data.hubspot_id = hubReturn.vid;
        }

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updatePin = async(request: Request, response: Response, next: NextFunction) => {
        let { pin } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);

        if (!data.checkIfUnencryptedPinIsValid(pin, data.pin)) {
			data.pin = pin;
            data.hashPin();
		}

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updateFaceID = async(request: Request, response: Response, next: NextFunction) => {
        let { status } = request.body;
        let repo = getRepository(UserSetting);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne({ where: { user_id: userId} });
        data.face_id_status = status;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static deactivateAccount = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;
        let api = new BasiqHelper();

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);
        let acAr = [];
        let accountss = await getRepository(Account).createQueryBuilder("acc").getRawMany();

        for(let i in accountss) {
            try {
                let decryptId = await HashHelper.decrypt(accountss[i].acc_user_id);
                if(decryptId === userId) {
                    acAr.push(accountss[i].acc_account_id);
                }
            } catch (e) {
                continue;
            }
        }

        try {

            if(acAr.length !== 0) {
                // delete accounts
                await getConnection()
                    .createQueryBuilder()
                    .delete()
                    .from(Account)
                    .where({"account_id": In(acAr)})
                    .execute();
            }
            
            // delete user halo calculation history
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(RatingHistory)
                .where("user_id = :id", { id: data.basiq_id })
                .execute();

            // remove user settings
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(UserSetting)
                .where("user_id = :id", { id: data.id })
                .execute();

            // remove user in basiq
            await api.deleteUser(data.basiq_id);

            // remove user info
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

		let hub = new HubSpotHelper();
		let hubReturn;
        let curdate = new Date();
		let curtime = curdate.toISOString();

        let hubsObject = {
			properties: 
			[   
				{ property: 'g_greener_member_lifecycle_stage', value: 'Greener App User - Deactivated' },
                { property: 'g_app_profile_deleted_date', value: curtime },
                { property: 'g_greener_member_profile_picture', value: '' },
                { property: 'g_greener_store_name_highest_CO2_avoided_previous_90_days', value: ' ' },
                { property: 'g_greener_store_name_second_highest_CO2_avoided_previous_90_days', value: '' },
                { property: 'g_greener_store_name_third_highest_CO2_avoided_previous_90_days', value: '' },
                { property: 'g_greener_store_logo_highest_CO2_avoided_previous_90_days', value: '' },
                { property: 'g_greener_store_logo_second_highest_CO2_avoided_previous_90_days', value: '' },
                { property: 'g_greener_store_logo_third_highest_CO2_avoided_previous_90_days', value: '' },
                { property: 'g_connected_bank_account_name', value: '' },
                { property: 'g_connected_bank_account_logo', value: '' },
                { property: 'g_connected_bank_account_name_02', value: '' },
                { property: 'g_connected_bank_account_logo_02', value: '' },
                { property: 'g_connected_bank_account_name_03', value: '' },
                { property: 'g_connected_bank_account_logo_03', value: '' },
                { property: 'g_greener_store_first_purchase', value: '' },
                { property: 'g_greener_store_logo_first_purchase', value: '' },
                { property: 'g_greener_rating_change_over_the_last_week', value: '' },
                { property: 'g_greener_store_name_highest_CO2_avoided_previous_7_days', value: '' },
                { property: 'g_greener_store_name_second_highest_CO2_avoided_previous_7_days', value: '' },
                { property: 'g_greener_store_name_third_highest_CO2_avoided_previous_7_days', value: '' },
                { property: 'g_other_store_name_highest_CO2_previous_7_days', value: '' },
                { property: 'g_other_store_name_second_highest_CO2_previous_7_days', value: '' },
                { property: 'g_other_store_name_third_highest_CO2_previous_7_days', value: '' }
			] 
		}

		try{
        	hubReturn = await hub.updateContact(data.hubspot_id, hubsObject);
        } catch(e) {
            let er = {
                'error': e,
                'message': 'Please contact greener support.'
			}

            return response.status(422).send(er);
        }

        return response.send({"message": "Successfully Deactivated."});
    }

    static updateSetting = async(request: Request, response: Response, next: NextFunction) => {
        let { items } = request.body;
        let repo = getRepository(UserSetting);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne({ where: { name: { user_id: userId} } });
			if(items.length !== 0) {
                for(let a in items) {
                    if(items[a].item == 'green_purchases'){
                        data.green_purchases_status = items[a].value;
                    }
                    if(items[a].item == 'non_green_purchases'){
                        data.non_green_purchases_status = items[a].value;
                    }
                    if(items[a].item == 'email_notification'){
                        data.email_notification_status = items[a].value;
                    }
                    if(items[a].item == 'location_services'){
                        data.location_services_status = items[a].value;
                    }
                }
            }

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static updatePhoto = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        AWS.config.update({
            accessKeyId: env.AWS_ACCESS_KEY,
            secretAccessKey: env.AWS_SECRET_KEY,
            region: env.AWS_REGION
        })
    
        let s3 = new AWS.S3();
    
        // Binary data base64
        let fileContent  = Buffer.from(request['files'].photo.data, 'binary');
    
        // Setting up S3 upload parameters
        let params = {
            Bucket: 'greener-wallet',
            Key: "users/images/"+new Date().getTime()+"-"+request['files'].photo.name, // File name you want to save as in S3
            Body: fileContent 
        };

        let { userId } = jwtPayload;
        let user = await repo.findOne(userId);

        // remove old image
        var params_del = {
            Bucket: 'greener-wallet',
            Key: user.photo_key
        };

        s3.deleteObject(params_del, function (err, data) {
            if (data) {
                // deleted
            }
            else {
                // Check if you have sufficient permissions
            }
        });

        s3.deleteObject()
    
        // Uploading files to the bucket
        s3.upload(params, async(err, data) => {
            if (err) {
                console.log(err);
                return response.status(422).send({"message": "Unable to upload photo."});
            }

            user.photo_url = data.Location;
            user.photo_key = data.Key;
            
            await repo.save(user);

            let hub = new HubSpotHelper();
            let hubReturn;
            let curdate = new Date();
            let curtime = curdate.toISOString();

            let hubsObject = {
                properties: 
                [   
                    { property: 'g_greener_member_profile_picture', value: user.photo_url }
                ] 
            }

            try{
                hubReturn = await hub.updateContact(user.hubspot_id, hubsObject);
            } catch(e) {
                let er = {
                    'error': e,
                    'message': 'Please contact greener support.'
                }

                return response.status(422).send(er);
            }

            return response.send({"message": "Successfully Uploaded.", "data": {'photo_url': user.photo_url}});
        });
    }

    static updatePhotoBySocial = async(request: Request, response: Response, next: NextFunction) => {
        let { photo_url } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);
        data.photo_url = photo_url;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        let hub = new HubSpotHelper();
        let hubReturn;
        let curdate = new Date();
        let curtime = curdate.toISOString();

        let hubsObject = {
            properties: 
            [   
                { property: 'g_greener_member_profile_picture', value: photo_url }
            ] 
        }

        try{
            hubReturn = await hub.updateContact(data.hubspot_id, hubsObject);
        } catch(e) {
            let er = {
                'error': e,
                'message': 'Please contact greener support.'
            }

            return response.status(422).send(er);
        }

        return response.send({"message": "Successfully Updated.", "photo_url": photo_url});
    }

    static updateOnboardStatus = async(request: Request, response: Response, next: NextFunction) => {
        let { is_onboarded } = request.body;
        let repo = getRepository(User);
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        let { userId } = jwtPayload;
        let data = await repo.findOne(userId);
        data.is_onboarded = is_onboarded;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "is_onboarded": is_onboarded});
    }

}