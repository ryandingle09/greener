import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Credential} from "../models/Credential";

export class CredentialController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Credential);
        let data = await repo
            .createQueryBuilder("credential")
            .orderBy("credential.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let search = request.query.query ? request.query.query : ''
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Credential);

        search = search.toString().toLowerCase();

        let data = await repo
            .createQueryBuilder("credential")
            .where("LOWER(credential.name) like :name", { name:`%${search}%` })
            .orWhere("LOWER(credential.description) like :description", { description:`%${search}%` })
            .orderBy("credential.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static create = async(request: Request, response: Response, next: NextFunction) => {
        let { name, description } = request.body;
        let data = new Credential();

        data.name = name;
        data.description = description;

        const repo = getRepository(Credential);

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Name already taken."});
        }

        return response.send({"message": "Successfully created."});
    }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Credential);

        try {
            let data = await repo.findOneOrFail(request.params.id);
            return response.send(data);
        } catch (e) {
            return response.send({"message": "Data not found."});
        }
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {
        let { name, description } = request.body;
        let repo = getRepository(Credential);
        let data;

        try {
            data = await repo.findOneOrFail(request.params.id);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }

        data.name = name;
        data.desciption = description;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static delete = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Credential);
        let data = await repo.findOne(request.params.id);

        try {
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."}); 
        }

        response.send({"message": "Successfully Deleted."}); 
    }

}