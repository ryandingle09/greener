import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Store} from "../models/Store";

export class StoreController {

    static recommended = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Store);
        let s1 = request.query.s1;
        let s2 = request.query.s2;
        let s3 = request.query.s3;
        let s4 = request.query.s4;
        
        let data = await repo
            .createQueryBuilder("Store")
            .where("LOWER(Store.name) = :s1", {s1: s1.toString().toLowerCase() })
            .orWhere("LOWER(Store.name) = :s2", {s2: s2.toString().toLowerCase() })
            .orWhere("LOWER(Store.name) = :s3", {s3: s3.toString().toLowerCase() })
            .orWhere("LOWER(Store.name) = :s4", {s4: s4.toString().toLowerCase() })
            .getMany();

        return response.send(data);
    }

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let greenr:any = request.query.is_greener ? request.query.is_greener : false;
        let offset = (page * limit) - limit;
        let sort_by:any = request.query.sort_by ? request.query.sort_by : 'name';
        let sort_order:any = request.query.sort_order ? request.query.sort_order : 'ASC';
        let is_online:any = request.query.is_online ? request.query.is_online : false;
        let repo = getRepository(Store);
        let data = await repo
            .createQueryBuilder("Store")
            .leftJoinAndSelect("Store.sector", "sector")
            .leftJoinAndSelect("Store.storeaddresses", "address")
            .leftJoinAndSelect("Store.subsectors", "subsectors")
            .leftJoinAndSelect("Store.accreditations", "accreditations")
            .orderBy("Store."+sort_by+"", sort_order)
            .skip(offset)
            .take(limit)
            .getMany();

        if(is_online === '1') {
            console.log('is_online = ', is_online);
            data = await repo
                .createQueryBuilder("Store")
                .leftJoinAndSelect("Store.sector", "sector")
                .leftJoinAndSelect("Store.subsectors", "subsectors")
                .leftJoinAndSelect("Store.storeaddresses", "address")
                .leftJoinAndSelect("Store.accreditations", "accreditations")
                .where("Store.is_online is TRUE")
                .orderBy("Store."+sort_by+"", sort_order)
                .skip(offset)
                .take(limit)
                .getMany();
        }

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let search = request.query.query ? request.query.query : ''
        let page:any = request.query.page ? request.query.page : 1;
        let greenr:any = request.query.is_greener ? request.query.is_greener : false;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let sort_by:any = request.query.sort_by ? request.query.sort_by : 'name';
        let sort_order:any = request.query.sort_order ? request.query.sort_order : 'ASC';
        let repo = getRepository(Store);

        search = search.toString().toLowerCase();

        let data = await repo
            .createQueryBuilder("Store")
            .leftJoinAndSelect("Store.sector", "sector")
            .leftJoinAndSelect("Store.subsectors", "subsectors")
            .leftJoinAndSelect("Store.storeaddresses", "address")
            .leftJoinAndSelect("Store.accreditations", "accreditations")
            .where("LOWER(Store.name) like :name", { name:`%${search}%` })
            .orderBy("Store."+sort_by+"", sort_order)
            .skip(offset)
            .take(limit)
            .getMany();
        
        if(greenr !== false) {
            if(greenr == 0){
                data = await repo
                .createQueryBuilder("Store")
                .leftJoinAndSelect("Store.sector", "sector")
                .leftJoinAndSelect("Store.storeaddresses", "address")
                .leftJoinAndSelect("Store.subsectors", "subsectors")
                .leftJoinAndSelect("Store.accreditations", "accreditations")
                .where("LOWER(Store.name) like :name and Store.is_greenr = :is_greener", { name:`%${search}%`, is_greener: false })
                .orderBy("Store."+sort_by+"", sort_order)
                .skip(offset)
                .take(limit)
                .getMany();
            }
            if(greenr == 1){
                data = await repo
                .createQueryBuilder("Store")
                .leftJoinAndSelect("Store.sector", "sector")
                .leftJoinAndSelect("Store.subsectors", "subsectors")
                .leftJoinAndSelect("Store.storeaddresses", "address")
                .leftJoinAndSelect("Store.accreditations", "accreditations")
                .where("LOWER(Store.name) like :name and Store.is_greenr = :is_greener", { name:`%${search}%`, is_greener: true })
                .orderBy("Store."+sort_by+"", sort_order)
                .skip(offset)
                .take(limit)
                .getMany();
            }
        }

        return response.send(data);
    }

    static findBySector = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Store);

        let data = await repo
            .createQueryBuilder("Store")
            .where("Store.sectorId = :id", { id:`${request.params.id}` })
            .leftJoinAndSelect("Store.subsectors", "subsectors")
            .leftJoinAndSelect("Store.sector", "sector")
            .leftJoinAndSelect("Store.storeaddresses", "address")
            .leftJoinAndSelect("Store.accreditations", "accreditations")
                .skip(offset)
                .take(limit)
                .getMany();

        return response.send(data);
    }

    static create = async(request: Request, response: Response, next: NextFunction) => {
        let { name, logo } = request.body;
        let data = new Store();

        data.name = name;
        data.logo = logo;

        const repo = getRepository(Store);

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Name already taken."});
        }

        return response.send({"message": "Successfully created."});
    }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Store);

        try {
            let data = await repo.findOneOrFail(request.params.id, {relations: ['storeaddresses','subsectors','sector','accreditations']});
            return response.send(data);
        } catch (e) {
            return response.send({"message": "Data not found."});
        }
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {
        let { name, logo } = request.body;
        let repo = getRepository(Store);
        let data;

        try {
            data = await repo.findOneOrFail(request.params.id);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }

        data.name = name;
        data.desciption = logo;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static delete = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Store);
        let data = await repo.findOne(request.params.id);

        try {
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."}); 
        }

        return response.send({"message": "Successfully Deleted."}); 
    }
}