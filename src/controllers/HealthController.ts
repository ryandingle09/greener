import {NextFunction, Request, Response} from "express";

export class HealthController {

    static check = async(request: Request, response: Response, next: NextFunction) => {
        let status_code = 200;
        let data = {
            'status': 'UP'
        };

        return response.status(status_code).send(data);
    }

}