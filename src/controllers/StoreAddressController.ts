import {getRepository, createQueryBuilder} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {StoreAddress} from "../models/StoreAddress";
import {Store} from "../models/Store";
import getDistanceFromLatLonInM from '../helpers/common';

const geolib = require('geolib');

export class StoreAddressController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(StoreAddress);

        let data = await repo
            .createQueryBuilder("StoreAddress")
            .leftJoinAndSelect("StoreAddress.store", "store")
            .leftJoinAndSelect('store.sector', 'sector')
            .leftJoinAndSelect('store.subsectors', 'subsectors')
            .leftJoinAndSelect("store.accreditations", "accreditations")
                .skip(offset)
                .take(limit)
                .getMany();

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let address = request.query.address ? request.query.address : '';
        let category = request.query.category ? request.query.category : false;
        let latitude:any = request.query.latitude ? request.query.latitude : '';
        let longitude:any = request.query.longitude ? request.query.longitude : '';
        let range:any = request.query.range ? request.query.range : 5000;
        let page:any = request.query.page ? request.query.page : 1
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(StoreAddress);
        let repo2 = getRepository(Store);
        let data;
        let storesData = [];

        address = address.toString().toLowerCase();

        let storesAdd = await repo
            .createQueryBuilder("StoreAddress")
            .distinctOn(["StoreAddress.address"])
            .leftJoinAndSelect("StoreAddress.store", "store")
            .leftJoinAndSelect('store.sector', 'sector')
            .leftJoinAndSelect('store.subsectors', 'subsectors')
            .leftJoinAndSelect("store.accreditations", "accreditations")
            .orderBy("StoreAddress.address")
            .getMany();

        if(category !== false) {
            storesAdd = await repo
                .createQueryBuilder("StoreAddress")
                .distinctOn(["StoreAddress.address"])
                .leftJoinAndSelect("StoreAddress.store", "store")
                .leftJoinAndSelect('store.subsectors', 'subsectors')
                .leftJoinAndSelect('store.sector', 'sector')
                .leftJoinAndSelect("store.accreditations", "accreditations")
                .where("store.sectorId = :category", { category: category})
                .orderBy("StoreAddress.address")
                .getMany();
        }

        // online store
        let check_no_address = await repo2
            .createQueryBuilder("store")
            .addSelect("COUNT(storeaddresses.id) as address_count")
            .leftJoinAndSelect("store.storeaddresses", "storeaddresses")
            .leftJoinAndSelect('store.subsectors', 'subsectors')
            .leftJoinAndSelect('store.sector', 'sector')
            .leftJoinAndSelect("store.accreditations", "accreditations")
            .where("store.is_online IS TRUE")
            .groupBy("store.id")
            .addGroupBy("storeaddresses.id")
            .addGroupBy("subsectors.id")
            .addGroupBy("sector.id")
            .addGroupBy("accreditations.id")
            .getMany();
            
        let no_address = [];
        for(let i in check_no_address) {
            if(check_no_address[i].storeaddresses.length === 0){
                no_address.push(check_no_address[i]);
            }
        }

        let nearIds = [];

        for(let i in storesAdd) {
            storesData.push(storesAdd[i]);
        }

        for(let b in storesData){
            let lat1 = storesData[b].latitude;
            let lon1 = storesData[b].longitude;
            let lat2 = latitude;
            let lon2 = longitude;

            let distance_meter = getDistanceFromLatLonInM({ lat1, lon1, lat2, lon2, });
            let distance_in_km = (distance_meter / 1000);

            // check if current within the requested kilometer range
            let point = geolib.isPointWithinRadius(
                { latitude: lat1, longitude: lon1 }, // store address
                { latitude: lat2, longitude: lon2 },  // current lat/lon
                range //range in KM default 5000/5km
            );
    
            if(point) {
                nearIds.push({
                    'id': storesData[b].id,
                    'storeId': storesData[b].storeId,
                    'address': storesData[b].address,
                    'latitude': storesData[b].latitude,
                    'longitude': storesData[b].longitude,
                    'opening_hours': storesData[b].opening_hours,
                    'distance_kilometer': distance_in_km, 
                    'distance_meter': distance_meter,
                    'created_at': storesData[b].created_at,
                    'updated_at': storesData[b].updated_at,
                    "store": storesData[b].store
                });
            }
        }

        data = nearIds.sort((a, b) => a.distance_kilometer - b.distance_kilometer);

        if(address !== '') {
            data = await repo
                .createQueryBuilder("StoreAddress")
                .leftJoinAndSelect("StoreAddress.store", "store")
                .leftJoinAndSelect('store.sector', 'sector')
                .leftJoinAndSelect('store.subsectors', 'subsectors')
                .leftJoinAndSelect("store.accreditations", "accreditations")
                .where("LOWER(StoreAddress.address) like :address", { address:`%${address}%` })
                .orderBy("StoreAddress.created_at", "DESC")
                .skip(offset)
                .take(limit)
                .getMany();
        }

        return response.send({
            'no_address': no_address,
            'has_address': data
        });
    }

    // static create = async(request: Request, response: Response, next: NextFunction) => {
    //     let { name, logo } = request.body;
    //     let data = new StoreAddress();

    //     data.name = name;
    //     data.logo = logo;

    //     const repo = getRepository(StoreAddress);

    //     try {
    //         await repo.save(data);
    //     } catch (e) {
    //         return response.status(422).send({"message": "Name already taken."});
    //     }

    //     return response.send({"message": "Successfully created."});
    // }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(StoreAddress);

        try {
            let data = await repo.findOneOrFail(request.params.id);
            return response.send(data);
        } catch (e) {
            return response.send({"message": "Data not found."});
        }
    }

    // static update = async(request: Request, response: Response, next: NextFunction) => {
    //     let { address, logo } = request.body;
    //     let repo = getRepository(StoreAddress);
    //     let data;

    //     try {
    //         data = await repo.findOneOrFail(request.params.id);
    //     } catch (e) {
    //         return response.status(422).send({"message": "Data not found."});
    //     }

    //     data.address = address;
    //     data.desciption = logo;

    //     try {
    //         await repo.save(data);
    //     } catch (e) {
    //         return response.status(422).send({"message": "Unable to update data."});
    //     }

    //     return response.send({"message": "Successfully Updated.", "data": data});
    // }

    // static delete = async(request: Request, response: Response, next: NextFunction) => {
    //     let repo = getRepository(StoreAddress);
    //     let data = await repo.findOne(request.params.id);

    //     try {
    //         await repo.remove(data);
    //     } catch (e) {
    //         return response.status(422).send({"message": "No data associated with the given parameter."}); 
    //     }

    //     return response.send({"message": "Successfully Deleted."}); 
    // }
}