import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Accreditation} from "../models/Accreditation";

export class AccreditationController {
    
    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Accreditation);
        let data = await repo
            .createQueryBuilder("accreditation")
            .orderBy("accreditation.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let search = request.query.query ? request.query.query : '';
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Accreditation);
        let data = await repo
            .createQueryBuilder("accreditation")
            .where("accreditation.name like :name", { name:`%${search}%` })
            .orWhere("accreditation.description like :description", { description:`%${search}%` })
            .orWhere("accreditation.country like :country", { country:`%${search}%` })
            .orderBy("accreditation.created_at", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static create = async(request: Request, response: Response, next: NextFunction) => {
        let { name, description, country } = request.body;
        let data = new Accreditation();

        data.name = name;
        data.description = description;
        data.country = country;

        const repo = getRepository(Accreditation);

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Name already taken."});
        }

        return response.send({"message": "Successfully created."});
    }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Accreditation);

        try {
            let data = await repo.findOneOrFail(request.params.id);
            return response.send(data);
        } catch (e) {
            return response.send({"message": "Data not found."});
        }
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {
        let { name, description, country } = request.body;
        let repo = getRepository(Accreditation);
        let data;

        try {
            data = await repo.findOneOrFail(request.params.id);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }

        data.name = name;
        data.desciption = description;
        data.country = country;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static delete = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Accreditation);
        let data = await repo.findOne(request.params.id);

        try {
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."}); 
        }

        response.send({"message": "Successfully Deleted."}); 
    }

}