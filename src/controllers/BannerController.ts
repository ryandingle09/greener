import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Banner} from "../models/Banner";

export class BannerController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let category:any = request.query.category ? request.query.category : false;
        let repo = getRepository(Banner);
        let data = await repo
            .createQueryBuilder("Banner")
            .leftJoinAndSelect("Banner.banneritems", "banneritems")
            .leftJoinAndSelect("banneritems.store", "store")
            .leftJoinAndSelect("store.storeaddresses", "storeaddresses")
            .leftJoinAndSelect("store.sector", "sector")
            .leftJoinAndSelect("store.subsectors", "subsectors")
            .orderBy("Banner.sort_order", "ASC")
            .skip(offset)
            .take(limit)
            .getMany();

        if(category !== false) {
            data = await repo
                .createQueryBuilder("Banner")
                .leftJoinAndSelect("Banner.banneritems", "banneritems")
                .leftJoinAndSelect("banneritems.store", "store")
                .leftJoinAndSelect("store.storeaddresses", "storeaddresses")
                .leftJoinAndSelect("store.sector", "sector")
                .leftJoinAndSelect("store.subsectors", "subsectors")
                .where("sector.id = '"+category+"'")
                .skip(offset)
                .take(limit)
                .getMany();
        }

        return response.send(data);
    }

    static search = async(request: Request, response: Response, next: NextFunction) => {
        let search = request.query.query ? request.query.query : ''
        let page:any = request.query.page ? request.query.page : 1;
        let limit:any = request.query.limit ? request.query.limit : 10;
        let offset = (page * limit) - limit;
        let repo = getRepository(Banner);

        search = search.toString().toLowerCase();

        let data = await repo
            .createQueryBuilder("Banner")
            .leftJoinAndSelect("Banner.banneritems", "banneritems")
            .leftJoinAndSelect("banneritems.store", "store")
            .leftJoinAndSelect("store.storeaddresses", "storeaddresses")
            .leftJoinAndSelect("store.sector", "sector")
            .leftJoinAndSelect("store.subsectors", "subsectors")
            .where("LOWER(store.name) like :name or LOWER(sector.name) like :name or LOWER(Banner.title) like :name", { name:`%${search}%` })
            .skip(offset)
            .take(limit)
            .getMany();

        return response.send(data);
    }

    static create = async(request: Request, response: Response, next: NextFunction) => {
        let { title, description } = request.body;
        let data = new Banner();

        data.title = title;
        data.description = description;

        const repo = getRepository(Banner);

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Title already taken."});
        }

        return response.send({"message": "Successfully created."});
    }

    static detail = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Banner);

        try {
            let data = await repo.findOneOrFail(request.params.id, {relations: ['banneritems']});
            return response.send(data);
        } catch (e) {
            return response.send({"message": "Data not found."});
        }
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {
        let { title, desciption } = request.body;
        let repo = getRepository(Banner);
        let data;

        try {
            data = await repo.findOneOrFail(request.params.id);
        } catch (e) {
            return response.status(422).send({"message": "Data not found."});
        }

        data.title = title;
        data.desciption = desciption;

        try {
            await repo.save(data);
        } catch (e) {
            return response.status(422).send({"message": "Unable to update data."});
        }

        return response.send({"message": "Successfully Updated.", "data": data});
    }

    static delete = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(Banner);
        let data = await repo.findOne(request.params.id);

        try {
            await repo.remove(data);
        } catch (e) {
            return response.status(422).send({"message": "No data associated with the given parameter."}); 
        }

        return response.send({"message": "Successfully Deleted."}); 
    }
}