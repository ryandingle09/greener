import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {QuizCategory} from "../models/QuizCategory";

export class QuizCategoryController {

    static list = async(request: Request, response: Response, next: NextFunction) => {
        let repo = getRepository(QuizCategory);
        let data = await repo
            .createQueryBuilder("QuizCategory")
            .leftJoinAndSelect("QuizCategory.quizquestions", "quizquestions")
            .leftJoinAndSelect("quizquestions.choices", "choices")
            .orderBy("QuizCategory.sort_order", "ASC")
            .addOrderBy("quizquestions.code", "ASC")
            .getMany();

        return response.send(data);
    }
}