import {NextFunction, Request, Response} from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { User } from "../models/User";
import { UserSetting } from "../models/UserSetting";
import { Account } from "../models/Account";
import { RatingHistory } from "../models/RatingHistory";
import { Otp } from "../models/Otp";
import { LoginAttempt } from "../models/LoginAttempt";
import { EmailHelper } from "../helpers/email";
import { SmsHelper } from "../helpers/sms";
import { HubSpotHelper } from "../helpers/hubspot";
import { HashHelper } from "../helpers/hasher";
import { MixpanelHelper } from '../helpers/mixpanel';
import { RatingHelper } from "../helpers/rating";

const otpGenerator = require('otp-generator');
const RequestIp = require('@supercharge/request-ip');
const env = process.env;

export class AuthController {

	static login = async(request: Request, response: Response, next: NextFunction) => {
		let { email, pin, mobile_number, flow_type } = request.body;
		let ip = RequestIp.getClientIp(request)
		let user: User;
		let userRepository = getRepository(User);
		let logRepository = getRepository(LoginAttempt);
		let userSettingRepository = getRepository(UserSetting);

		let countLog = await logRepository.count({ where: {'ip_address': ip}});

		if(countLog >= 3) {
			let checkLog = await logRepository.findOne({
					where: {
						ip_address: ip
					},
					order: {
						created_at: "DESC"
					}
			});

			let dateToday:any = new Date();
			let dateDb:any = new Date(checkLog.created_at);
	
			if(dateToday - dateDb < 2*60*1000){
				return response.status(422).send({"message": "You've been attempt to login 3 times with wrong credentials. Please try again every 2 minutes."});
			}

		}
		
		try {
			if(flow_type === 'social') {
				user = await userRepository.findOneOrFail({ where: { email }, select: ['id', 'email', 'pin', 'status', 'basiq_id', 'is_onboarded', 'encrypted_id', 'device_ids'] });
			}else if(flow_type === 'mobile') {
				user = await userRepository.findOneOrFail({ where: { mobile_number }, select: ['id', 'mobile_number', 'pin', 'status', 'basiq_id', 'is_onboarded', 'encrypted_id', 'device_ids'] });
			}else{
				return response.status(422).send({
					'flow_type': {
						'message': 'message": "flow_type parameter is required.',
						'rule': 'notfound'
					}
				});
			}
		} catch (error) {
			return response.status(422).send({
				'notfound': {
					'message': 'message": "Credentials do not matched.',
					'rule': 'notfound'
				}
			});
		}

		if (!user.checkIfUnencryptedPinIsValid(pin, user.pin)) {
			let log = new LoginAttempt();
			log.ip_address = ip;

			await logRepository.save(log);

			return response.status(422).send({
				'notfound': {
					'message': 'message": "Credentials do not matched.',
					'rule': 'notfound'
				}
			});
		}

		const token = jwt.sign(
			{ userId: user.id },
			env.jwtSecret,
			{ expiresIn: "1h" }
		);

		let userSetting = await userSettingRepository.findOne({ where: { user_id: user.id }, select: ['green_purchases_status', 'non_green_purchases_status', 'email_notification_status', 'location_services_status', 'face_id_status'] });

		await userRepository.save(user);

		let halo = await (await RatingHelper.ThreeMonths(user.basiq_id)).rating.green;
		let acAr = [];
		let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

		for(let i in accounts) {
			try {
				let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
				if(decryptId === user.id) {
					acAr.push(accounts[i].acc_created_at);
				}
			} catch (e) {
				continue;
			}
		}

		let linked_accounts_count = acAr.length;

		let link_d:any = false;
		if(linked_accounts_count !== 0) {
			link_d = acAr[0];
		}

		let mixData = {};
		
		//set 
        mixData['date_first_linked'] = link_d;
        mixData['number_linked_accounts'] = linked_accounts_count;
        mixData['halo_rating'] = halo;
        mixData['is_onboarded'] = user.is_onboarded;
		mixData['last_login_date'] = new Date();
		// set user alias
        MixpanelHelper.set(user.encrypted_id, mixData);

		return response.send({
			"message": "Successfully Logged In.", 
			"data" : {
				'id': user.id, 
				'email': user.email, 
				'photo_url': user.photo_url, 
				'mobile_number': user.mobile_number, 
				'status': user.status, 
				'settings': userSetting, 
				'basiq_id': user.basiq_id,
				'is_onboarded': user.is_onboarded,
				'device_ids': user.device_ids
			},
			"event": {
				'number_linked_accounts':linked_accounts_count,
				'date_first_linked': link_d,
				'halo_score': halo
			},
			"token": token
		});
	}

	static create = async(request: Request, response: Response, next: NextFunction) => {
		let { email, mobile_number, first_name, last_name, address, is_from_apple, is_from_fb, is_from_google, apple_id, flow_type } = request.body;
		let user = new User();
		let mixData = {};

		user.email = email;
		user.mobile_number = mobile_number;
		user.first_name = first_name;
		user.last_name = last_name;
		user.address = address;
        user.role = '1';
		user.status = true;

		if(is_from_apple) {
			user.is_from_apple = is_from_apple;
			user.apple_id = apple_id;
			mixData['signup_method'] = 'apple';
		}

		if(is_from_fb) {
			user.is_from_fb = is_from_fb;
			mixData['signup_method'] = 'facebook';
		}

		if(is_from_google) {
			user.is_from_google = is_from_google;
			mixData['signup_method'] = 'google';
		}

		let userRepository = getRepository(User);

		if(flow_type === 'social') {
			let countMail = await userRepository.count({ where: {email}});
	
			if(countMail !== 0){
				return response.status(422).send({
					'email': {
						'message': 'Email Address already taken.',
						'rule': 'unique'
					}
				});
			}


			if((mobile_number) && mobile_number !== '')
			{
				let countMobile = await userRepository.count({ where: {mobile_number}});
		
				if(countMobile !== 0){
					return response.status(422).send({
						'mobile_number': {
							'message': 'Mobile Number already used.',
							'rule': 'unique'
						}
					});
				}
			}
		}else{
			let countMobile = await userRepository.count({ where: {mobile_number}});
	
			if(countMobile !== 0){
				return response.status(422).send({
					'mobile_number': {
						'message': 'Mobile Number already used.',
						'rule': 'unique'
					}
				});
			}

			if((email) && email !== '')
			{
				let countMail = await userRepository.count({ where: {email}});
	
				if(countMail !== 0){
					return response.status(422).send({
						'email': {
							'message': 'Email Address already taken.',
							'rule': 'unique'
						}
					});
				}
			}
		}

		await userRepository.save(user);
		
		let data;
		
		if(flow_type === 'social') {
			data = await userRepository.findOneOrFail({ where: { email }, select: [ "id", "email", "mobile_number", "photo_url", "encrypted_id" ] });
		}
		else{
			data = await userRepository.findOneOrFail({ where: { mobile_number }, select: [ "id", "email", "mobile_number", "photo_url", "encrypted_id" ] });
		}

		const token = jwt.sign(
			{ userId: data.id },
			env.jwtSecret,
			{ expiresIn: "1h" }
		);

		let curdate = new Date();
		let curtime = curdate.toISOString();
		let hubsObject = {
			properties: 
			[   
				{ property: 'email', value: email },
				{ property: 'firstname', value: first_name },
				{ property: 'lastname', value: last_name },
				{ property: 'phone', value: mobile_number },
				{ property: 'address', value: address },
				{ property: 'g_appprofilecreateddate', value: curtime },
				{ property: 'g_greener_member_lifecycle_stage', value: 'Greener App User - Bank NOT connected' },
				{ property: 'g_greener_member_profile_picture', value: '' },
			] 
		}
		
		let hub = new HubSpotHelper();
		let hubReturn;

		try{
        	hubReturn = await hub.createContact(hubsObject);
        } catch(e) {
			// already exists
			let data = {
				properties: 
				[   
					{ property: 'firstname', value: first_name },
					{ property: 'lastname', value: last_name },
					{ property: 'phone', value: mobile_number },
					{ property: 'address', value: address },
					{ property: 'g_appprofilecreateddate', value: curtime },
					{ property: 'g_greener_member_lifecycle_stage', value: 'Greener App User - Bank NOT connected' },
					{ property: 'g_greener_member_profile_picture', value: '' },
				] 
			}

			hubReturn = await hub.createOrUpdate(email, data);
        }

		let encrypted_id = await HashHelper.encrypt(data.id);
		data.hubspot_id = hubReturn.vid;
		data.encrypted_id = encrypted_id;

		await userRepository.save(data);
		
		let userSetting = new UserSetting();
		userSetting.user_id = data.id;

		let userSettingRepository = getRepository(UserSetting);
		await userSettingRepository.save(userSetting);

		//set 
        mixData['$first_name'] = user.first_name;
		mixData['$signed_up_date'] = curtime;
		// set user alias
        MixpanelHelper.set(encrypted_id, mixData);

        return response.send({
			"message": "User Created Successfully.", 
			"data" : {'id': data.id, 'email': data.email, 'mobile_number': data.mobile_number, 'photo_url': data.photo_url},
			"token": token
		});
	}

	static checkEmail = async(request: Request, response: Response, next: NextFunction) => {
		let { email } = request.body;
		const userRepository = getRepository(User);

		let count = await userRepository.count({ where: {email}});

		if(count !== 0){
			return response.status(422).send({'message': 'Email Address already taken.'});
		}

		return response.send({'message': 'Email address is available'});
	}

	static checkMobileNumber = async(request: Request, response: Response, next: NextFunction) => {
		let { mobile_number } = request.body;
		const userRepository = getRepository(User);

		let count = await userRepository.count({ where: {mobile_number}});

		if(count !== 0){
			return response.status(422).send({'message': 'mobile_number already used.'});
		}

		return response.send({'message': 'Mobile Number is available'});
	}
	
	static forgotPassword = async(request: Request, response: Response, next: NextFunction) => {
		let { email } = request.body;

		let user: User;
		let userRepository = getRepository(User);
		
		try {
			user = await userRepository.findOneOrFail({ where: { email } });
		} catch (error) {
			return response.status(422).send({"message": "Email not found."});
		}

		const token = jwt.sign(
			{ userId: user.id, email: user.email },
			env.jwtSecret,
			{ expiresIn: "1h" }
		);
		  
		let subject = 'Reset password request';
		let message = 'http://localhost:3000/api/v1/auth/test';
		let sendmail = EmailHelper.notify(email, subject, message);

		if(!(sendmail)) {
			return response.status(422).send({"message": "Error sending email. Please contact software developer."});
		}

		return response.send({"message": "Forgot Password Link has been send to your email. Expiration is 1hr", "token": 'fwef'});
	}

	static setPassword = async(request: Request, response: Response, next: NextFunction) => {
		let { email, password } = request.body;

		if(!(request.params.token)) {
			return response.status(400).send({"message": "Forgot password token not found."});
		}

		let jwtPayload;

		try {
			jwtPayload = <any>jwt.verify(request.params.token, env.jwtSecret);
			response.locals.jwtPayload = jwtPayload;
		} catch (error) {
			return response.status(401).send({"message": "Invalid token."});
		}

		let user: User;
		let userRepository = getRepository(User);

		try {
			user = await userRepository.findOneOrFail({ where: { email } });
		} catch (error) {
			return response.status(422).send({"message": "Email not found."});
		}

		let userToUpdate = await userRepository.findOne(user.id);
		userToUpdate.password = password;
		
		try {
			await userRepository.save(userToUpdate);
		} catch (error) {
			return response.status(422).send({"message": "Error updating your password."});
		}

		return response.send({"message": "Password successfully udpated."});
	}
	  
	static refreshToken = async(request: Request, response: Response, next: NextFunction) => {
		let { user_id } = request.body;
		let user: User;
		let userRepository = getRepository(User);
		
		try {
			user = await userRepository.findOneOrFail(user_id);
		} catch (error) {
			return response.status(422).send({"message": "User not found with the given ID."});
		}

		
		let halo = await (await RatingHelper.ThreeMonths(user.basiq_id)).rating.green;
		let acAr = [];
		let accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

		for(let i in accounts) {
			try {
				let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
				if(decryptId === user.id) {
					acAr.push(accounts[i].acc_created_at);
				}
			} catch (e) {
				continue;
			}
		}

		let linked_accounts_count = acAr.length;

		console.log(acAr);

		let link_d:any = false;
		if(linked_accounts_count !== 0) {
			link_d = acAr[0];
		}

		const token = jwt.sign(
			{ userId: user.id },
			env.jwtSecret,
			{ expiresIn: "1h" }
		);

		let mixData = {};
		
		//set 
        mixData['date_first_linked'] = link_d;
        mixData['number_linked_accounts'] = linked_accounts_count;
        mixData['halo_rating'] = halo;
        mixData['is_onboarded'] = user.is_onboarded;
		mixData['last_login_date'] = new Date();
		// set user alias
        MixpanelHelper.set(user.encrypted_id, mixData);

		return response.send({
			"message": "Success getting new token.", 
			"event": {
				'number_linked_accounts': linked_accounts_count,
				'date_first_linked': link_d,
				'halo_score': halo,
				'device_ids': user.device_ids
			},
			"token": token
		});
	}

	static generateOtp = async(request: Request, response: Response, next: NextFunction) => {
		let { mobile_number, is_signup } = request.body;
		let otpRepository = getRepository(Otp);
		let userRepository = getRepository(User);

		if(is_signup === 'yes') {
			let count = await userRepository.count({ where: {mobile_number}});

			if(count !== 0){
				return response.status(422).send({'message': 'mobile_number already used.'});
			}
		}

		let otpVerification = otpGenerator.generate(6, { upperCase: false, specialChars: false , alphabets: false });
		let otp = new Otp();
		otp.otp = otpVerification
		
		try {
			await otpRepository.save(otp);
		} catch (error) {
			return response.status(422).send({"message": "Unable to generate otp. Please try again later."});
		}

		let message = ""+otpVerification+" One-Time password (OTP). DO NOT SHARE THIS WITH ANYONE. If you don't request for an OTP, Please contact Greener Support.";
		let from = "Greener";

		let sendSMS = SmsHelper.notifyByTwillio(from, mobile_number, message);

		if(!(sendSMS)) {
			return response.status(422).send({"message": "Error sending sms. Please contact software developer."});
		}

		return response.send({"message": "Success getting new otp. Expiration is 2 minutes.", "otp": otpVerification});
	}

	static generateOtpViaEmail = async(request: Request, response: Response, next: NextFunction) => {
		let { email } = request.body;
		let otpRepository = getRepository(Otp);

		let otpVerification = otpGenerator.generate(6, { upperCase: false, specialChars: false , alphabets: false });
		let otp = new Otp();
		otp.otp = otpVerification
		
		try {
			await otpRepository.save(otp);
		} catch (error) {
			return response.status(422).send({"message": "Unable to generate otp. Please try again later."});
		}

		let subject = "OTP - Reset PIN request";
		let message = ""+otpVerification+" One-Time password (OTP). DO NOT SHARE THIS WITH ANYONE. If you don't request for an OTP, Please contact Greener Support.";
		let sendmail = EmailHelper.notify(email, subject, message);

		if(!(sendmail)) {
			return response.status(422).send({"message": "Error sending email. Please contact Greener Support."});
		}

		return response.send({"message": "OTP verification to update your PIN has been sent to your email address."});
	}

	static verifyOtp = async(request: Request, response: Response, next: NextFunction) => {
		let { otp } = request.body;

		let data: Otp;
		let otpRepository = getRepository(Otp);

		try {
			data = await otpRepository.findOneOrFail({where: {otp: otp}});

			let dateToday:any = new Date();
			let dateDb:any = new Date(data.created_at);

			if(dateToday - dateDb > 2*60*1000){
				return response.status(422).send({"message": "OTP is expired. Please generate new OTP."});
			}
		} catch (error) {
			return response.status(422).send({"message": "Invalid OTP."});
		}

		return response.send({"message": "OTP successfully verified.", "otp": otp});
	}

	static setPin = async(request: Request, response: Response, next: NextFunction) => {
		let { pin, user_id } = request.body;

		let userRepository = getRepository(User);
		let userToUpdate = await userRepository.findOne(user_id);
		userToUpdate.pin = pin;
		userToUpdate.status = true;

        userToUpdate.hashPin();
		
		try {
			await userRepository.save(userToUpdate);
		} catch (error) {
			return response.status(422).send({"message": "Unable to set User PIN."});
		}

		return response.send({"message": "PIN Successfully Set."});
	}

	static verifyPin = async(request: Request, response: Response, next: NextFunction) => {
		let { pin, email } = request.body;

		let user: User;
		let userRepository = getRepository(User);
		
		try {
			user = await userRepository.findOneOrFail({ where: { email } });
		} catch (error) {
			return response.status(422).send({"message": "User not found."});
		}

		if (!user.checkIfUnencryptedPinIsValid(pin, user.pin)) {
			return response.status(422).send({"message": "Invalid PIN."});
		}

		const token = jwt.sign(
			{ userId: user.id, email: user.email },
			env.jwtSecret,
			{ expiresIn: "1h" }
		);

		return response.send({"message": "PIN Successfully Verified.", "token": token});
	}

	static verifyEmail = async(request: Request, response: Response, next: NextFunction) => {
		let { email } = request.body;
		const userRepository = getRepository(User);

		let count = await userRepository.count({ where: {email}});

		if(count !== 0){
			let user = await userRepository.findOneOrFail({ where: { email }});

			return response.send({'message': 'Email is verified registered', 'user_id': user.id});
		}

		return response.status(422).send({'message': 'Email Address not found.'});
	}

	static verifyMobileNumber = async(request: Request, response: Response, next: NextFunction) => {
		let { mobile_number } = request.body;
		const userRepository = getRepository(User);

		let count = await userRepository.count({ where: {mobile_number}});

		if(count !== 0){
			let user = await userRepository.findOneOrFail({ where: { mobile_number }});
			
			return response.send({'message': 'Mobile Number is verified registered.', 'user_id': user.id });
		}

		return response.status(422).send({'message': 'Mobile Number not found.'});
	}

	static verifyMobileNumberOwner = async(request: Request, response: Response, next: NextFunction) => {
		let { mobile_number, email } = request.body;
		let userRepository = getRepository(User);
		let count = await userRepository.count({ where: {email, mobile_number}});
		
		if(count === 0){
			return response.send({
				'message': 'Mobile Number is not associated with this user',
				'status': 0
			});
		}

		return response.send({
			'message': 'Mobile Number is associated with this user',
			'status': 1
		});
	}

	static updatePin = async(request: Request, response: Response, next: NextFunction) => {
		let { pin, mobile_number } = request.body;

		let userRepository = getRepository(User);
		let userToUpdate = await userRepository.findOne({ where: {'mobile_number': mobile_number}});
		userToUpdate.pin = pin;
		userToUpdate.status = true;

        userToUpdate.hashPin();
		
		try {
			await userRepository.save(userToUpdate);
		} catch (error) {
			return response.status(422).send({"message": "Unable to update User PIN."});
		}

		return response.send({"message": "PIN Successfully Updated."});
	}

	static checkPinStatus = async(request: Request, response: Response, next: NextFunction) => {
		let { identity, via } = request.body;

		let userRepository = getRepository(User);
		let count: any;
		let user: any;

		if(via === 'mobile') {
			count = await userRepository.count({ where: {'mobile_number': identity}});
			user = await userRepository.findOne({ where: {'mobile_number': identity}});
		}
		else if(via === 'email') {
			count = await userRepository.count({ where: {'email': identity}});
			user = await userRepository.findOne({ where: {'email': identity}});
		}
		else{
			return response.status(422).send({
				"message": "Invalida pameter value for 'via' - must be email or mobile onle."
			}); 
		}

		if(count !== 0) {
			if(user.pin !== null) {
				return response.send({
					"status": true,
					"message": "This user has PIN ALREADY."
				}); 
			}
		} else {
			return response.status(422).send({
				"message": "NO user associated with the given identity."
			});
		}

		return response.send({
			"status": false,
			"message": "This user has NO PIN SETUP YET."
		});
	}
}