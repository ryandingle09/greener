import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { Store } from "../models/Store";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class NewMerchants202105081620416524917 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        fs.createReadStream('src/migrations/csv/store_new_20210508.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let seed = [];

            for(let i in results) {
                seed.push({
                    'name': results[i].GreenBusinessName,
                    'logo': results[i].Logo,
                    'anzic_code': results[i].anzic_code
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Store)
            .values(seed)
            .execute();
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
