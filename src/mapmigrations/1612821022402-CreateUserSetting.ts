import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import {User} from "../models/User";
import {UserSetting} from "../models/UserSetting";

export class CreateUserSetting1612821022402 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let u = getRepository(User);

        let users = await u.find({select: ['id']});
        let userData = [];

        for(let i in users){
            let items = { 'user_id': users[i].id};
            userData.push(items);

            console.log(items);
        }

        console.log(userData);

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(UserSetting)
            .values(userData)
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
