import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { User } from "../models/User";
import { HashHelper } from "../helpers/hasher";

export class PopulateEncyptedUserId1620278728382 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let repo = getRepository(User);
		let users = await getRepository(User).createQueryBuilder("user").getRawMany();
        for(let i in users) {
            let encryptId = await HashHelper.encrypt(users[i].id);

            let data = await repo.findOneOrFail(users[i].user_id);
            data.encrypted_id = encryptId;
    
            await repo.save(data);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
