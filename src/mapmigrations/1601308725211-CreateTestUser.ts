import {MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { User } from "../models/User";

export class CreateTestUser1601308725211 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        let admin = new User();

        admin.email = 'admin_seed@gmail.com';
        admin.mobile_number = "639176780409";
        admin.password = "password";
        admin.hashPassword();
        admin.pin = '123456';
        admin.hashPin();
        admin.role = "0";
        admin.status = true;

        let repo = getRepository(User);
        await repo.save(admin);

        let test = new User();

        test.email = 'test_seed@gmail.com';
        test.mobile_number = "639065601556";
        test.password = "password";
        test.hashPassword();
        test.pin = '123456';
        test.hashPin();
        test.role = "1";
        test.status = true;

        const userRepository = getRepository(User);
        await userRepository.save(test);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
