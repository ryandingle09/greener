import {MigrationInterface, QueryRunner, getConnection, getRepository } from "typeorm";
import { Store } from "../models/Store";
import { StoreAddress } from "../models/StoreAddress";

const csv = require('csv-parser');
const fs = require('fs');
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store_address_20210526.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class NewStoreAddress202105261622035185262 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let storesData = [];
        let data = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let seed = [];
        for(let i in results) {
            let id;

            for(let a in storesData) {
                if(results[i].GreenBusinessName.toLowerCase() === storesData[a].name.toLowerCase()) {
                    id = storesData[a].id;
                }
            }

            if(id == null) {
                continue;
            }
            
            seed.push({
                'storeId': id,
                'address': results[i].DisplayAddress,
                'latitude': results[i].Latitude,
                'longitude': results[i].Longitude
            });
        }

        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(StoreAddress)
        .values(seed)
        .execute();

        console.log("seed data: " + JSON.stringify(seed));
    }
    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
