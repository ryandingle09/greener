import * as serviceAccount from '../firebase/gree-293405-firebase-adminsdk-ginhy-5109a02860.json';

const admin = require("firebase-admin");
const env = process.env;

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://gree-293405.firebaseio.com"
});

export class FirebaseHelper{

  static notifyDevice = async(fcmToken, payload, options) => {
    let registrationToken = fcmToken;

    admin.messaging().sendToDevice(registrationToken, payload, options)
      .then(function(response) {
        return true;
      })
      .catch(function(error) {
        console.log(error);
      });

    return true;
  }

}
