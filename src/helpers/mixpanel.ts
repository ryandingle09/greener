const Mixpanel = require('mixpanel');
const env = process.env;

export class MixpanelHelper{

    static track = async(event_name, data) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN);
            mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {protocol: 'https'});
            data['environment'] = env.ENVIRONMENT;
            
            mixpanel.track(event_name, data);
        } catch (e) {
            console.log(e);
        }
    }
    
    static import_batch = async(events) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {
                secret: env.MIXPANEL_SECRET
            });
            mixpanel.import_batch(events);
        } catch (e) {
            console.log(e);
        }
    }

    static track_batch = async(events) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {
                secret: env.MIXPANEL_SECRET
            });
            mixpanel.track_batch(events);
        } catch (e) {
            console.log(e);
        }
    }

    static set_once = async(alias, data) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN);
            mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {protocol: 'https'});
            data['environment'] = env.ENVIRONMENT;
    
            mixpanel.alias(alias);
            mixpanel.people.set_once(alias, data);
        } catch (e) {
            console.log(e);
        }
    }

    static set = async(alias, data) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN);
            mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {protocol: 'https'});
            data['environment'] = env.ENVIRONMENT;
            mixpanel.people.set(alias, data);
        } catch (e) {
            console.log(e);
        }
    }

    static alias = async(alias) => {
        try{
            let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN);
            mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN, {protocol: 'https'});

            mixpanel.alias(alias);
        } catch (e) {
            console.log(e);
        }
    }

}