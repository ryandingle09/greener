import { getRepository, getConnection } from "typeorm";
import { RatingHistory } from "../models/RatingHistory";
import { Classification } from "../models/Classification";
import { StoreSubSector } from "../models/StoreSubSector";
import { Store } from "../models/Store";
import { Account } from "../models/Account";
import { HashHelper } from "../helpers/hasher";
import { StoreNonGreener } from "../models/StoreNonGreener";
import { Sector } from "../models/Sector";
import { SubSector } from "../models/SubSector";
import { HubSpotHelper } from "../helpers/hubspot";
import { MixpanelHelper } from '../helpers/mixpanel';
import { Helper as GlobalHelper } from "./helper";

export class RatingHelper{

	static ThreeMonths = async(loginName, userId = false) => {

		// Set accounts sync to true
		let accounts_sync = true;
	
		// Check if accounts are not synced
		if(userId !== false) {
		  	let accountss = await getRepository(Account).createQueryBuilder("acc").getRawMany();
	
		 	for(let i in accountss) {
				try {
					let decryptId = await HashHelper.decrypt(accountss[i].acc_user_id);
					if(decryptId === userId) {
						if(accountss[i].acc_account_synced === false) {
							accounts_sync = false;
							break;
						}
					}
				} catch (e) {
					continue;
				}
		  	}
	
		  // If accounts are not synced yet, return false
		  if ( accounts_sync === false ) {
			return {
					'accounts_sync': accounts_sync
				};
		  }
		}

        const fromDate_90 = GlobalHelper.getDateRange(90).fromDate;
        const toDate_90 = GlobalHelper.getDateRange(90).toDate;
		let home_energy = 0;
		let diet = 0;
		let travel = 0;
		let daily_spend = 0;

		// lookups
		let home_energy_lookup = ['110','111','112','113','114','115','116'];
		let diet_lookup = ['20','21','22','23','24','25','40','41','42','43','44','45','46','47'];
		let travel_lookup = ['81','100','101','102','103','104','105'];

		let data = await getRepository(RatingHistory)
			.createQueryBuilder("calc")
			.where("calc.calculation_date BETWEEN '"+fromDate_90+"' and '"+toDate_90+"' and calc.user_id = :id", { id: loginName })
			.distinctOn(["calc.id"])
			.leftJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect(StoreSubSector, "sts", "sts.storeId = store.id")
			.leftJoinAndSelect(Classification, "cls", "cls.anzic_code = calc.category_code")
			.leftJoinAndSelect("sts.info", "info")
			.getRawMany();

		for(let d in data) {
			if(home_energy_lookup.includes(''+data[d].info_sub_code+'') === true || home_energy_lookup.includes(''+data[d].cls_sub_code+'') === true) {
				home_energy += Number(data[d].calc_rating);
			} else if(diet_lookup.includes(''+data[d].info_sub_code+'') === true || diet_lookup.includes(''+data[d].cls_sub_code+'') === true) {
				diet += Number(data[d].calc_rating);
			} else if(travel_lookup.includes(''+data[d].info_sub_code+'') === true || travel_lookup.includes(''+data[d].cls_sub_code+'') === true) {
				travel += Number(data[d].calc_rating);
			} else {
				daily_spend += Number(data[d].calc_rating);
			}
		}

		home_energy = home_energy * (365/90);
		diet = diet * (365/90);
		travel = travel * (365/90);
		daily_spend = daily_spend * (365/90);

		let total_tc02 = home_energy + diet + travel + daily_spend;
		let total_rating = (1 - total_tc02/40) * 10;

		if(home_energy === 0)  home_energy = 0;
		else  home_energy = Math.max(0,(1-(home_energy)/8.0)) * 2.00;

		if(diet === 0)  diet = 0;
		else  diet = Math.max(0,(1-(diet)/5.0)) * 1.25;

		if(travel === 0)  travel = 0;
		else  travel = Math.max(0,(1-(travel)/6.0)) * 1.50;

		if(daily_spend === 0)  daily_spend = 0;
		else  daily_spend = Math.max(0,total_rating - (home_energy + diet + travel));

		let summary = [
			{'title': 'Home & Energy', 'rating': home_energy, 'over': '2.0' },
			{'title': 'Diet', 'rating': diet, 'over': '1.25' },
			{'title': 'Travel', 'rating': travel, 'over': '1.50' },
			{'title': 'Other daily spend & bills', 'rating': daily_spend, 'over': '5.25' }
		]

		const final_rating = Math.max(0,total_rating);

		let rating = {
			'green': final_rating,
			'gray': 10 - final_rating
		}

		return {
			'breakdown': summary,
			'rating': rating,
			'accounts_sync': accounts_sync
		};
	
	}

	static PastSevenDays = async(loginName) => {
        const toDate = GlobalHelper.getDateRange(6).toDate;
        const fromDate = GlobalHelper.getDateRange(6).fromDate;

        let home_energy = 0;
		let diet = 0;
		let travel = 0;
		let daily_spend = 0;

		// lookups
		let home_energy_lookup = ['110','111','112','113','114','115','116'];
		let diet_lookup = ['20','21','22','23','24','25','40','41','42','43','44','45','46','47'];
		let travel_lookup = ['81','100','101','102','103','104','105'];

		let data = await getRepository(RatingHistory)
			.createQueryBuilder("calc")
			.where("calc.calculation_date BETWEEN '"+fromDate+"' and '"+toDate+"' and calc.user_id = :id", { id: loginName })
			.leftJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect(StoreSubSector, "sts", "sts.storeId = store.id")
			.leftJoinAndSelect(Classification, "cls", "cls.anzic_code = calc.category_code")
			.leftJoinAndSelect("sts.info", "info")
			.getRawMany();

		for(let d in data) {
			if(home_energy_lookup.includes(''+data[d].info_sub_code+'') === true || home_energy_lookup.includes(''+data[d].cls_sub_code+'') === true) {
				home_energy += Number(data[d].calc_rating);

			} else if(diet_lookup.includes(''+data[d].info_sub_code+'') === true || diet_lookup.includes(''+data[d].info_sub_code+'') === true) {
				diet += Number(data[d].calc_rating);

			} else if(travel_lookup.includes(''+data[d].info_sub_code+'') === true || travel_lookup.includes(''+data[d].info_sub_code+'') === true) {
				travel += Number(data[d].calc_rating);

			} else {
				daily_spend += Number(data[d].calc_rating);

			}
		}

		home_energy = home_energy * (365/90);
		diet = diet * (365/90);
		travel = travel * (365/90);
		daily_spend = daily_spend * (365/90);

		let total_tc02 = (home_energy + diet + travel + daily_spend);
		let total_rating = (1 - total_tc02/40) * 10;

		if(home_energy === 0)  home_energy = 0;
		else  home_energy = Math.max(0,(1-(home_energy)/8.0)) * 2.00;

		if(diet === 0)  diet = 0;
		else  diet = Math.max(0,(1-(diet)/5.0)) * 1.25;

		if(travel === 0)  travel = 0;
		else  travel = Math.max(0,(1-(travel)/6.0)) * 1.50;

		if(daily_spend === 0)  daily_spend = 0;
		else  daily_spend = Math.max(0,total_rating - (home_energy + diet + travel));

		let summary = [
			{'title': 'Home & Energy', 'rating': home_energy, 'over': '2.0' },
			{'title': 'Diet', 'rating': diet, 'over': '1.25' },
			{'title': 'Travel', 'rating': travel, 'over': '1.50' },
			{'title': 'Other daily spend & bills', 'rating': daily_spend, 'over': '5.25' }
		]
		
		const final_rating = Math.max(0,total_rating);

		let rating = {
			'green': final_rating,
			'gray': 10 - final_rating
		}

		return {
			'breakdown': summary,
			'rating': rating
		};
    }

	static setHubsPotTopThreeMerchants = async(loginName, hubsPotId, change_over_last_week, fromDate, toDate) => {
		const storePositions = ['','second_','third_'];
		let properties = [];

		function arrayUnique(arr, uniqueKey) {
			const uniqueList = []
			return arr.filter(function(item) {
			  if (uniqueList.indexOf(item[uniqueKey]) === -1) {
				uniqueList.push(item[uniqueKey])
				return true
			  }
			})
		}

		properties.push({ property: 'g_greener_rating_change_over_the_last_week', value: change_over_last_week.toFixed(2) });

		// greener stores
		let greener_stores = await getRepository(RatingHistory)
			.createQueryBuilder("calc")
			.addSelect("((calc.spend * class.emission_factor_gc02) / 1000)", "carbon")
			.where("calc.calculation_date BETWEEN '"+fromDate+"' and '"+toDate+"' and calc.for_recommendation IS FALSE and calc.store_name != '' and calc.user_id = :id", { id: loginName })
			.distinctOn(["calc.id", "carbon"])
			.leftJoinAndSelect(Classification, "class", "class.anzic_code = calc.category_code")
			.leftJoinAndSelect(Store, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect("stores.sector", "sector")
			.leftJoinAndSelect("stores.subsectors", "subsectors")
			.leftJoinAndSelect("subsectors.info", "subsector_info")
			.orderBy("carbon", "DESC")
			.limit(3)
			.getRawMany();

		greener_stores.sort((a, b) => Number(b.carbon) - Number(a.carbon));

		const unique_greener_stores = arrayUnique(greener_stores, 'stores_name');

		// for greener store name and logo
		for(let i in unique_greener_stores){ 
			properties.push({ property: `g_greener_store_name_${storePositions[i]}highest_CO2_avoided_previous_7_days`, value: unique_greener_stores[i].stores_name }); 
			properties.push({ property: `g_greener_store_name_${storePositions[i]}highest_CO2_avoided_previous_7_days_logo`, value: unique_greener_stores[i].stores_logo }); 
			properties.push({ property: `g_greener_store_category_${storePositions[i]}highest_co2_avoided_previous_7_days`, value: unique_greener_stores[i].sector_name }); 
			properties.push({ property: `g_greener_store_subcategory_${storePositions[i]}highest_co2_avoided_previous_7_days`, value: unique_greener_stores[i].subsector_info_name }); 
		}

		// greener stores
		let nongreener_stores = await getRepository(RatingHistory)
			.createQueryBuilder("calc")
			.addSelect("((calc.spend * class.emission_factor_gc02) / 1000)", "carbon")
			.where("calc.calculation_date BETWEEN '"+fromDate+"' and '"+toDate+"' and calc.for_recommendation IS TRUE and calc.store_name != '' and calc.user_id = :id", { id: loginName })
			.distinctOn(["calc.id", "carbon"])
			.leftJoinAndSelect(StoreNonGreener, "stores", "LOWER(stores.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect(Classification, "class", "class.anzic_code = calc.category_code")
			.leftJoinAndSelect(SubSector, "sub_sector", "sub_sector.sub_code::varchar = class.sub_code")
			.leftJoinAndSelect(Sector, "sector", "sector.id = class.sector_id::uuid")
			.orderBy("carbon", "DESC")
			.limit(3)
			.getRawMany();
		
		nongreener_stores.sort((a, b) => Number(b.carbon) - Number(a.carbon));

		let unique_nongreener_stores = arrayUnique(nongreener_stores, 'stores_name');
		unique_nongreener_stores = arrayUnique(nongreener_stores, 'calc_store_name');

		// for greener store name and logo
		for(let i in unique_nongreener_stores){ 
			let store_name = unique_nongreener_stores[i].stores_name;
			let store_logo = unique_nongreener_stores[i].stores_logo;
			let sector_name = unique_nongreener_stores[i].sector_name;
			let sub_sector_name = unique_nongreener_stores[i].sub_sector_name;

			if(unique_nongreener_stores[i].stores_name === null) store_name = unique_nongreener_stores[i].calc_store_name;
				store_name = store_name.replace(/(^\w|\s\w)/g, m => m.toUpperCase());

			properties.push({ property: `g_other_store_name_${storePositions[i]}highest_CO2_previous_7_days`, value: store_name }); 
			properties.push({ property: `g_other_store_name_${storePositions[i]}highest_CO2_previous_7_days_logo`, value: store_logo }); 
			properties.push({ property: `g_other_store_category_${storePositions[i]}highest_co2_previous_7_days`, value: sector_name }); 
			properties.push({ property: `g_other_store_subcategory_${storePositions[i]}highest_co2_previous_7_days`, value: sub_sector_name }); 
		}

		let check = (property) => {
			return properties.some(function(el) {
				return el.property === property;
			});
		}

		// check if properties exist and add blank value to reset in hubspot
		for(let sp in storePositions){
			//greener
			if(check('g_greener_store_name_'+storePositions[sp]+'highest_CO2_avoided_previous_7_days') === false) 
				properties.push({ property: `g_greener_store_name_${storePositions[sp]}highest_CO2_avoided_previous_7_days`, value: '' });

			if(check('g_greener_store_name_'+storePositions[sp]+'highest_CO2_avoided_previous_7_days_logo') === false) 
				properties.push({ property: `g_greener_store_name_${storePositions[sp]}highest_CO2_avoided_previous_7_days_logo`, value: '' });

			if(check('g_greener_store_category_'+storePositions[sp]+'highest_co2_avoided_previous_7_days') === false) 
				properties.push({ property: `g_greener_store_category_${storePositions[sp]}highest_co2_avoided_previous_7_days`, value: '' });

			if(check('g_greener_store_subcategory_'+storePositions[sp]+'highest_co2_avoided_previous_7_days') === false) 
				properties.push({ property: `g_greener_store_subcategory_${storePositions[sp]}highest_co2_avoided_previous_7_days`, value: '' });

			//non greener
			if(check('g_other_store_name_'+storePositions[sp]+'highest_CO2_previous_7_days') === false) 
				properties.push({ property: `g_other_store_name_${storePositions[sp]}highest_CO2_previous_7_days`, value: '' });

			if(check('g_other_store_name_'+storePositions[sp]+'highest_CO2_previous_7_days_logo') === false) 
				properties.push({ property: `g_other_store_name_${storePositions[sp]}highest_CO2_previous_7_days_logo`, value: '' });

			if(check('g_other_store_category_'+storePositions[sp]+'highest_co2_previous_7_days') === false) 
				properties.push({ property: `g_other_store_category_${storePositions[sp]}highest_co2_previous_7_days`, value: '' });

			if(check('g_other_store_subcategory_'+storePositions[sp]+'highest_co2_previous_7_days') === false) 
				properties.push({ property: `g_other_store_subcategory_${storePositions[sp]}highest_co2_previous_7_days`, value: '' });
		}

		let hub = new HubSpotHelper();
		let hubReturn;
		let hubsObject = { properties: properties }

		try{
			hubReturn = await hub.updateContact(hubsPotId, hubsObject);
		} catch(e) {
			let er = {
				'error': e,
				'message': 'Please contact greener support.'
			}

			console.log(er);
		}
	}

	static syncRatingToHubspotAndMixpanel = async(user) => {
		const halo = await RatingHelper.ThreeMonths(user.basiq_id);
		
		// hubspot
		let properties = [];
			properties.push({ property: 'g_greener_rating_current', value: halo.rating.green.toFixed(2) });

		const hub = new HubSpotHelper();
		const hubsObject = { properties: properties }

		try{
			await hub.updateContact(user.hubspot_id, hubsObject);
		} catch(e) {
			// pass
			console.log(e);
		}

		// mixpanel
		let mixData= {}
        	mixData['halo_rating'] = halo.rating.green.toFixed(2);
			
		MixpanelHelper.set(user.encrypted_id, mixData);
	}

	static linkedAccounts = async(user_id) => {
		const acAr = [];
		const accounts = await getRepository(Account).createQueryBuilder("acc").getRawMany();

		for(let i in accounts) {
			try {
				let decryptId = await HashHelper.decrypt(accounts[i].acc_user_id);
				if(decryptId === user_id) {
					acAr.push(accounts[i].acc_created_at);
				}
			} catch (e) {
				continue;
			}
		}

		return acAr.length;
	}

	static findOcc(arr, key){
        let arr2 = [];

        arr.forEach((x)=>{

           if(arr2.some((val)=>{ return val[key] == x[key] })) {

             arr2.forEach((k)=>{
               if(k[key] === x[key]){
                 k["purchase_number_count"]++
               }
            })

           } else {

             let a = {}

             a['store_id'] = x['store_id']
             a['sector_id'] = x['sector_id']
             a['rating'] = x['rating']
             a['subsector_id'] = x['subsector_id']
             a['logo'] = x['logo']
             a[key] = x[key]
             a["purchase_number_count"] = 1
             arr2.push(a);

           }
        });

        arr2.sort((a, b) => Number(b.purchase_number_count) - Number(a.purchase_number_count));

        return arr2
	}

	static setCelebrated = async() => {
		let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today)
        yesterday.setDate(yesterday.getDate() - 2);

        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);
        let days_90 = new Date(yesterday);
        days_90.setDate(yesterday.getDate() - 7);

        let days_90_last = days_90.getFullYear()+'-'+("0"+(days_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90.getDate())).slice(-2);
        let fromDate:any = days_90_last;
        let toDate:any = last;

		// set celebrated=true for transactions les than the current fetch caseu it was celebrated already
		await getConnection()
			.createQueryBuilder()
			.update(RatingHistory)
			.set({ celebrated: true })
			.where("calculation_date between '"+fromDate+"' and '"+toDate+"' and for_recommendation IS FALSE")
			.execute();

		return true;
	}

	static setGreenerOffset = async(loginName) => {
		let greener_store = await getRepository(RatingHistory)
			.createQueryBuilder("calc")
			.where("calc.user_id = :id and for_recommendation IS FALSE", { id: loginName })
			.leftJoinAndSelect(Store, "store", "LOWER(store.name) = LOWER(calc.store_name)")
			.leftJoinAndSelect("store.subsectors", "subsector")
			.limit(1)
			.orderBy("calc.calculation_date", "DESC")
			.getRawOne();

		// get lookup table data for rating computation
		let repoLookUp = getRepository(Classification);
		let lookup = await repoLookUp.createQueryBuilder("lookup").getMany();

		let base = (375 / 1000000);
		let life_style = 1;
		let status = 1;
		let trees = 0;

		if(greener_store) {
			// classification lookup
			for(let b in lookup){
				if(greener_store.calc_category_code === lookup[b].anzic_code) {
					base = (lookup[b].emission_factor_gc02 / 1000000);
					break;
				}
			}

			let rating_tc02 = base * 1 * life_style * greener_store.calc_spend;

			trees = (rating_tc02 * 2);

			if(trees == null && trees == 0) {
				trees = 0;
			}

			// set to celebrated
			await getConnection()
				.createQueryBuilder()
				.update(RatingHistory)
				.set({ celebrated: true })
				.where("id = :id", { id: greener_store.calc_id })
				.execute();
		}
		else
		{
			status = 0;
			trees = 0;
			greener_store = 'No recent greener store found.';
		}

		return {
			status: status, 
			trees: trees, 
			greener_store: greener_store
		}
	}

}
