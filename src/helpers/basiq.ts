const axios = require('axios');
const qs = require('qs');
const env = process.env;
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

export class BasiqHelper{

    request = async({endpoint, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.basiqURL + endpoint,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    requestSpecifyUrl = async({endpoint, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: endpoint,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    requestEnrich = async({endpoint, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.basiqEnrichURL + endpoint,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    async getMerchantInfo(queryParam) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        try {
            let response = await this.requestEnrich({
                endpoint: '?q='+ queryParam,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async generateToken(scope='SERVER_ACCESS') {
        let grant_type = 'client_credentials';
        let key = env.basiqAPIKEY;
        let access_token = myCache.get( "access_token" );

        if ( access_token == undefined ){
            console.log('Token is expired or cached token not found!');
            let response = await this.request({
                endpoint: '/token',
                headers: {
                    Authorization: 'Basic '+key+'',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'basiq-version': env.basiqAPIVersion
                },
                data: {
                    scope: scope,
                    grant_type: grant_type
                },
            });

            console.log(response);

            myCache.mset([{key: "access_token", val: response.data.access_token, ttl: 3000}]);

            return response.data.access_token;
        }

        console.log('Cached token not yet expired and detected!');
        return access_token;
    }

    async generateTokenFrontEnd() {
        let grant_type = 'client_credentials';
        let key = env.basiqAPIKEY;

        let response = await this.request({
            endpoint: '/token',
            headers: {
                Authorization: 'Basic '+key+'',
                'Content-Type': 'application/x-www-form-urlencoded',
                'basiq-version': env.basiqAPIVersion
            },
            data: {
                scope: 'CLIENT_ACCESS',
                grant_type: grant_type
            },
        });

        return response.data.access_token;
    }

    async generateTokenApi(scope='SERVER_ACCESS') {
        let access_token = myCache.get( "access_token" );

        if ( access_token == undefined ){
            console.log('Token is expired or cached token not found!');
            let key = env.basiqAPIKEY;
            let response = await this.request({
                endpoint: '/token',
                headers: {
                    Authorization: 'Basic '+key+'',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'basiq-version': env.basiqAPIVersion
                },
                data: {
                    scope: scope
                },
            });

            console.log(response);

            myCache.mset([{key: "access_token", val: response.data.access_token, ttl: 3000}]);

            return response.data.access_token;
        }

        console.log('Cached token not yet expired and detected!');
        return access_token;
    }

    async generateTokenPerUser(scope='SERVER_ACCESS') {
        let grant_type = 'client_credentials';
        let key = env.basiqAPIKEY;

        try {
            let access_token = myCache.get( "access_token" );

            if ( access_token == undefined ){
                console.log('Token is expired or cached token not found!');
                let response = await this.request({
                    endpoint: '/token',
                    headers: {
                        Authorization: 'Basic '+key+'',
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'basiq-version': env.basiqAPIVersion,
                    },
                    data: {
                        scope: scope,
                        grant_type: grant_type
                    },
                });

                myCache.mset([{key: "access_token", val: response.data.access_token, ttl: 3000}]);

                return response.data.access_token;
            }

            console.log('Cached token not yet expired and detected!');
            return access_token;

        } catch(e) {
            console.log(e);
        }
    }

    async createConnection(loginName) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        let test_cred = {
            "loginId": "Wentworth-Smith",
            "password": "whislter",
            "institution":{
              "id":"AU00000"
            }
        }
        try {
            let response = await this.request({
                endpoint: '/users/'+loginName+'/connections',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: test_cred,
            });

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async createUser(UserData) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        try {
            let response = await this.request({
                endpoint: '/users',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: UserData,
            });

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async updateUser(loginName, UserData) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        try {
            let response = await this.request({
                endpoint: '/users/'+loginName,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: UserData,
            });

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getUser(loginName) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        try {
            let response = await this.request({
                endpoint: '/users/'+loginName,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: '',
            }, 'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async deleteUser(loginName) {
        let authToken = await this.generateTokenPerUser('SERVER_ACCESS');
        try {
            let response = await this.request({
                endpoint: '/users/'+loginName,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data: '',
            }, 'delete');

            return;

        } catch(e) {
            console.log(e);
        }
    }

    async syncAccounts(loginName) {
        let options = '/users/'+loginName+'/accounts';
        let authToken = await this.generateTokenApi('SERVER_ACCESS');

        try {
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response;

        } catch(e) {
            console.log(e);
        }
    }

    async getAccount(loginName, accountId) {
        let options = '/users/'+loginName+'/accounts/'+accountId;
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async getAccounts(loginName) {
        let authToken = await this.generateTokenPerUser(loginName);
        let options = '/users/'+loginName+'/accounts';

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async syncTransactionsByAccountId(loginName, accountId, isNext, fromDate, toDate) {

      let options = "/users/"+loginName+"/transactions?filter=account.id.eq('"+accountId+"'),transaction.class.eq('payment')";
      if ( fromDate && toDate ) {
        options += ",transaction.postDate.bt('"+fromDate+"','"+toDate+"')";
      }

      if ( isNext ) {
        options = isNext;
      }

      let authToken = await this.generateTokenPerUser(loginName);

      try {

        let response;

        if ( isNext ) {

          response = await this.requestSpecifyUrl({
            endpoint: options,
            headers: {
              'Accept': 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
            data : ''
          }, 'get');

        } else {

          response = await this.request({
            endpoint: options,
            headers: {
              'Accept': 'application/json',
              Authorization: `Bearer ${authToken}`,
            },
            data : ''
          }, 'get');

        }

        return response;

      } catch(e) {
        console.log(e);
      }
    }

    async syncTransactions(loginName, fromDate=false, toDate=false, isNext=false) {
        let options = "/users/"+loginName+"/transactions?filter=transaction.postDate.bt('"+fromDate+"','"+toDate+"'),transaction.class.eq('payment')";

        if(fromDate === false && toDate === false) {
            options = "/users/"+loginName+"/transactions?filter=transaction.class.eq('payment')";
        }

        let authToken = await this.generateTokenPerUser(loginName);

        console.log(options);

        try {
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response;

        } catch(e) {
            console.log(e);
        }
    }

    async syncNextTransactions(loginName, isNext) {
        let options = isNext;
        let authToken = await this.generateTokenPerUser(loginName);

        console.log(options);

        try {
            let response = await this.requestSpecifyUrl({
                endpoint: options,
                headers: {
                    'Accept': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response;

        } catch(e) {
            console.log(e);
        }
    }

    async getInstitution(loginName, bankId) {
        let options = '/institutions/'+bankId;
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async getConnection(loginName, connectionId) {
        let options = '/users/'+loginName+'/connections/'+connectionId;
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async refreshConnection(loginName, connectionId) {
        let options = '/users/'+loginName+'/connections/'+connectionId+'/refresh';
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            });

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async getJob(loginName, jobid) {
        let options = '/jobs/'+jobid;
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async getJobs(loginName) {
        let options = '/users/'+loginName+'/jobs';
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async getJobFromConnection(loginName, connectionId) {
        let options = '/users/'+loginName+'/jobs?filter=connection.id.eq('+connectionId+')';
        let authToken = await this.generateTokenPerUser(loginName);

        try{
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

}
