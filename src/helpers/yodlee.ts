const axios = require('axios');
const qs = require('qs');
const env = process.env;

export class YodleeHelper{

    request = async({endpoint, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.yodleeAPIUrl + endpoint,
            headers: {
              'Api-Version': env.yodleeAPIVersion,
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    async generateToken(isAdmin=false) {
        let loginName = env.yodleeAdminUserId;//(isAdmin) ? env.yodleeAdminUserId : env.yodleeUserName;
        let response = await this.request({
            endpoint: '/auth/token',
            headers: {
                'loginName': loginName,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify({
                clientId: env.yodleeClientId,
                secret: env.yodleeSecret,
            }),
        });
    
        return response.data.token.accessToken;
    }

    async generateTokenPerUser(loginName) {
        let response = await this.request({
            endpoint: '/auth/token',
            headers: {
                'loginName': loginName,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify({
                clientId: env.yodleeClientId,
                secret: env.yodleeSecret,
            }),
        });
    
        return response.data.token.accessToken;
    }

    async getTransactionCategories() {
        let authToken = await this.generateToken(true);
        let response = await this.request({
            endpoint: '/transactions/categories',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'get');

        return response.data.transactionCategory;
    }

    async getTransactions(loginName, fromDate=false, toDate=false, accountId=false) {
        let options = '/transactions?param=fake';
        if(fromDate) options = options+'&fromDate='+fromDate;
        if(toDate) options = options+'&toDate='+toDate;
        if(accountId) options = options+'&accountId='+accountId;

        try{
            let authToken = await this.generateTokenPerUser(loginName);
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response.data;
        } catch(e) {
            console.log(e);
        }
    }

    async syncTransactions(loginName, fromDate=false, toDate=false, accountId=false) {
        let options = '/transactions?param=fake';

        if(fromDate) options = options+'&fromDate='+fromDate;
        if(toDate) options = options+'&toDate='+toDate;
        if(accountId) options = options+'&accountId='+accountId;

        try {
            let authToken = await this.generateTokenPerUser(loginName);
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response;

        } catch(e) {
            console.log(e);
        }
    }

    async syncAccounts(loginName) {
        let options = '/accounts';

        try {

            let authToken = await this.generateTokenPerUser(loginName);
            let response = await this.request({
                endpoint: options,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${authToken}`,
                },
                data : ''
            }, 'get');

            return response;

        } catch(e) {
            console.log(e);
        }
    }
  
    async createUser({ loginName, email, name, address='' }) {
        let authToken = await this.generateToken(true);
        let data = JSON.stringify({
            user: {
                loginName: loginName,
                email: email,
                name: name,
                address: address,
                preferences: {
                    currency: 'AUD',
                    timeZone: 'AEST',
                    dateFormat: 'DD-MM-YYYY',
                    locale: 'en_AU',
                }
            }
        });

        let response = await this.request({
            endpoint: '/user/register',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data: data,
        });

        return response.data;
    }

    async deleteAccount(loginName, accountId) {
        let authToken = await this.generateTokenPerUser(loginName);
        let response = await this.request({
            endpoint: '/accounts/'+accountId,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'delete');

        return response.data;
    }

    // async createAccount() {
        
    // }

    async getAccount(loginName, accountId, container='bank', include='profile,holder,fullAccountNumberList,paymentProfile,autoRefresh') {
        let authToken = await this.generateTokenPerUser(loginName);

        // container query ?container=bank/creditCard/investment/insurance/loan/reward/bill/realEstate/otherAssets/otherLiabilities
        // include query?profile,holder,fullAccountNumberList,paymentProfile,autoRefresh

        let response = await this.request({
            endpoint: '/accounts/'+accountId+'?include='+include+'&container='+container,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'get');

        return response.data;
    }

    async getAccounts(loginName) {
        let authToken = await this.generateTokenPerUser(loginName);

        // container query ?container=bank/creditCard/investment/insurance/loan/reward/bill/realEstate/otherAssets/otherLiabilities
        // include query?profile,holder,fullAccountNumberList,paymentProfile,autoRefresh
        
        let response = await this.request({
            endpoint: '/accounts',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'get');

        return response.data;
    }

    async getBank(id) {
        let authToken = await this.generateToken(true);
        let response = await this.request({
            endpoint: '/providers/'+id,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'get');

        return response.data;
    }

    async getBankList(name=false, priority=false, skip=false, top=false, providerIds=false) {
        let authToken = await this.generateToken(true);
        let options = '/providers?param=fake';

        if(name) options = options+'&name='+name;
        //if(priority) options = options+'&priority='+name;
        if(skip) options = options+'&skip='+skip;
        if(top) options = options+'&top='+top;
        if(providerIds) options = options+'&providerIds='+providerIds;
        
        let response = await this.request({
            endpoint: options,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            data : ''
        }, 'get');

        return response.data;
    }

}
