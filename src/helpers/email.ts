
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const env = process.env;

export class EmailHelper{

    static notify = async(to, subject, content) => {
        // send email
		let transporter = nodemailer.createTransport(smtpTransport({
			service: env.SMTP_SERVICE,
			host: env.SMTP_HOST,
			auth: {
				user: env.SMTP_USERNAME,
				pass: env.SMTP_PASSWORD
			}
	   }));
	   
	   let mailOptions = {
			from: env.SMTP_FROM,
			to: to,
			subject: subject,
			// html: 'message'
			text: content
		}

		transporter.sendMail(mailOptions, function(error, info){
			if (error) {
                console.log(error);  
                return false;
			} else {     
                console.log('Email sent: ' + info.response);  
                return true;
			}   
	   	});
    }

}