const axios = require('axios');
const env = process.env;

export class GoogleHelper{

    request = async({params, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.googleMapUrl+'maps/api/place/findplacefromtext/json' + params,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    request2 = async({params, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.googleMapUrl+'maps/api/place/textsearch/json' + params,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }

    request3 = async({params, headers, data}, method = 'post') => {
        let configuration = {
            method: method,
            url: env.googleMapUrl+'maps/api/place/photo' + params,
            headers: {
              ...headers,
            },
            data: data,
        }

        return axios(configuration);
    }
    
    async getPriceLevel(input) {
        let inputtype = 'textquery';
        let fields = 'photos,formatted_address,name,rating,price_level,opening_hours,geometry';

        try {
            let response = await this.request({
                params: '?key='+env.googleMapKey+'&input='+input+'&inputtype='+inputtype+'&fields='+fields+'',
                headers: {
                    'Accept': '*/*',
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getPlacePhotoReference(input) {
        let inputtype = 'textquery';
        let fields = 'photos';

        try {
            let response = await this.request({
                params: '?key='+env.googleMapKey+'&input='+input+'&inputtype='+inputtype+'&fields='+fields+'&region=AU',
                headers: {
                    'Accept': '*/*',
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getStoreAddresses(input) {
        let fields = 'photos,formatted_address,name,rating,price_level,opening_hours,geometry';
        let params = '?query='+input+'&region=AU&fields='+fields+'&key='+env.googleMapKey+'';

        try {
            let response = await this.request2({
                params: params,
                headers: {
                    'Accept': '*/*',
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getPhotoFromAddresses(input) {
        let fields = 'photos';
        let params = '?query='+input+'&region=AU&fields='+fields+'&key='+env.googleMapKey+'';

        try {
            let response = await this.request2({
                params: params,
                headers: {
                    'Accept': '*/*',
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getStoreAddressesNext(input, next_page_token) {
        let fields = 'photos,formatted_address,name,rating,price_level,opening_hours,geometry';
        let params = '?query='+input+'&region=AU&fields='+fields+'&key='+env.googleMapKey+'&pagetoken='+next_page_token;

        try {
            let response = await this.request2({
                params: params,
                headers: {
                    'Accept': '*/*',
                },
                data: '',
            },'get');

            return response.data;

        } catch(e) {
            console.log(e);
        }
    }

    async getPlacePhoto(maxwidth, maxheight, photoreference) {
        let params = '?maxwidth='+maxwidth+'&maxheight='+maxheight+'&photoreference='+photoreference+'&key='+env.googleMapKey;
        let url = env.googleMapUrl+'maps/api/place/photo' + params;

        return url;
    }

}