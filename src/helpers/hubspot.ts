const axios = require('axios');
const qs = require('qs');
const env = process.env;

export class HubSpotHelper{

    request = async(data, endpoint = env.hubSpotUrl,method = 'POST') => {
        let configuration = {
            method: method,
            url: endpoint +"?hapikey="+ env.hubSpotKey,
            qs: { hapikey: env.hubSpotKey },
            headers: { 'Content-Type': 'application/json' },
            body: data,
            data: data,
            json: true 
        }

        return axios(configuration);
    }

    async createContact(data) {
        let response = await this.request(data);
        return response.data;
    }

    async updateContact(vid, data) {
        let endpoint =  env.hubSpotUrl+ "vid/" + vid +"/profile";
        let response = await this.request(data, endpoint);

        return response.data;
    }

    async createOrUpdate(email, data) {
        let endpoint =  env.hubSpotUrl+ "createOrUpdate/email/" + email +"/";
        let response = await this.request(data, endpoint);

        return response.data;
    }

    async checkContactbyEmail(email) {
        let endpoint =  env.hubSpotUrl+ "email/" + email +"/profile";
        let response = await this.request({}, endpoint, 'get');

        return response.data;
    }

    async getContactbyId(id) {
        let endpoint =  env.hubSpotUrl+ "vid/" + id +"/profile";
        let response = await this.request({}, endpoint, 'get');

        return response.data;
    }
}