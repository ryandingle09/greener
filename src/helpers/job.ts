import { getRepository, getConnection} from "typeorm";
import { Job } from "../models/Job";
import { BasiqHelper } from "../helpers/basiq";
import { FirebaseHelper } from "../helpers/firebase";

const Mixpanel = require('mixpanel');
const env = process.env;
export class JobHelper{

    static checkJobStatus = async(userId, user, connection_id, jobId, fcm_token, hubsPotId, step, saveJobId) => {
        let mixpanel = Mixpanel.init(env.MIXPANEL_TOKEN);
        let api = new BasiqHelper();
        let check = await api.getConnection(user.basiq_id, connection_id);
        let job = await api.getJob(user.basiq_id, jobId);

        // if the connection status is active stop/clear the interval and notify the user device
        if(job.steps[1].status.toString() === 'success' && job.steps[2].status.toString() === 'success') {
            let payload = {
                notification: {
                    title: "Finish set-up now to discover your Rating",
                    body: "Success! Your bank is now connected."
                },
                data: {
                    user_id: ''+userId+'',
                    institution_id: ''+check.institution.id+'',
                    status: '1',
                    message: 'success'
                }
            };

            let options = {
                priority: "normal",
                timeToLive: 60 * 60
            };

            let event_name = 'Bank Linking Success';
            let data = {
                'distinct_id': user.encrypted_id,
                'data': {
                    'job': job, 
                    'connection': check
                }
            }
            mixpanel.track(event_name, data);

            // delete job from the list
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Job)
                .where("id = :id", { id: saveJobId })
                .execute();

            FirebaseHelper.notifyDevice(fcm_token, payload, options);
        }

        if(job.steps[1].status.toString() != 'success' && job.steps[2].status.toString() != 'success') {
            let payload = {
                notification: {
                    title: "We'll just try connecting again in 24 hours.",
                    body: "Looks like your bank outage is still going. There's nothing you need to do. We've got it covered!"
                },
                data: {
                    user_id: ''+userId+'',
                    institution_id: ''+check.institution.id+'',
                    status: '0',
                    message: 'failed'
                }
            };

            let options = {
                priority: "normal",
                timeToLive: 60 * 60
            }

            let event_name = 'Bank Linking Failed';
            let data = {
                'distinct_id': user.encrypted_id,
                'first_name': user.first_name,
                'data': {
                    'job': job, 
                    'connection': check
                }
            }
            mixpanel.track(event_name, data);

            if(step === 1) {
                let newJob = new Job();
                    newJob.user_id = userId;
                    newJob.connection_id = connection_id;
                    newJob.job_id = jobId;
                    newJob.fcm_token = fcm_token;
                    newJob.step = 2;
                let repo2 = getRepository(Job);
                await repo2.save(newJob);
            }

            if(step === 2) {
                let newJob = new Job();
                    newJob.user_id = userId;
                    newJob.connection_id = connection_id;
                    newJob.job_id = jobId;
                    newJob.fcm_token = fcm_token;
                    newJob.step = 3;
                let repo2 = getRepository(Job);
                await repo2.save(newJob);
            }

            if(step === 3) {
                let newJob = new Job();
                    newJob.user_id = userId;
                    newJob.connection_id = connection_id;
                    newJob.job_id = jobId;
                    newJob.fcm_token = fcm_token;
                    newJob.step = 4;
                let repo2 = getRepository(Job);
                await repo2.save(newJob);
            }

            // delete current job as it is the previus job from previous step
            await getConnection()
                .createQueryBuilder()
                .delete()
                .from(Job)
                .where("id = :id", { id: saveJobId })
                .execute();

            FirebaseHelper.notifyDevice(fcm_token, payload, options);
        }

        return true;
    }

}