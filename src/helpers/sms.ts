const Nexmo = require('nexmo');
const env = process.env;
const client = require('twilio');

export class SmsHelper{

    static notify = async(from, mobileNumber, message) => {
        let nexmo = new Nexmo({
			apiKey: env.nextMoapiKey,
			apiSecret: env.nextMoapiSecret
		});
 
		nexmo.message.sendSms(from, mobileNumber, message, {
			type: "unicode"
		}, (err, responseData) => {
			if (err) {
				console.log(err);
			} else {
				if (responseData.messages[0]['status'] === "0") {
                    console.log("Message sent successfully.");
                    return true;
				} else {
					console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
                    return false;
				}
			}
		});
    }

	static notifyByTwillio = async(from, mobileNumber, message) => {
		try{
			client(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN).messages
				.create({
					body: message,
					from: from,
					to: mobileNumber
				})
				.then(message => console.log(message))
				.catch(e => { console.error('Got an error:', e.code, e.message); });
				
			return true;
		} catch (er) {
			console.log(er);
			return false;
		}
	}

}