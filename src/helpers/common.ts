
// // Calculate destination using range
function deg2rad(deg: number) {
    return deg * (Math.PI / 180);
}

function getDestinationLatLong({ lat,  long, range = 5 } : { lat: number; long: number;  range: number; }) {
    // φ = lat
    // λ = long
    var R = 6371; // Radius of the earth in km
    const dLat = deg2rad(lat);
    const dLong = deg2rad(long);
    const brng = deg2rad(90);
    const φ2 = Math.asin(
        Math.sin(dLat) * Math.cos(range / R) +
        Math.cos(dLat) * Math.sin(range / R) * Math.cos(brng),
    );

    const λ2 =
        dLong +
        Math.atan2(
            Math.sin(brng) * Math.sin(range / R) * Math.cos(dLat),
            Math.cos(range / R) - Math.sin(dLat) * Math.sin(φ2),
    );

    return { lat: φ2 / (Math.PI / 180), long: λ2 / (Math.PI / 180) };
}

function getDistanceFromLatLonInM({ lat1, lon1, lat2, lon2, }: { lat1: number; lat2: number; lon1: number; lon2: number }) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) *
        Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d * 1000;
}

// function getDistance(fromLat, fromLon, toLat, toLon){
//     let radius = 6371;   // Earth radius in km
//     let deltaLat = Math.toRadians(toLat - fromLat);
//     let deltaLon = Math.toRadians(toLon - fromLon);
//     let lat1 = Math.toRadians(fromLat);
//     let lat2 = Math.toRadians(toLat);
//     let aVal = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
//         Math.sin(deltaLon/2) * Math.sin(deltaLon/2) * Math.cos(lat1) * Math.cos(lat2);
//     let cVal = 2*Math.atan2(Math.sqrt(aVal), Math.sqrt(1-aVal));  

//     let distance = radius*cVal;
//     console.log("distance","radius * angle = " +distance);
//     return distance;
// }

export default getDistanceFromLatLonInM;