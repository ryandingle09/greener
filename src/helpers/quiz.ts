import {getRepository} from "typeorm";
import { QuizResponse } from "../models/QuizResponse";

export class QuizHelper{

    static getHomeEnergyResponse = async(userId) => {
        // get user quiz response
        const quizResponse = await getRepository(QuizResponse)
            .createQueryBuilder('quizresponse')
            .distinctOn(["question.code"])
            .leftJoinAndSelect('quizresponse.question', 'question')
            .leftJoinAndSelect('quizresponse.choice', 'choice')
            .where("quizresponse.userId = :userId and question.code IN ('qa','qb', 'qc')", { userId: userId })
            .orderBy("question.code", "ASC")
            .execute();
    
        return quizResponse;
    }

    static calculateHomeEnergy = async(t_amount, t_category_subdivision, quizResponse) => {
        let amount = t_amount;
        const home_energy_subcodes = ['26', '27', '28', '29'];

        if(home_energy_subcodes.includes(''+t_category_subdivision+'') === true) {

            const q1 = quizResponse[0].quizresponse_answer;
            const q2 = quizResponse[1].quizresponse_answer;
            const q3 = quizResponse[2].quizresponse_answer;
            const household_size = Number( 1.0 + ((q1 - q2 - 1) * 0.5) + (q2 * 0.3) );

            amount = (t_amount / household_size); // devide by equivalized household size

            if(q3 === '0' || q3 === '1') // Rooftop Sola or Renewable/Green Energy multiple by 0%
                amount = t_amount * 0;
            else if(q3 === '2') // Partly Green multiply 50%
                amount = t_amount * 0.5;
            
        }

        return amount;
    }
    

}
