import {getConnection, getRepository} from "typeorm";
import { Transaction } from "../models/Transaction";
import { RatingHistory } from "../models/RatingHistory";
import { StoreNonGreener } from "../models/StoreNonGreener";
import { Sector } from "../models/Sector";
import * as AWS from 'aws-sdk';
import { MixpanelHelper } from '../helpers/mixpanel';
import { QuizHelper } from "../helpers/quiz";

const moment = require('moment');
const env = process.env;

export class TransactionHelper{

  static SyncInitialTransaction = async(transactions, acAr, encrypt_loginName, lookup, stAr, loginName, stList, userId, first_name) => {
	let transArray = [];
      let transRejectArray = [];
	let ratingArray = [];
      let mixpanelEvents = [];

      for(let t in transactions) {
          // checking if transaction has no category code to be matched up
          if(transactions[t].enrich.category.anzsic.class.code === '0' || transactions[t].enrich.category.anzsic.class.code === null) {
              transRejectArray.push({
                  'status': transactions[t].status ? transactions[t].status : null,
                  'description': transactions[t].description ? transactions[t].description : null,
                  'connection': transactions[t].connection ? transactions[t].connection : null,
                  'postDate': transactions[t].postDate ? transactions[t].postDate : null,
                  'subClass': transactions[t].subClass ? transactions[t].subClass : null,
                  'enrich': transactions[t].enrich ? transactions[t].enrich : null
              });
          }

          for(let ac in acAr) {
              if(transactions[t].account === acAr[ac].account_id) {
                  let t_basiq_id = transactions[t].id ? transactions[t].id : null;
                  let t_basiq_name_encrypted = encrypt_loginName;
                  let t_store_name = transactions[t].enrich.merchant.businessName ? transactions[t].enrich.merchant.businessName : '';
                  let t_amount = transactions[t].amount ? transactions[t].amount.replace("-", "") : null;
                  let t_date = transactions[t].postDate ? transactions[t].postDate : null;
                  let t_account = transactions[t].account ? transactions[t].account : null;
                  let t_institution = transactions[t].institution ? transactions[t].institution : null;
                  let t_description = transactions[t].description ? transactions[t].description : null;
                  let t_sub_class = transactions[t].subClass.code ? transactions[t].subClass.code : null;
                  let t_website = transactions[t].enrich.merchant.website ? transactions[t].enrich.merchant.website : null;
                  let t_merchant_route_no = transactions[t].enrich.location ? transactions[t].enrich.location.routeNo : null;
                  let t_merchant_route = transactions[t].enrich.location ? transactions[t].enrich.location.route : null;
                  let t_merchant_postal_code = transactions[t].enrich.location ? transactions[t].enrich.location.postalCode : null;
                  let t_merchant_suburb = transactions[t].enrich.location ? transactions[t].enrich.location.suburb : null;
                  let t_merchant_state = transactions[t].enrich.location ? transactions[t].enrich.location.state : null;
                  let t_merchant_country = transactions[t].enrich.location ? transactions[t].enrich.location.country : null;
                  let t_merchant_formatted_address = transactions[t].enrich.location ? transactions[t].enrich.location.formattedAddress : null;
                  let t_merchant_geometry_lan = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lat : null;
                  let t_merchant_geometry_lon = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lng : null;
                  let t_category_division = transactions[t].enrich.category.anzsic.division.code ? transactions[t].enrich.category.anzsic.division.code : null;
                  let t_category_subdivision = transactions[t].enrich.category.anzsic.subdivision.code ? transactions[t].enrich.category.anzsic.subdivision.code : null;
                  let t_category_group = transactions[t].enrich.category.anzsic.group.code ? transactions[t].enrich.category.anzsic.group.code : null;
                  let t_category_class = transactions[t].enrich.category.anzsic.class.code ? transactions[t].enrich.category.anzsic.class.code : null;
                  let store_name = t_store_name;
                  let greener = 1;
                  let rating = 0;
                  let is_recommended = true;
                  let is_partner = false;

                  try {
                      for(let b in stAr){
                          let st_name = stAr[b].store_name;

                          if(st_name.toString().toLowerCase() === t_store_name.toString().toLowerCase()) {
                              is_recommended = false;
                              is_partner = true;
                          }
                      }

                      let base = (375 / 1000000);
                      let life_style = 1;

                      // classification lookup
                      for(let b in lookup){
                          if(t_category_class === lookup[b].anzic_code) {
                              base = (lookup[b].emission_factor_gc02 / 1000000);
                          }
                      }

                      rating = base * greener * life_style * t_amount;

                      let secRepo = getRepository(Sector);
                      let nGRepo = getRepository(StoreNonGreener);
                      let sector_name:any = false;
                      let merchant_id:any = false;

                      for(let i in stList) {
                          if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase()) {
                              let sector = await secRepo.findOne(stList[i].sectorId);
                              sector_name = sector.name;
                              merchant_id = stList[i].id;
                          }
                      }

                      if(sector_name === false) {
                          // classification lookup
                          for(let b in lookup){
                              if(t_category_class === lookup[b].anzic_code) {
                                  let sector = await secRepo.findOne(lookup[b].sector_id);
                                  sector_name = sector.name;

                                  let ngSt = await nGRepo.findOne({ where: {name: t_store_name}});

                                  if(ngSt) {
                                      merchant_id = ngSt.id;
                                  }
                              }
                          }
                      }

                      let st_date_str = t_date;//.toString().replace('-','/');
                      var currentDate = new Date(st_date_str);
                      var readableTDate = currentDate.getFullYear()+'-'+("0"+(currentDate.getMonth()+1)).slice(-2)+'-'+("0"+(currentDate.getDate())).slice(-2);

                      // dates for past week
                      let today_90 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
                      let yesterday_90 = new Date(today_90);
                      yesterday_90.setDate(yesterday_90.getDate() - 1);
                      let last_90 = yesterday_90.getFullYear()+'-'+("0"+(yesterday_90.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_90.getDate())).slice(-2);
                      let days_90_90 = new Date(yesterday_90);
                      days_90_90.setDate(days_90_90.getDate() - 90);
                      let days_90_last_90 = days_90_90.getFullYear()+'-'+("0"+(days_90_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90_90.getDate())).slice(-2);
                      let fromDate_90:any = days_90_last_90;
                      let toDate_90:any = last_90;

                      let today_60 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
                      let yesterday_60 = new Date(today_60);
                      yesterday_60.setDate(yesterday_60.getDate() - 1);
                      let last_60 = yesterday_60.getFullYear()+'-'+("0"+(yesterday_60.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_60.getDate())).slice(-2);
                      let days_60_60 = new Date(yesterday_60);
                      days_60_60.setDate(days_60_60.getDate() - 60);
                      let days_60_last_60 = days_60_60.getFullYear()+'-'+("0"+(days_60_60.getMonth()+1)).slice(-2)+'-'+("0"+(days_60_60.getDate())).slice(-2);
                      let fromDate_60:any = days_60_last_60;
                      let toDate_60:any = last_60;

                      let today_30 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
                      let yesterday_30 = new Date(today_30);
                      yesterday_30.setDate(yesterday_30.getDate() - 1);
                      let last_30 = yesterday_30.getFullYear()+'-'+("0"+(yesterday_30.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_30.getDate())).slice(-2);
                      let days_30_30 = new Date(yesterday_30);
                      days_30_30.setDate(days_30_30.getDate() - 30);
                      let days_30_last_30 = days_30_30.getFullYear()+'-'+("0"+(days_30_30.getMonth()+1)).slice(-2)+'-'+("0"+(days_30_30.getDate())).slice(-2);
                      let fromDate_30:any = days_30_last_30;
                      let toDate_30:any = last_30;


                      let minDate_90 = new Date(fromDate_90);
                      let maxDate_90 =  new Date(toDate_90);

                      let minDate_60 = new Date(fromDate_60);
                      let maxDate_60 =  new Date(toDate_60);

                      let minDate_30 = new Date(fromDate_30);
                      let maxDate_30 =  new Date(toDate_30);

                      let is_90 = false;
                      let is_60 = false;
                      let is_30 = false;

                      if (currentDate > minDate_90 && currentDate < maxDate_90 ){
                          console.log(currentDate, "is within 90 days range");
                          is_90 = true;
                      }
                      if (currentDate > minDate_60 && currentDate < maxDate_60 ){
                          console.log(currentDate, "is within 60 days range");
                          is_60 = true;
                      }
                      if (currentDate > minDate_30 && currentDate < maxDate_30 ){
                          console.log(currentDate, "is within 30 days range");
                          is_30 = true;
                      }


                      let dateUTC = new Date(moment.utc(currentDate));
                      let event_name = 'Old Transaction';
                      let data = {
                          'distinct_id': userId,
                          'first_name': first_name,
                          'date': readableTDate,
                          'merchantName': t_store_name,
                          'Merchant ID': merchant_id,
                          'Merchant UX category': sector_name,
                          'Merchant ANZSIC class': t_category_class,
                          'Merchant ANZSIC group': t_category_group,
                          'Merchant Greener Partner': is_partner,
                          'Value': t_amount,
                          'Last 30 days': is_30,
                          'Last 60 days': is_60,
                          'Last 90 days': is_90,
                          'time': dateUTC,
                          'Pre-link status': true,
                          'environment': env.ENVIRONMENT
                      }
                      let events = {
                          event: event_name,
                          properties: data
                      };

                      mixpanelEvents.push(events);

                  } catch(e) {
                      for(let b in stAr){
                          let st_name = stAr[b].store_name;

                          if(st_name.toString().toLowerCase() === t_store_name.toString().toLowerCase()) {
                              is_recommended = false;
                              is_partner = true;
                          }
                      }

                      let base = (375 / 1000000);
                      let life_style = 1;

                      // classification lookup
                      for(let b in lookup){
                          if(t_category_class === lookup[b].anzic_code) {
                              base = (lookup[b].emission_factor_gc02 / 1000000);
                          }
                      }

                      rating = base * greener * life_style * t_amount;

                      transRejectArray.push({
                          'status': transactions[t].status ? transactions[t].status : null,
                          'description': transactions[t].description ? transactions[t].description : null,
                          'connection': transactions[t].connection ? transactions[t].connection : null,
                          'postDate': transactions[t].postDate ? transactions[t].postDate : null,
                          'subClass': transactions[t].subClass ? transactions[t].subClass : null,
                          'enrich': transactions[t].enrich ? transactions[t].enrich : null
                      });

                      let event_name = 'Failed transaction';
                      let data = {
                          'distinct_id': userId,
                          'first_name': first_name,
                          'data': transactions[t],
                          'error_data': e
                      }

                      MixpanelHelper.track(event_name, data);
                  }

                  transArray.push({
                      'basiq_id': t_basiq_id,
                      'transaction_id': t_basiq_id,
                      'basiq_name_encrypted': t_basiq_name_encrypted,
                      'amount': t_amount,
                      'transaction_date': t_date,
                      'account_id': t_account,
                      'description': t_description,
                      'merchant_name': t_store_name,
                      'merchant_url': t_website,
                      'merchant_route_no': t_merchant_route_no,
                      'merchant_route': t_merchant_route,
                      'merchant_postal_code': t_merchant_postal_code,
                      'merchant_suburb': t_merchant_suburb,
                      'merchant_state': t_merchant_state,
                      'merchant_country': t_merchant_country,
                      'merchant_formatted_address': t_merchant_formatted_address,
                      'merchant_geometry_lan': t_merchant_geometry_lan,
                      'merchant_geometry_lon': t_merchant_geometry_lon,
                      'sub_class': t_sub_class,
                      'category_division': t_category_division,
                      'category_subdivision': t_category_subdivision,
                      'category_group': t_category_group,
                      'category_class': t_category_class,
                      'institution': t_institution
                  });

                  ratingArray.push({
                      'user_id' : loginName,
                      'store_name' : store_name.toString().toLowerCase(),
                      'spend' : t_amount,
                      'rating' : rating,
                      'calculation_date' : t_date,
                      'for_recommendation' : is_recommended,
                      'category_code' : t_category_class,
                      'after_link' : false,
                      'celebrated' : false
                  });
              }
              else{
                  // SKIPPING NOT SELECTED ACCOUNT
              }
          }
      }

      if(transRejectArray.length !== 0){
          let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
          let file_name = new Date()+'.json';

          AWS.config.update({
              accessKeyId: env.AWS_ACCESS_KEY,
              secretAccessKey: env.AWS_SECRET_KEY,
              region: env.AWS_REGION
          });

          let s3 = new AWS.S3();

          // Setting up S3 upload parameters
          let params = {
              Bucket: 'greener-wallet',
              Key: 'transactions/rejected/'+today+'/'+file_name+'',
              Body: JSON.stringify(transRejectArray),
              ContentType: 'application/json',
          };

          // Uploading files to the bucket
          s3.upload(params, async(err, data) => {
              if (err) {
                  console.log(err);
              }

              console.log("successfully uploaded!");
          });
      }

      await getConnection()
          .createQueryBuilder()
          .insert()
          .into(Transaction)
          .values(transArray)
          .onConflict(`("transaction_id") DO NOTHING`)
          .execute();

      await getConnection()
          .createQueryBuilder()
          .insert()
          .into(RatingHistory)
          .values(ratingArray)
          .execute();

      return {
          'events': mixpanelEvents,
          'total_transaction_from_basiq': transactions.length,
          'transaction_saved_count': transArray.length,
          'transaction_rating_history_saved_count': ratingArray.length,
          'transaction_failed_saved_count': transRejectArray.length,
      }
  }

  static SyncDailyTransaction = async(transactions, encrypt_loginName, lookup, loginName, quizResponse, stList, userId, first_name) => {
	    let transArray = [];
        let transRejectArray = [];
	    let ratingArray = [];
        let mixpanelEvents = [];

        for(let t in transactions) {
          // checking if transaction has no category code to be matched up
          if(transactions[t].enrich.category.anzsic.class.code === '0' || transactions[t].enrich.category.anzsic.class.code === null) {
              transRejectArray.push({
                  'status': transactions[t].status ? transactions[t].status : null,
                  'description': transactions[t].description ? transactions[t].description : null,
                  'connection': transactions[t].connection ? transactions[t].connection : null,
                  'postDate': transactions[t].postDate ? transactions[t].postDate : null,
                  'subClass': transactions[t].subClass ? transactions[t].subClass : null,
                  'enrich': transactions[t].enrich ? transactions[t].enrich : null
              });
          }

        let t_basiq_id = transactions[t].id ? transactions[t].id : null;
        let t_basiq_name_encrypted = encrypt_loginName;
        let t_store_name = transactions[t].enrich.merchant.businessName ? transactions[t].enrich.merchant.businessName : '';
        let t_amount = transactions[t].amount ? transactions[t].amount.replace("-", "") : null;
        let t_date = transactions[t].postDate ? transactions[t].postDate : null;
        let t_account = transactions[t].account ? transactions[t].account : null;
        let t_institution = transactions[t].institution ? transactions[t].institution : null;
        let t_description = transactions[t].description ? transactions[t].description : null;
        let t_sub_class = transactions[t].subClass.code ? transactions[t].subClass.code : null;
        let t_website = transactions[t].enrich.merchant.website ? transactions[t].enrich.merchant.website : null;
        let t_merchant_route_no = transactions[t].enrich.location ? transactions[t].enrich.location.routeNo : null;
        let t_merchant_route = transactions[t].enrich.location ? transactions[t].enrich.location.route : null;
        let t_merchant_postal_code = transactions[t].enrich.location ? transactions[t].enrich.location.postalCode : null;
        let t_merchant_suburb = transactions[t].enrich.location ? transactions[t].enrich.location.suburb : null;
        let t_merchant_state = transactions[t].enrich.location ? transactions[t].enrich.location.state : null;
        let t_merchant_country = transactions[t].enrich.location ? transactions[t].enrich.location.country : null;
        let t_merchant_formatted_address = transactions[t].enrich.location ? transactions[t].enrich.location.formattedAddress : null;
        let t_merchant_geometry_lan = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lat : null;
        let t_merchant_geometry_lon = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lng : null;
        let t_category_division = transactions[t].enrich.category.anzsic.division.code ? transactions[t].enrich.category.anzsic.division.code : null;
        let t_category_subdivision = transactions[t].enrich.category.anzsic.subdivision.code ? transactions[t].enrich.category.anzsic.subdivision.code : null;
        let t_category_group = transactions[t].enrich.category.anzsic.group.code ? transactions[t].enrich.category.anzsic.group.code : null;
        let t_category_class = transactions[t].enrich.category.anzsic.class.code ? transactions[t].enrich.category.anzsic.class.code : null;
        let t_logo_master = transactions[t].enrich.links ? transactions[t].enrich.links['logo-master'] : null;
        let store_name = t_store_name;
        let greener = 1;
        let rating = 0;
        let is_recommended = true;
        let is_partner = false;
        let t_actdate = transactions[t].acct_created_at;

        let t_date_raw = new Date(t_date);
        let t_date_conv = t_date_raw.getFullYear()+'-'+("0"+(t_date_raw.getMonth()+1)).slice(-2)+'-'+("0"+(t_date_raw.getDate())).slice(-2);

        let t_actdate_raw = new Date(t_actdate);
        let t_actdate_conv = t_actdate_raw.getFullYear()+'-'+("0"+(t_actdate_raw.getMonth()+1)).slice(-2)+'-'+("0"+(t_actdate_raw.getDate())).slice(-2);

        let t_after_link = (t_date_conv >= t_actdate_conv ? true : false);

        try {
            let secRepo = getRepository(Sector);
            let nGRepo = getRepository(StoreNonGreener);
            let sector_name:any = false;
            let merchant_id:any = false;

            for(let i in stList) {
                if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase() || stList[i].secondary_names !== null && stList[i].secondary_names.includes(''+t_store_name.toString().toLowerCase()+'') === true) {
                    let sector = await secRepo.findOne(stList[i].sectorId);
                    sector_name = sector.name;
                    merchant_id = stList[i].id;
                    store_name = stList[i].name;
                    is_partner = true;
                    is_recommended = false;
                    
                    if(t_after_link) greener = 0;

                    break;
                }
            }

            if(sector_name === false) {
                // classification lookup
                for(let b in lookup){
                    if(t_category_class === lookup[b].anzic_code) {
                        let sector = await secRepo.findOne(lookup[b].sector_id);
                        sector_name = sector.name;

                        let ngSt = await nGRepo.findOne({ where: {name: t_store_name}});

                        if(ngSt) {
                            merchant_id = ngSt.id;
                        }
                    }
                }
            }

            let base = (375 / 1000000);
            let life_style = 1;
            let carbon = 0;

            // classification lookup
            for(let b in lookup){
                if(t_category_class === lookup[b].anzic_code) {
                    base = (lookup[b].emission_factor_gc02 / 1000000);
                    carbon = (t_amount * lookup[b].emission_factor_gc02 / 1000);
                }
            }

            let carbon_offset = (carbon*(365/90)/40000*10);
              
            // greener Home & Energy rating adjustments
            //const amount = await QuizHelper.calculateHomeEnergy(t_amount,t_category_subdivision, quizResponse);

            rating = base * greener * life_style * t_amount;

            let st_date_str = t_date;//.toString().replace('-','/');
            var currentDate = new Date(st_date_str);
            var readableTDate = currentDate.getFullYear()+'-'+("0"+(currentDate.getMonth()+1)).slice(-2)+'-'+("0"+(currentDate.getDate())).slice(-2);

            // dates for past week
            let today_90 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
            let yesterday_90 = new Date(today_90);
            yesterday_90.setDate(yesterday_90.getDate() - 1);
            let last_90 = yesterday_90.getFullYear()+'-'+("0"+(yesterday_90.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_90.getDate())).slice(-2);
            let days_90_90 = new Date(yesterday_90);
            days_90_90.setDate(days_90_90.getDate() - 90);
            let days_90_last_90 = days_90_90.getFullYear()+'-'+("0"+(days_90_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90_90.getDate())).slice(-2);
            let fromDate_90:any = days_90_last_90;
            let toDate_90:any = last_90;

            let today_60 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
            let yesterday_60 = new Date(today_60);
            yesterday_60.setDate(yesterday_60.getDate() - 1);
            let last_60 = yesterday_60.getFullYear()+'-'+("0"+(yesterday_60.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_60.getDate())).slice(-2);
            let days_60_60 = new Date(yesterday_60);
            days_60_60.setDate(days_60_60.getDate() - 60);
            let days_60_last_60 = days_60_60.getFullYear()+'-'+("0"+(days_60_60.getMonth()+1)).slice(-2)+'-'+("0"+(days_60_60.getDate())).slice(-2);
            let fromDate_60:any = days_60_last_60;
            let toDate_60:any = last_60;

            let today_30 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
            let yesterday_30 = new Date(today_30);
            yesterday_30.setDate(yesterday_30.getDate() - 1);
            let last_30 = yesterday_30.getFullYear()+'-'+("0"+(yesterday_30.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_30.getDate())).slice(-2);
            let days_30_30 = new Date(yesterday_30);
            days_30_30.setDate(days_30_30.getDate() - 30);
            let days_30_last_30 = days_30_30.getFullYear()+'-'+("0"+(days_30_30.getMonth()+1)).slice(-2)+'-'+("0"+(days_30_30.getDate())).slice(-2);
            let fromDate_30:any = days_30_last_30;
            let toDate_30:any = last_30;


            let minDate_90 = new Date(fromDate_90);
            let maxDate_90 =  new Date(toDate_90);

            let minDate_60 = new Date(fromDate_60);
            let maxDate_60 =  new Date(toDate_60);

            let minDate_30 = new Date(fromDate_30);
            let maxDate_30 =  new Date(toDate_30);

            let is_90 = false;
            let is_60 = false;
            let is_30 = false;

            if (currentDate > minDate_90 && currentDate < maxDate_90 ){
                console.log(currentDate, "is within 90 days range");
                is_90 = true;
            }
            if (currentDate > minDate_60 && currentDate < maxDate_60 ){
                console.log(currentDate, "is within 60 days range");
                is_60 = true;
            }
            if (currentDate > minDate_30 && currentDate < maxDate_30 ){
                console.log(currentDate, "is within 30 days range");
                is_30 = true;
            }

            let dateUTC = new Date(moment.utc(currentDate));
            let event_name = 'New Transaction';
            let data = {
                'distinct_id': userId,
                '$insert_id': t_basiq_id,
                'first_name': first_name,
                'date': readableTDate,
                'merchantName': t_store_name,
                'Merchant ID': merchant_id,
                'Merchant UX category': sector_name,
                'Merchant ANZSIC class': t_category_class,
                'Merchant ANZSIC group': t_category_group,
                'Merchant Greener Partner': is_partner,
                'Value': t_amount,
                'Carbon': carbon_offset,
                'Last 30 days': is_30,
                'Last 60 days': is_60,
                'Last 90 days': is_90,
                'time': dateUTC,
                'Pre-link status': false,
                'environment': env.ENVIRONMENT
            }
            let events = {
                event: event_name,
                properties: data
            };

            mixpanelEvents.push(events);

        } catch(e) {
            console.log("errr: ", e);
            for(let i in stList) {
                if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase() || stList[i].secondary_names.includes(''+t_store_name.toString().toLowerCase()+'') === true) {
                    greener = 0;
                    break;
                }
            }

            let base = (375 / 1000000);
            let life_style = 1;

            // classification lookup
            for(let b in lookup){
                if(t_category_class === lookup[b].anzic_code) {
                    base = (lookup[b].emission_factor_gc02 / 1000000);
                }
            }

            rating = base * greener * life_style * t_amount;
            is_recommended = true;

            if(rating === 0) {
                is_recommended = false;
            }

            transRejectArray.push({
                'status': transactions[t].status ? transactions[t].status : null,
                'description': transactions[t].description ? transactions[t].description : null,
                'connection': transactions[t].connection ? transactions[t].connection : null,
                'postDate': transactions[t].postDate ? transactions[t].postDate : null,
                'subClass': transactions[t].subClass ? transactions[t].subClass : null,
                'enrich': transactions[t].enrich ? transactions[t].enrich : null
            });

            let event_name = 'Failed transaction';
            let data = {
                'distinct_id': userId,
                'first_name': first_name,
                'data': transactions[t],
                'error_data': e
            }

            MixpanelHelper.track(event_name, data);
        }

        transArray.push({
            'basiq_id': t_basiq_id,
            'transaction_id': t_basiq_id,
            'basiq_name_encrypted': t_basiq_name_encrypted,
            'amount': t_amount,
            'transaction_date': t_date,
            'account_id': t_account,
            'description': t_description,
            'merchant_name': t_store_name,
            'merchant_url': t_website,
            'merchant_route_no': t_merchant_route_no,
            'merchant_route': t_merchant_route,
            'merchant_postal_code': t_merchant_postal_code,
            'merchant_suburb': t_merchant_suburb,
            'merchant_state': t_merchant_state,
            'merchant_country': t_merchant_country,
            'merchant_formatted_address': t_merchant_formatted_address,
            'merchant_geometry_lan': t_merchant_geometry_lan,
            'merchant_geometry_lon': t_merchant_geometry_lon,
            'sub_class': t_sub_class,
            'category_division': t_category_division,
            'category_subdivision': t_category_subdivision,
            'category_group': t_category_group,
            'category_class': t_category_class,
            'institution': t_institution,
            'logo_master': t_logo_master
        });

        ratingArray.push({
            'user_id' : loginName,
            'store_name' : store_name.toString().toLowerCase(),
            'spend' : t_amount,
            'rating' : rating,
            'calculation_date' : t_date,
            'for_recommendation' : is_recommended,
            'category_code' : t_category_class,
            'after_link' : t_after_link,
            'celebrated' : false
        });
      }

      if(transRejectArray.length !== 0){
          let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
          let file_name = new Date()+'.json';

          AWS.config.update({
              accessKeyId: env.AWS_ACCESS_KEY,
              secretAccessKey: env.AWS_SECRET_KEY,
              region: env.AWS_REGION
          });

          let s3 = new AWS.S3();

          // Setting up S3 upload parameters
          let params = {
              Bucket: 'greener-wallet',
              Key: 'transactions/rejected/'+today+'/'+file_name+'',
              Body: JSON.stringify(transRejectArray),
              ContentType: 'application/json',
          };

          // Uploading files to the bucket
          s3.upload(params, async(err, data) => {
              if (err) {
                  console.log(err);
              }

              // UPLOADED
          });
      }

      await getConnection()
          .createQueryBuilder()
          .insert()
          .into(Transaction)
          .values(transArray)
          .onConflict(`("transaction_id") DO NOTHING`)
          .execute();

      await getConnection()
          .createQueryBuilder()
          .insert()
          .into(RatingHistory)
          .values(ratingArray)
          .execute();

      MixpanelHelper.import_batch(mixpanelEvents);

      return {
          'events': mixpanelEvents,
          'total_transaction_from_basiq': transactions.length,
          'transaction_saved_count': transArray.length,
          'transaction_rating_history_saved_count': ratingArray.length,
          'transaction_failed_saved_count': transRejectArray.length,
      }
  }

  static resyncHaloCalculateTransactions = async(transactions, basiq_id, stList, lookup, quizResponse, hasPreviousLinked=true) => {
	    let ratingArray: RatingHistory[] = [];

        for(let t in transactions) {

          let t_store_name = transactions[t].merchant_name;
          let t_amount = transactions[t].amount;
          let t_date = transactions[t].transaction_date;
          let t_actdate = transactions[t].acct_created_at;
          let t_category_class = transactions[t].category_class;
          let store_name = transactions[t].merchant_name;
          let category_subdivision = transactions[t].category_subdivision;
          let greener = 1;
          let rating = 0;
          let is_recommended = true;

          let t_date_raw = new Date(t_date);
          let t_date_conv = t_date_raw.getFullYear()+'-'+("0"+(t_date_raw.getMonth()+1)).slice(-2)+'-'+("0"+(t_date_raw.getDate())).slice(-2);

          let t_actdate_raw = new Date(t_actdate);
          let t_actdate_conv = t_actdate_raw.getFullYear()+'-'+("0"+(t_actdate_raw.getMonth()+1)).slice(-2)+'-'+("0"+(t_actdate_raw.getDate())).slice(-2);

          let t_after_link = (t_date_conv >= t_actdate_conv ? true : false);
          let is_celebrated = (hasPreviousLinked === true ? true : false);

          try {

              // if transaction is post account linking, check if greener store
              if ( t_after_link === true ) {
                for(let i in stList) {
                    if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase() || stList[i].secondary_names !== null && stList[i].secondary_names.includes(''+t_store_name.toString().toLowerCase()+'') === true) {
                        
                        store_name = stList[i].name;
                        greener = 0;    
                        is_recommended = false;
                        break;

                    }
                }
              }

              for(let i in stList) {
                    if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase() || stList[i].secondary_names !== null && stList[i].secondary_names.includes(''+t_store_name.toString().toLowerCase()+'') === true) {
                        
                        store_name = stList[i].name;
                        is_recommended = false;
                        break;

                    }
              }

              let base = (375 / 1000000);
              let life_style = 1;

              // classification lookup
              for(let b in lookup){
                  if(t_category_class === lookup[b].anzic_code) {
                      base = (lookup[b].emission_factor_gc02 / 1000000);
                  }
              }

              // greener Home & Energy rating adjustments
              // const amount = await QuizHelper.calculateHomeEnergy(t_amount,category_subdivision, quizResponse);
              
              rating = base * greener * life_style * t_amount;

          } catch(e) {
            continue;
          }

          let rat = new RatingHistory();
              rat.user_id = basiq_id;
              rat.store_name = store_name.toString().toLowerCase();
              rat.spend = t_amount;
              rat.rating = rating;
              rat.calculation_date = t_date;
              rat.for_recommendation = is_recommended;
              rat.category_code = t_category_class;
              rat.after_link = t_after_link;
              rat.celebrated = is_celebrated;
              ratingArray.push(rat);
      }

      let connection = getConnection();
      await connection.manager.save(ratingArray, { chunk: 10000 });

      return true
  }
  // no selected accounts
  static SyncTransactionInitiator = async(transactions, encrypt_loginName) => {
	let transArray = [];

      for(let t in transactions) {
          let t_basiq_id = transactions[t].id ? transactions[t].id : null;
          let t_basiq_name_encrypted = encrypt_loginName;
          let t_store_name = transactions[t].enrich.merchant.businessName ? transactions[t].enrich.merchant.businessName : '';
          let t_amount = transactions[t].amount ? transactions[t].amount.replace("-", "") : null;
          let t_date = transactions[t].postDate ? transactions[t].postDate : null;
          let t_account = transactions[t].account ? transactions[t].account : null;
          let t_institution = transactions[t].institution ? transactions[t].institution : null;
          let t_description = transactions[t].description ? transactions[t].description : null;
          let t_sub_class = transactions[t].subClass.code ? transactions[t].subClass.code : null;
          let t_website = transactions[t].enrich.merchant.website ? transactions[t].enrich.merchant.website : null;
          let t_merchant_route_no = transactions[t].enrich.location ? transactions[t].enrich.location.routeNo : null;
          let t_merchant_route = transactions[t].enrich.location ? transactions[t].enrich.location.route : null;
          let t_merchant_postal_code = transactions[t].enrich.location ? transactions[t].enrich.location.postalCode : null;
          let t_merchant_suburb = transactions[t].enrich.location ? transactions[t].enrich.location.suburb : null;
          let t_merchant_state = transactions[t].enrich.location ? transactions[t].enrich.location.state : null;
          let t_merchant_country = transactions[t].enrich.location ? transactions[t].enrich.location.country : null;
          let t_merchant_formatted_address = transactions[t].enrich.location ? transactions[t].enrich.location.formattedAddress : null;
          let t_merchant_geometry_lan = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lat : null;
          let t_merchant_geometry_lon = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lng : null;
          let t_category_division = transactions[t].enrich.category.anzsic.division.code ? transactions[t].enrich.category.anzsic.division.code : null;
          let t_category_subdivision = transactions[t].enrich.category.anzsic.subdivision.code ? transactions[t].enrich.category.anzsic.subdivision.code : null;
          let t_category_group = transactions[t].enrich.category.anzsic.group.code ? transactions[t].enrich.category.anzsic.group.code : null;
          let t_category_class = transactions[t].enrich.category.anzsic.class.code ? transactions[t].enrich.category.anzsic.class.code : null;

          try {
              transArray.push({
                  'basiq_id': t_basiq_id,
                  'transaction_id': t_basiq_id,
                  'basiq_name_encrypted': t_basiq_name_encrypted,
                  'amount': t_amount,
                  'transaction_date': t_date,
                  'account_id': t_account,
                  'description': t_description,
                  'merchant_name': t_store_name,
                  'merchant_url': t_website,
                  'merchant_route_no': t_merchant_route_no,
                  'merchant_route': t_merchant_route,
                  'merchant_postal_code': t_merchant_postal_code,
                  'merchant_suburb': t_merchant_suburb,
                  'merchant_state': t_merchant_state,
                  'merchant_country': t_merchant_country,
                  'merchant_formatted_address': t_merchant_formatted_address,
                  'merchant_geometry_lan': t_merchant_geometry_lan,
                  'merchant_geometry_lon': t_merchant_geometry_lon,
                  'sub_class': t_sub_class,
                  'category_division': t_category_division,
                  'category_subdivision': t_category_subdivision,
                  'category_group': t_category_group,
                  'category_class': t_category_class,
                  'institution': t_institution
              });
          } catch(e) {
              continue;
          }
      }

      await getConnection()
          .createQueryBuilder()
          .insert()
          .into(Transaction)
          .values(transArray)
          .onConflict(`("transaction_id") DO NOTHING`)
          .execute();

      return {
          'total_transaction_from_basiq': transactions.length,
          'transaction_saved_count': transArray.length
      }
  }

  // having selected accounts
  static SyncInitialTransactionV2 = async(transactions, encrypt_loginName, lookup, loginName, stList, userId, first_name) => {
    let transArray = [];
    let mixpanelEvents = [];

    for(let t in transactions) {

      let t_basiq_id = transactions[t].id ? transactions[t].id : null;
      let t_basiq_name_encrypted = encrypt_loginName;
      let t_store_name = transactions[t].enrich.merchant.businessName ? transactions[t].enrich.merchant.businessName : '';
      let t_amount = transactions[t].amount ? transactions[t].amount.replace("-", "") : null;
      let t_date = transactions[t].postDate ? transactions[t].postDate : null;
      let t_account = transactions[t].account ? transactions[t].account : null;
      let t_institution = transactions[t].institution ? transactions[t].institution : null;
      let t_description = transactions[t].description ? transactions[t].description : null;
      let t_sub_class = transactions[t].subClass.code ? transactions[t].subClass.code : null;
      let t_website = transactions[t].enrich.merchant.website ? transactions[t].enrich.merchant.website : null;
      let t_merchant_route_no = transactions[t].enrich.location ? transactions[t].enrich.location.routeNo : null;
      let t_merchant_route = transactions[t].enrich.location ? transactions[t].enrich.location.route : null;
      let t_merchant_postal_code = transactions[t].enrich.location ? transactions[t].enrich.location.postalCode : null;
      let t_merchant_suburb = transactions[t].enrich.location ? transactions[t].enrich.location.suburb : null;
      let t_merchant_state = transactions[t].enrich.location ? transactions[t].enrich.location.state : null;
      let t_merchant_country = transactions[t].enrich.location ? transactions[t].enrich.location.country : null;
      let t_merchant_formatted_address = transactions[t].enrich.location ? transactions[t].enrich.location.formattedAddress : null;
      let t_merchant_geometry_lan = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lat : null;
      let t_merchant_geometry_lon = transactions[t].enrich.location ? transactions[t].enrich.location.geometry.lng : null;
      let t_category_division = transactions[t].enrich.category.anzsic.division.code ? transactions[t].enrich.category.anzsic.division.code : null;
      let t_category_subdivision = transactions[t].enrich.category.anzsic.subdivision.code ? transactions[t].enrich.category.anzsic.subdivision.code : null;
      let t_category_group = transactions[t].enrich.category.anzsic.group.code ? transactions[t].enrich.category.anzsic.group.code : null;
      let t_category_class = transactions[t].enrich.category.anzsic.class.code ? transactions[t].enrich.category.anzsic.class.code : null;
      let t_logo_master = transactions[t].enrich.links ? transactions[t].enrich.links['logo-master'] : null;
      let is_partner = false;

      try {
        let secRepo = getRepository(Sector);
        let nGRepo = getRepository(StoreNonGreener);
        let sector_name:any = false;
        let merchant_id:any = false;

        for(let i in stList) {
            if(stList[i].name.toString().toLowerCase() === t_store_name.toString().toLowerCase() || stList[i].secondary_names !== null && stList[i].secondary_names.includes(''+t_store_name.toString().toLowerCase()+'') === true) {
                let sector = await secRepo.findOne(stList[i].sectorId);
                sector_name = sector.name;
                merchant_id = stList[i].id;
                is_partner = true;
                break;
            }
        }

        if(sector_name === false) {
            // classification lookup
            for(let b in lookup){
                if(t_category_class === lookup[b].anzic_code) {
                    let sector = await secRepo.findOne(lookup[b].sector_id);
                    sector_name = sector.name;

                    let ngSt = await nGRepo.findOne({ where: {name: t_store_name}});

                    if(ngSt) {
                        merchant_id = ngSt.id;
                    }
                }
            }
        }

        let carbon = 0;

        // classification lookup
        for (let b in lookup) {
          if (t_category_class === lookup[b].anzic_code) {
            carbon = (t_amount * lookup[b].emission_factor_gc02 / 1000);
            break;
          }
        }

        let carbon_offset = (carbon*(365/90)/40000*10);

        let st_date_str = t_date;
        var currentDate = new Date(st_date_str);
        var readableTDate = currentDate.getFullYear()+'-'+("0"+(currentDate.getMonth()+1)).slice(-2)+'-'+("0"+(currentDate.getDate())).slice(-2);

        // dates for past week
        let today_90 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday_90 = new Date(today_90);
        yesterday_90.setDate(yesterday_90.getDate() - 1);
        let last_90 = yesterday_90.getFullYear()+'-'+("0"+(yesterday_90.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_90.getDate())).slice(-2);
        let days_90_90 = new Date(yesterday_90);
        days_90_90.setDate(days_90_90.getDate() - 90);
        let days_90_last_90 = days_90_90.getFullYear()+'-'+("0"+(days_90_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90_90.getDate())).slice(-2);
        let fromDate_90:any = days_90_last_90;
        let toDate_90:any = last_90;

        let today_60 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday_60 = new Date(today_60);
        yesterday_60.setDate(yesterday_60.getDate() - 1);
        let last_60 = yesterday_60.getFullYear()+'-'+("0"+(yesterday_60.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_60.getDate())).slice(-2);
        let days_60_60 = new Date(yesterday_60);
        days_60_60.setDate(days_60_60.getDate() - 60);
        let days_60_last_60 = days_60_60.getFullYear()+'-'+("0"+(days_60_60.getMonth()+1)).slice(-2)+'-'+("0"+(days_60_60.getDate())).slice(-2);
        let fromDate_60:any = days_60_last_60;
        let toDate_60:any = last_60;

        let today_30 = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday_30 = new Date(today_30);
        yesterday_30.setDate(yesterday_30.getDate() - 1);
        let last_30 = yesterday_30.getFullYear()+'-'+("0"+(yesterday_30.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday_30.getDate())).slice(-2);
        let days_30_30 = new Date(yesterday_30);
        days_30_30.setDate(days_30_30.getDate() - 30);
        let days_30_last_30 = days_30_30.getFullYear()+'-'+("0"+(days_30_30.getMonth()+1)).slice(-2)+'-'+("0"+(days_30_30.getDate())).slice(-2);
        let fromDate_30:any = days_30_last_30;
        let toDate_30:any = last_30;

        let minDate_90 = new Date(fromDate_90);
        let maxDate_90 =  new Date(toDate_90);

        let minDate_60 = new Date(fromDate_60);
        let maxDate_60 =  new Date(toDate_60);

        let minDate_30 = new Date(fromDate_30);
        let maxDate_30 =  new Date(toDate_30);

        let is_90 = false;
        let is_60 = false;
        let is_30 = false;

        if (currentDate > minDate_90 && currentDate < maxDate_90) {
            is_90 = true;
        } else if (currentDate > minDate_60 && currentDate < maxDate_60) {
            is_60 = true;
        } else if (currentDate > minDate_30 && currentDate < maxDate_30) {
            is_30 = true;
        }

        let dateUTC = new Date(moment.utc(currentDate));
        let event_name = 'Old Transaction';
        let data = {
            'distinct_id': userId,
            '$insert_id': t_basiq_id,
            'first_name': first_name,
            'date': readableTDate,
            'merchantName': t_store_name,
            'Merchant ID': merchant_id,
            'Merchant UX category': sector_name,
            'Merchant ANZSIC class': t_category_class,
            'Merchant ANZSIC group': t_category_group,
            'Merchant Greener Partner': is_partner,
            'Value': t_amount,
            'Carbon': carbon_offset,
            'Last 30 days': is_30,
            'Last 60 days': is_60,
            'Last 90 days': is_90,
            'time': dateUTC,
            'Pre-link status': true,
            'environment': env.ENVIRONMENT
        }
        let events = {
            event: event_name,
            properties: data
        };

        mixpanelEvents.push(events);

      } catch(e) {
        console.log(e);
      }

      transArray.push({
        'basiq_id': t_basiq_id,
        'transaction_id': t_basiq_id,
        'basiq_name_encrypted': t_basiq_name_encrypted,
        'amount': t_amount,
        'transaction_date': t_date,
        'account_id': t_account,
        'description': t_description,
        'merchant_name': t_store_name,
        'merchant_url': t_website,
        'merchant_route_no': t_merchant_route_no,
        'merchant_route': t_merchant_route,
        'merchant_postal_code': t_merchant_postal_code,
        'merchant_suburb': t_merchant_suburb,
        'merchant_state': t_merchant_state,
        'merchant_country': t_merchant_country,
        'merchant_formatted_address': t_merchant_formatted_address,
        'merchant_geometry_lan': t_merchant_geometry_lan,
        'merchant_geometry_lon': t_merchant_geometry_lon,
        'sub_class': t_sub_class,
        'category_division': t_category_division,
        'category_subdivision': t_category_subdivision,
        'category_group': t_category_group,
        'category_class': t_category_class,
        'institution': t_institution,
        'logo_master': t_logo_master
      });
    }

    await getConnection()
        .createQueryBuilder()
        .insert()
        .into(Transaction)
        .values(transArray)
        .onConflict(`("transaction_id") DO NOTHING`)
        .execute();

    MixpanelHelper.import_batch(mixpanelEvents);

    return {
        'events': mixpanelEvents,
        'total_transaction_from_basiq': transactions.length,
        'transaction_saved_count': transArray.length
    }

  }

}
