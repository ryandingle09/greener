import { getConnection, getManager, getRepository } from "typeorm";
import { Classification } from "../models/Classification";
import { User } from "../models/User";
import * as jwt from "jsonwebtoken";

const env = process.env;

interface TransactionDateRange {
    is_90: boolean;
    is_60: boolean;
    is_30: boolean;
}

export class Helper{

    static getIDfromToken = async(request, response) => {
        let Jwttoken = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(Jwttoken, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        const { userId } = jwtPayload;

        return userId;
    }

    static getStoreLookUps = async() => {
        const lookup = await getRepository(Classification).createQueryBuilder("lookup").getMany();
        return lookup;
    }

    static getUserData = async(userId) => {
        const user = await getRepository(User).findOne(userId);
        return user;
    }

    static getDateRange = (lookback: number) => {
        let today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+(new Date().getDate())).slice(-2);
        let yesterday = new Date(today)
        yesterday.setDate(yesterday.getDate() - 1);

        let last = yesterday.getFullYear()+'-'+("0"+(yesterday.getMonth()+1)).slice(-2)+'-'+("0"+(yesterday.getDate())).slice(-2);
        let days_90 = new Date(yesterday);
        days_90.setDate(yesterday.getDate() - lookback);

        let days_90_last = days_90.getFullYear()+'-'+("0"+(days_90.getMonth()+1)).slice(-2)+'-'+("0"+(days_90.getDate())).slice(-2);
        let fromDate:any = days_90_last;
        let toDate:any = last;

        return {'fromDate': fromDate,'toDate': toDate}
    }

    static formatDate(date: Date): string{
        const year = date.getFullYear();
        const month = ("0"+(date.getMonth()+1)).slice(-2);
        const day = ("0" + date.getDate()).slice(-2);

        return `${year}-${month}-${day}`;
    }

    static getTransactionDateRange(postDateStr: string): TransactionDateRange {
        const transactionPostDate = new Date(postDateStr);

        const today = new Date(Helper.formatDate(new Date()));
        const yesterday = new Date(today.setDate(today.getDate()-1));

        const fromDate_90 = new Date(yesterday.setDate(yesterday.getDate()- 90));
        const toDate_90 = yesterday;

        const fromDate_60 = new Date(yesterday.setDate(yesterday.getDate()- 60));
        const toDate_60 = yesterday;

        const fromDate_30 = new Date(yesterday.setDate(yesterday.getDate()- 30));
        const toDate_30 = yesterday;

        return {
            is_90 : transactionPostDate > fromDate_90 && transactionPostDate < toDate_90 ? true : false,
            is_60 : transactionPostDate > fromDate_60 && transactionPostDate < toDate_60 ? true : false,
            is_30 : transactionPostDate > fromDate_30 && transactionPostDate < toDate_30 ? true : false
        }
    }

}