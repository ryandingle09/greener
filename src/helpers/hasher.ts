import SimpleCrypto from "simple-crypto-js";
const env = process.env;

export class HashHelper{

    static encrypt = async(text) => {
		let simpleCrypto = new SimpleCrypto(env.SaltSecretKey);
		let cipherText = simpleCrypto.encrypt(text);

		return cipherText;
    }

    static decrypt = async(textHash) => {
		let simpleCrypto = new SimpleCrypto(env.SaltSecretKey);
		let decipherText = simpleCrypto.decrypt(textHash);

		return decipherText;
    }

}