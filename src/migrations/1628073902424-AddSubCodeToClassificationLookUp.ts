import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { Classification } from "../models/Classification";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const classData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/classification.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class AddSubCodeToClassificationLookUp1628073902424 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(Classification);
        let sub = await data
            .createQueryBuilder("cl")
            .getMany();

        for(let i in sub) {
            classData.push(sub[i]);
        }

        console.log(classData);
        
        let queryStr = ` `;
        let sets = [];
        console.log('here');
        for(let i in results) {
            console.log('here');
            for(let b in classData) {
                if(results[i].anzic_code.toString() === classData[b].anzic_code.toString()) {
                    sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.classifications SET sub_code=${(results[i].sub_code !== '') ? results[i].sub_code : null }, emission_factor_tc02=${results[i].emissions_tc02} WHERE anzic_code='${classData[b].anzic_code}' `);
                }
            }
        }
        console.log('here');
        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        return await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
