import {MigrationInterface, QueryRunner, getConnection, getRepository } from "typeorm";
import { Banner } from "../models/Banner";
import { BannerItem } from "../models/BannerItem";
import { Store } from "../models/Store";
import { GoogleHelper } from '../helpers/google';

const results = [];
const bannerData = [];
const storesData = [];
const env = process.env;

export class StoreBannerFromGoogleMaps1623051916673 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data2 = getRepository(Store);
        let stores = await data2
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let queryStr = ` `;
        let sets = [];

        let count = 0;

        const storeBanners = {
            'bupa': 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/bupa-hero-image-banner.png',
            'huddle insurance': 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/Huddle+Hero.jpg',
            'who gives a crap': 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/Who+Gives+A+Crap+Banner.png',
            'atiyah': 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/atiyah-banner.jpg',
            "single use ain't sexy": 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/Single+Use+Aint+Sexy+banner.jpg',
            'zero co': 'https://greener-wallet.s3.ap-southeast-2.amazonaws.com/images/Zero+Co+Banner.png'
        }
            
        for(let a in storesData) {
            count += 1;

            // if(count === 5) break; use for test

            let storeId = storesData[a].id;

            let api = new GoogleHelper();
            let text_search = ''+storesData[a].name+' in Australia AU, AUS';
            let ref = await api.getPlacePhotoReference(text_search);
            let logo = null;
            let ref_photo;
            let ref_width;
            let ref_height;

            console.log("ref: ", ref);

            try {
                ref_photo = ref.candidates[0].photos[0].photo_reference;
                ref_width = ref.candidates[0].photos[0].width;
                ref_height = ref.candidates[0].photos[0].height;

                logo = await api.getPlacePhoto(ref_width, ref_height, ref_photo);

                console.log('logo: ',logo);
            } catch (e){
                console.log('no results getPlacePhotoReference, looking for wide search');
                let api = new GoogleHelper();
                let result = await api.getStoreAddresses(text_search);
                let addresses =  result.results;
                console.log('first get '+storesData[a].name+': ', addresses.length);
                let next = result.next_page_token;
                let status = result.status;
                console.log('has next on first get '+next);

                while (next !== undefined){
                    let result_next = await api.getStoreAddressesNext(text_search, next);
                    let nextLink = result_next.next_page_token;
                    status = result_next.status;
                    addresses = addresses.concat(result_next.results);
                    console.log('2nd get '+storesData[a].name+': ', result_next.results.length);
                    console.log('has next on 2nd get '+nextLink);

                    if(result_next.results.length !== 0) {
                        console.log("count on while :", result_next.results.length);
                        next = nextLink;
                    }

                    if(status === 'ZERO_RESULTS') {
                        console.log("ZERO_RESULTS");
                        next = nextLink;
                    }
                }

                for(let a in addresses){
                    try {
                        ref_photo = addresses[a].photos[0].photo_reference;
                        ref_width = addresses[a].photos[0].width;
                        ref_height = addresses[a].photos[0].height;

                        logo = await api.getPlacePhoto(ref_width, ref_height, ref_photo);

                        console.log('logo: ',logo);
                        break;
                    } catch (e){
                        console.log('No photo reference');
                    }
                }

                console.log("Pass through");
            }
          
            const storeName = storesData[a].name.toString().toLowerCase();
            const storeBanner = storeBanners[storeName] || logo;

            sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET "banner_image"='${storeBanner}' WHERE id='${storeId}' `);
        }

        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        return await queryRunner.query(finalStr.toString());

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
