import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { SubSector } from "../models/SubSector";
import { Sector } from "../models/Sector";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class CreateSubSector1622035211052 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let sectorsData = [];
        let data = getRepository(Sector);
        let secs = await data
            .createQueryBuilder("a")
            .orderBy("a.id", "ASC")
            .getMany();

        for(let i in secs) {
            sectorsData.push(secs[i]);
        }

        fs.createReadStream('src/migrations/csv/sub_sector.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let seed = [];

            for(let i in results) {
                let id;

                for(let a in sectorsData) {
                    if(results[i].Sector.toLowerCase() === sectorsData[a].name.toLowerCase()) {
                        id = sectorsData[a].id;
                    }

                    console.log(results[i].Sector.toLowerCase(), "-", sectorsData[a].name.toLowerCase());
                }
                console.log(id);

                seed.push({
                    'sectorId': id,
                    'name': results[i].BusinessSectorName,
                    'sub_code': results[i].sub_code,
                    'description': results[i].BusinessSectorDescription
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(SubSector)
            .values(seed)
            .onConflict(`("sub_code") DO NOTHING`)
            .execute();

            console.log(seed);
        });
     }

     public async down(queryRunner: QueryRunner): Promise<void> {
     }

}
