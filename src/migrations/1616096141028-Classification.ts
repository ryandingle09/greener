import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { Sector } from "../models/Sector";
import { Classification } from "../models/Classification";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const sectorsData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/classification.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class Classification1616096141028 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(Sector);
        let stores = await data
            .createQueryBuilder("a")
            .orderBy("a.id", "ASC")
            .getMany();

        for(let i in stores) {
            sectorsData.push(stores[i]);
        }

        let seed = [];

        for(let i in results) {
            let id;

            for(let a in sectorsData) {
                if(results[i].category.toLowerCase() === sectorsData[a].name.toLowerCase()) {
                    id = sectorsData[a].id;
                }
            }
            
            seed.push({
                'sector_id': id,
                'anzic_code': results[i].anzic_code,
                'anzic_description': results[i].description,
                'emission_factor_gc02': results[i].emissions_gc02
            });

            console.log(seed);
        }

        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(Classification)
        .values(seed)
        .execute();

        console.log("seed data: " + JSON.stringify(seed));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
