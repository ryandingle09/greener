import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { Credential } from "../models/Credential";
import { Accreditation } from "../models/Accreditation";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const credData = [];
const accData = [];
const env = process.env;

export class CreateCredentialAccreditation1602474815982 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let acc = getRepository(Accreditation);
        let accreditations = await acc
            .createQueryBuilder("accreditation")
            .orderBy("accreditation.id", "ASC")
            .getMany();

        for(let i in accreditations) {
           accData.push(accreditations[i]);
        }

        let cred = getRepository(Credential);
        let credentials = await cred
            .createQueryBuilder("credential")
            .orderBy("credential.id", "ASC")
            .getMany();

        for(let i in credentials) {
           credData.push(credentials[i]);
        }

        fs.createReadStream('src/migrations/csv/credential_accreditation.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let queryStr = ` `;
            let sets = [];

            for(let i in results) {
                let acc_id;
                let cred_id;

                for(let a in credData) {
                    if(results[i].GreenLensName.toLowerCase() === credData[a].name.toLowerCase()) {
                        cred_id = credData[a].id;
                        let name = ''+i+' cred: '+results[i].GreenLensName.toLowerCase()+' : '+credData[a].name.toLowerCase()+'';
                        console.log(name);
                    }
                }

                for(let a in accData) {
                    if(results[i].AccreditationName.toLowerCase() === accData[a].name.toLowerCase()) {
                        acc_id = accData[a].id;
                        let name = ''+i+' accr: '+results[i].AccreditationName.toLowerCase()+' : '+accData[a].name.toLowerCase()+'';
                        console.log(name);
                    }
                }

                if(acc_id === undefined) continue;
                console.log(acc_id, " - ", results[i].AccreditationName.toLowerCase());

                sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.accreditations SET "credential_id"='${cred_id}' WHERE id::uuid='${acc_id}'`);
            } 

            let sets2 = sets.join(';');
            let finalStr = queryStr+sets2;

            await queryRunner.query(finalStr.toString());
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}