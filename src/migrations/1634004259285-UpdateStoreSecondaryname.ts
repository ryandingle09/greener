import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { Store } from "../models/Store";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class UpdateStoreSecondaryname1634004259285 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let storeRep = getRepository(Store);
        
        for(let i in results) {
            if(results[i].secondary_names != undefined || results[i].secondary_names != null || results[i].secondary_names != '') {
                let data = await storeRep.findOne({where: {'name': results[i].GreenBusinessName}});
                data.secondary_names = results[i].secondary_names.split(",");
                await storeRep.save(data);
            }
        }

        return
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
