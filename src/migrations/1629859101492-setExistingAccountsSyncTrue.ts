import {MigrationInterface, QueryRunner} from "typeorm";

const env = process.env;

export class setExistingAccountsSyncTrue1629859101492 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let finalStr = `UPDATE ${env.TYPEORM_SCHEMA}.accounts SET account_synced=TRUE;`;
        await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
