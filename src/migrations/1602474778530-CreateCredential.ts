import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { Credential } from "../models/Credential";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class CreateCredential1602474778530 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        fs.createReadStream('src/migrations/csv/credentials.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let seed = [];

            for(let i in results) {
                seed.push({
                    'name': results[i].GreenCredName,
                    'description': results[i].GreenCredDescription
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Credential)
            .values(seed)
            .onConflict(`("name") DO NOTHING`)
            .execute();

            console.log(seed);
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
