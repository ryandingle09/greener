import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { QuizCategory } from "../models/QuizCategory";
import { QuizQuestion } from "../models/QuizQuestion";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const categories = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/quiz_question.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class QuizQuestions1623259503409 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(QuizCategory);
        let category = await data
            .createQueryBuilder("category")
            .orderBy("category.id", "ASC")
            .getMany();

        for(let i in category) {
            categories.push(category[i]);
        }

        let seed = [];

        for(let i in results) {
            let catId;

            for(let b in category) {
                if(results[i].category.toLowerCase() === category[b].name.toLowerCase()) {
                    catId = category[b].id;
                }
            }

            seed.push({
                'quizcategoryId': catId,
                'title': results[i].question,
                'code': results[i].code,
                'icon': results[i].icon,
                'type': results[i].type,
                'has_minimum': parseInt(results[i].has_minimum) ? results[i].has_minimum : null,
                'control': parseInt(results[i].control) ? results[i].control : null,
                'show_badge_if_value': parseInt(results[i].show_badge_if_value) ? results[i].show_badge_if_value : null
            });
        }

        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(QuizQuestion)
        .values(seed)
        .execute();

        console.log(seed);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
