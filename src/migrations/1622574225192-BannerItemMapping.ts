import {MigrationInterface, QueryRunner, getConnection, getRepository } from "typeorm";
import { Banner } from "../models/Banner";
import { BannerItem } from "../models/BannerItem";
import { Store } from "../models/Store";
import { GoogleHelper } from '../helpers/google';

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const bannerData = [];
const storesData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/banner.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class BannerItemMapping1622574225192 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(Banner);
        let banner = await data
            .createQueryBuilder("banner")
            .orderBy("banner.id", "ASC")
            .getMany();

        for(let i in banner) {
            bannerData.push(banner[i]);
        }

        let data2 = getRepository(Store);
        let stores = await data2
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let sets = [];

        for(let i in results) {
            let bannerId;
            let subs = results[i].stores.split(",");

            for(let b in bannerData) {
                if(results[i].title.toLowerCase() === bannerData[b].title.toLowerCase()) {
                    bannerId = bannerData[b].id;
                }
            }

            for(let ss in subs) {
                console.log("store:", subs[ss]);
                
                for(let a in storesData) {
                    let storeId = storesData[a].id;

                    if(subs[ss].toString().toLowerCase() === storesData[a].name.toLowerCase()) {
                        console.log('Merchant found: ', subs[ss].toString().toLowerCase());

                        let api = new GoogleHelper();
                        let text_search = ''+storesData[a].name+' in Australia AU, AUS';
                        let ref = await api.getPlacePhotoReference(text_search);
                        let logo = null;
                        let ref_photo;
                        let ref_width;
                        let ref_height;

                        console.log("ref: ", ref);

                        try {
                            ref_photo = ref.candidates[0].photos[0].photo_reference;
                            ref_width = ref.candidates[0].photos[0].width;
                            ref_height = ref.candidates[0].photos[0].height;

                            logo = await api.getPlacePhoto(ref_width, ref_height, ref_photo);

                            console.log('logo: ',logo);
                        } catch (e){
                            console.log('no results getPlacePhotoReference, looking for wide search');
                            let api = new GoogleHelper();
                            let result = await api.getStoreAddresses(text_search);
                            let addresses =  result.results;
                            console.log('first get '+storesData[a].name+': ', addresses.length);
                            let next = result.next_page_token;
                            let status = result.status;
                            console.log('has next on first get '+next);

                            while (next !== undefined){
                                let result_next = await api.getStoreAddressesNext(text_search, next);
                                let nextLink = result_next.next_page_token;
                                status = result_next.status;
                                addresses = addresses.concat(result_next.results);
                                console.log('2nd get '+storesData[a].name+': ', result_next.results.length);
                                console.log('has next on 2nd get '+nextLink);

                                if(result_next.results.length !== 0) {
                                    console.log("count on while :", result_next.results.length);
                                    next = nextLink;
                                }

                                if(status === 'ZERO_RESULTS') {
                                    console.log("ZERO_RESULTS");
                                    next = nextLink;
                                }
                            }

                            for(let a in addresses){
                                try {
                                    ref_photo = addresses[a].photos[0].photo_reference;
                                    ref_width = addresses[a].photos[0].width;
                                    ref_height = addresses[a].photos[0].height;
    
                                    logo = await api.getPlacePhoto(ref_width, ref_height, ref_photo);
    
                                    console.log('logo: ',logo);
                                    break;
                                } catch (e){
                                    console.log('No photo reference');
                                }
                            }

                            console.log("Pass through");
                        }
                        
                        sets.push({
                            'bannerId': bannerId,
                            'storeId': storeId,
                            'logo': logo
                        });
                    }
                }
            }
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(BannerItem)
            .values(sets)
            .execute();

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
