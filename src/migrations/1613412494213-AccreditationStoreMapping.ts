import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import { Store } from "../models/Store";
import { Accreditation } from "../models/Accreditation";
import { StoreAccreditation } from "../models/StoreAccreditation";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store_accreditation.csv')
    .pipe(csv())
    .on('data', (data) => results.push(data))
    .on('end', async() => {
});

export class AccreditationStoreMapping1613412494213 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const storesData = [];
        const accreditationsData = [];

        let data = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let data2 = getRepository(Accreditation);
        let accreditations = await data2
            .createQueryBuilder("accreditation")
            .orderBy("accreditation.id", "ASC")
            .getMany();

        for(let i in accreditations) {
            accreditationsData.push(accreditations[i]);
        }

        let sets = [];

        for(let i in results) {

            let accreditationId;
            for(let a in accreditationsData) {
                if(results[i].Accreditation.toLowerCase() === accreditationsData[a].name.toLowerCase()) {
                    accreditationId = accreditationsData[a].id;
                    let storeId;

                    for(let b in storesData) {
                        console.log("store: " +JSON.stringify(results[i].StoreName));
                        if(results[i].StoreName.toLowerCase() === storesData[b].name.toLowerCase()) {
                            storeId = storesData[b].id;
                            sets.push({
                                'accreditationId': accreditationId,
                                'storeId': storeId
                            });
                        }
                    }
                }
            }
        }
        
        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreAccreditation)
            .values(sets)
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
