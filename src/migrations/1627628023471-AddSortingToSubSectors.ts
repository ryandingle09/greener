import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { SubSector } from "../models/SubSector";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const subsectorData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/sub_sector.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class AddSortingToSubSectors1627628023471 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(SubSector);
        let sub = await data
            .createQueryBuilder("sub")
            .leftJoinAndSelect("sub.sector", "sec")
            .orderBy("sub.id", "ASC")
            .getMany();

        for(let i in sub) {
            subsectorData.push(sub[i]);
        }

        console.log(subsectorData);
        
        let queryStr = ` `;
        let sets = [];

        for(let i in results) {
            for(let b in subsectorData) {
                if(results[i].BusinessSectorName.toLowerCase() === subsectorData[b].name.toLowerCase() && results[i].Sector.toLowerCase() === subsectorData[b].sector.name.toLowerCase() ) {
                    let subId = subsectorData[b].id;
                    sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.sub_sectors SET sort_order=${results[i].sort_order} WHERE id='${subId}' `);
                }
            }
        }
        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        return await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
