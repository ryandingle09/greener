import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import { Store } from "../models/Store";

const csv = require('csv-parser');
const fs = require('fs');
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class UpdateStoreImages1613422396642 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        
        let datas = [];
        let data = getRepository(Store);
        let storeRep = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            datas.push(stores[i]);
        }

        // let queryStr = ` `;
        // let sets = [];

        for(let b in datas){
            for(let i in results) {
                if(datas[b].name.toString().toLowerCase() === results[i].GreenBusinessName.toString().toLowerCase()){
                    if(results[i].Logo === undefined || results[i].Logo === null) continue;

                    let data = await storeRep.findOneOrFail(datas[b].id);
                    data.logo = `https://greener-wallet.s3-ap-southeast-2.amazonaws.com/images/${results[i].Logo}`;
                    await storeRep.save(data);

                    // sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET logo="https://greener-wallet.s3-ap-southeast-2.amazonaws.com/images/`+results[i].Logo+`" WHERE id='`+datas[b].id+`'`);
                }
            }
        }
        
        // let sets2 = sets.join(';');
        // let finalStr = queryStr+sets2;

        // await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
