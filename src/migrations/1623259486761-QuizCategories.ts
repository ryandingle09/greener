import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { QuizCategory } from "../models/QuizCategory";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class QuizCategories1623259486761 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        fs.createReadStream('src/migrations/csv/quiz_category.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            console.log(results);
            let seed = [];

            for(let i in results) {
                seed.push({
                    'sort_order': parseInt(results[i].sort_order),
                    'name': results[i].name,
                    'description': results[i].description,
                    'has_computation': results[i].has_computation
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(QuizCategory)
            .values(seed)
            .execute();

            console.log(seed);
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
