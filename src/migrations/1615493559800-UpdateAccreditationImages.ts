import {MigrationInterface, getRepository, QueryRunner, getConnection} from "typeorm";
import { Accreditation } from "../models/Accreditation";

const csv = require('csv-parser');
const fs = require('fs');
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/accreditation.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class UpdateAccreditationImages1615493559800 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        console.log(results);
        
        let data = getRepository(Accreditation);
        let datas = [];
        let accs = await data
            .createQueryBuilder("acc")
            .orderBy("acc.id", "ASC")
            .getMany();

        for(let i in accs) {
            datas.push(accs[i]);
        }

        let queryStr = ` `;
        let sets = [];

        for(let b in datas){
            for(let i in results) {
                if(datas[b].name.toString().toLowerCase() === results[i].AccreditationName.toString().toLowerCase()){
                    sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.accreditations SET "logo"='https://greener-wallet.s3-ap-southeast-2.amazonaws.com/images/accreditation/${results[i].AccreditationLogo}' WHERE id='${datas[b].id}'`);
                }
            }
        }
        
        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
