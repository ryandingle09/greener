import {MigrationInterface, QueryRunner, getConnection, getRepository} from "typeorm";
import { QuizQuestion } from "../models/QuizQuestion";
import { QuizQuestionChoice } from "../models/QuizQuestionChoice";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const questions = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/quiz_question_choice.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class QuizQuestionChoices1623687718967 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(QuizQuestion);
        let question = await data
            .createQueryBuilder("question")
            .orderBy("question.id", "ASC")
            .getMany();

        for(let i in question) {
            questions.push(question[i]);
        }

        let seed = [];

        for(let i in results) {
            let Id;

            for(let b in question) {
                if(results[i].question.toLowerCase() === question[b].title.toLowerCase()) {
                    Id = question[b].id;
                }
            }

            if(results[i].question !== '' || results[i].question !== null) {
                let cText = results[i].title.toString().split(',');
                let cIcon = results[i].icon.toString().split(',');
                let cValue = results[i].value.toString().split(',');
                let cBadge = results[i].badge.toString().split(',');
                for(let c in cText) {
                    seed.push({
                        'quizquestionId': Id,
                        'title': cText[c],
                        'icon': cIcon[c],
                        'value': cValue[c],
                        'badge': cBadge[c]
                    });
                }
            }
        }

        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(QuizQuestionChoice)
        .values(seed)
        .execute();

        console.log(seed);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
