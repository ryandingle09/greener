import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { Sector } from "../models/Sector";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class CreateSector1602474824741 implements MigrationInterface {

     public async up(queryRunner: QueryRunner): Promise<void> {
        fs.createReadStream('src/migrations/csv/sector.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let seed = [];

            for(let i in results) {
                let order = parseInt(results[i].order);

                if(isNaN(order)) {
                    order = null;
                }

                seed.push({
                    'name': results[i].BusinessSectorName,
                    'description': results[i].BusinessSectorDescription,
                    'sort_order': order
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Sector)
            .values(seed)
            .onConflict(`("name") DO NOTHING`)
            .execute();

            console.log(seed);
        });
     }

     public async down(queryRunner: QueryRunner): Promise<void> {
     }

}
