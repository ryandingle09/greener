import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { Accreditation } from "../models/Accreditation";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/accreditation.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class CreateAccreditation1602474800226 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let seed = [];

        for(let i in results) {
            seed.push({
                'name': results[i].AccreditationName,
                'description': results[i].AccreditationDesc,
                'logo': results[i].AccreditationLogo
            });
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Accreditation)
            .values(seed)
            .onConflict(`("name") DO NOTHING`)
            .execute();

        console.log(seed);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
