import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { Store } from "../models/Store";
import { Sector } from "../models/Sector";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const storesData = [];
const sectorsData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store_sector_mapping.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});

export class CategoryMapping1607628301643 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let datas = getRepository(Sector);
        let sectors = await datas
            .createQueryBuilder("sectors")
            .orderBy("sectors.id", "ASC")
            .getMany();

        for(let i in sectors) {
            sectorsData.push(sectors[i]);
        }
        let queryStr = ` `;
        let sets = [];

        for(let i in results) {

            let sectoreId;

            for(let a in sectorsData) {
                if(results[i].Category.toLowerCase() === sectorsData[a].name.toLowerCase()) {
                    sectoreId = sectorsData[a].id;
                    let storeId;

                    for(let b in storesData) {
                        if(results[i].Store.toLowerCase() === storesData[b].name.toLowerCase()) {
                            storeId = storesData[b].id;
                            sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET "sectorId"='${sectoreId}' WHERE id='${storeId}' `);
                        }
                    }
                }
            }
        }
        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        return await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
