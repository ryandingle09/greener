import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import { Store } from "../models/Store";

const csv = require('csv-parser');
const fs = require('fs');
const results = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/onlinestoremapping.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
});

export class OnlineStoreTagging1619756410165 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let datas = [];
        let data = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            datas.push(stores[i]);
        }

        let queryStr = ` `;
        let sets = [];

        for(let b in datas){
            for(let i in results) {
                if(datas[b].name.toString().toLowerCase() === results[i].name.toString().toLowerCase()){
                    sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET is_online=${results[i].is_online} WHERE id='${datas[b].id}'`);

                    if(results[i].website !== '') {
                        sets.push(`UPDATE ${env.TYPEORM_SCHEMA}.stores SET "website"='${results[i].website}' WHERE id='${datas[b].id}'`);
                    }
                }
            }
        }
        
        let sets2 = sets.join(';');
        let finalStr = queryStr+sets2;

        await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
