import {MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { Banner } from "../models/Banner";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const env = process.env;

export class CreateBanners1622574182905 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        fs.createReadStream('src/migrations/csv/banner.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', async() => {
            let seed = [];

            let count = 0;
            for(let i in results) {
                count += 1;
                seed.push({
                    'sort_order': count,
                    'title': results[i].title,
                    'description': results[i].description
                });
            }

            await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Banner)
            .values(seed)
            .execute();

            console.log(seed);
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
