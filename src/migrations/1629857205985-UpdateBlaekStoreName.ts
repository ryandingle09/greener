import {MigrationInterface, QueryRunner} from "typeorm";

const env = process.env;

export class UpdateBlaekStoreName1629857205985 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let finalStr = `UPDATE ${env.TYPEORM_SCHEMA}.stores SET name='BLAEK' WHERE lower(name)='blaek';`;
        await queryRunner.query(finalStr.toString());
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
