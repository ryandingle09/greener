import {MigrationInterface, QueryRunner, getRepository, getConnection} from "typeorm";
import { Store } from "../models/Store";
import { SubSector } from "../models/SubSector";
import { StoreSubSector } from "../models/StoreSubSector";

const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const storesData = [];
const sectorsData = [];
const env = process.env;

fs.createReadStream('src/migrations/csv/store_sub_sector_mapping.csv')
.pipe(csv())
.on('data', (data) => results.push(data))
.on('end', async() => {});


export class StoreSubSectorMapping1622035264774 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let data = getRepository(Store);
        let stores = await data
            .createQueryBuilder("stores")
            .orderBy("stores.id", "ASC")
            .getMany();

        for(let i in stores) {
            storesData.push(stores[i]);
        }

        let datas = getRepository(SubSector);
        let sectors = await datas
            .createQueryBuilder("sectors")
            .orderBy("sectors.id", "ASC")
            .getMany();

        for(let i in sectors) {
            sectorsData.push(sectors[i]);
        }

        let sets = [];

        for(let i in results) {
            let storeId;
            let subs = results[i].SubSector.split(",");

            for(let b in storesData) {
                if(results[i].Store.toLowerCase() === storesData[b].name.toLowerCase()) {
                    storeId = storesData[b].id;

                    for(let ss in subs) {
                        let sectoreId
                        
                        for(let a in sectorsData) {
                            sectoreId = sectorsData[a].id;
        
                            if(subs[ss].toString().toLowerCase() === sectorsData[a].name.toLowerCase()) {
                                sets.push({
                                    'subSectorId': sectoreId,
                                    'storeId': storeId,
                                });
                            }
                        }
                    }
                }
            }
        }

        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(StoreSubSector)
            .values(sets)
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
