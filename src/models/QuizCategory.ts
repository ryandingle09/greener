import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany
  } from "typeorm";
  import {QuizQuestion} from "../models/QuizQuestion";
  
  @Entity('quiz_categories')
  @Unique(["name"])
  export class QuizCategory {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      sort_order: number

      @Column({nullable: true})
      name: string

      @Column({nullable: true})
      description: string

      @Column({nullable: true, default: true})
      has_computation: boolean
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @OneToMany(() => QuizQuestion, quizquestion => quizquestion.quizcategory)
      quizquestions: QuizQuestion[];

  }