import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
  } from "typeorm";
  import {Banner} from "../models/Banner";
  import {Store} from "../models/Store";
  
  @Entity('banner_items')
  export class BannerItem {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      bannerId: string

      @Column({nullable: true, type: 'uuid'})
      storeId: string

      @Column({nullable: true})
      logo: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => Banner, banner => banner.banneritems)
      banner: Banner;

      @ManyToOne(() => Store)
      @JoinColumn([
        { name: "storeId", referencedColumnName: "id" },
      ])
      store: Store;
  }