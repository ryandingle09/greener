import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  import { Length, IsNotEmpty, MaxLength } from "class-validator";
  import * as bcrypt from "bcryptjs";
  
  @Entity('users')
  @Unique(["email", "mobile_number"])
  export class User {

      @PrimaryGeneratedColumn('uuid')
      id: string;
    
      @Column({nullable: true})
      @Length(6)
      encrypted_id: string;

      @Column({nullable: true})
      yodlee_id: string
    
      @Column({nullable: true, default: false})
      is_onboarded: Boolean

      @Column({nullable: true})
      basiq_id: string

      @Column({nullable: true})
      hubspot_id: string

      @Column({nullable: true})
      apple_id: string

      @Column({nullable: true})
      @MaxLength(255)
      first_name: string

      @Column({nullable: true})
      @MaxLength(255)
      last_name: string

      @Column({nullable: true})
      birth_date: Date

      @Column({nullable: true})
      address: string

      @Column({nullable: true})
      @MaxLength(255)
      mobile_number: string

      @Column({nullable: true})
      @MaxLength(255)
      email: string;

      @Column({nullable: true})
      @Length(6)
      pin: string;
    
      @Column({nullable: true})
      @Length(6)
      password: string;
    
      @Column({nullable: true, default: false})
      is_from_fb: Boolean;
    
      @Column({nullable: true, default: false})
      is_from_google: Boolean;
    
      @Column({nullable: true, default: false})
      is_from_apple: Boolean;
    
      @Column()
      @IsNotEmpty()
      role: string;
    
      @Column({nullable: true})
      photo_key: string;
    
      @Column({nullable: true})
      photo_url: string;
    
      @Column({nullable: true, type: "json", default: null})
      device_ids: string;

      @Column({default: false})
      status: boolean;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;
    
      hashPassword() {
        this.password = bcrypt.hashSync(this.password.toString(), 10);
      }

      hashPin() {
        this.pin = bcrypt.hashSync(this.pin.toString(), 10);
      }
    
      checkIfUnencryptedPasswordIsValid(unencryptedPassword: string, encryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, encryptedPassword);
      }

      checkIfUnencryptedPinIsValid(unencryptedPin: string, encryptedPin: string) {
        return bcrypt.compareSync(unencryptedPin.toString(), encryptedPin);
      }

  }