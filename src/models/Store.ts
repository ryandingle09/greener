import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Unique,
    ManyToOne,
    OneToMany,
    JoinColumn
  } from "typeorm";
  import {StoreAddress} from "../models/StoreAddress";
  import {StoreSubSector} from "../models/StoreSubSector"
  import {Sector} from "../models/Sector";
  //import {BannerItem} from "../models/BannerItem";
  import {StoreAccreditation} from "../models/StoreAccreditation";
  
  @Entity('stores')
  @Unique(["name"])
  export class Store {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      name: string
    
      @Column({nullable: true, type: "simple-array", default: null})
      secondary_names: string[];

      @Column({nullable: true})
      logo: string

      @Column({nullable: true})
      banner_image: string

      @Column({nullable: true, default: 0})
      rating: number

      @Column({nullable: true})
      anzic_code: string

      @Column({nullable: true})
      sectorId: string

      @Column({nullable: true})
      accreditationId: string

      @Column({nullable: true})
      contact_number: string

      @Column({ type: 'jsonb', nullable: true })
      opening_hours: object

      @Column({ nullable: true, default: true })
      is_greenr: boolean

      @Column({ nullable: true})
      url: string

      @Column({ nullable: true})
      website: string

      @Column({ nullable: true, default: false })
      is_online: boolean

      @Column({ nullable: true})
      classification_type: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @OneToMany(() => StoreAddress, storeaddress => storeaddress.store)
      storeaddresses: StoreAddress[];

      @OneToMany(() => StoreSubSector, storesubsector => storesubsector.store)
      subsectors: StoreSubSector[];

      @ManyToOne(() => Sector)
      @JoinColumn([
        { name: "sectorId", referencedColumnName: "id" },
      ])
      sector: Sector;

      @OneToMany(() => StoreAccreditation, accreditations => accreditations.store)
      accreditations: StoreAccreditation[];

      // @OneToMany(() => BannerItem, banneritems => banneritems.store)
      // banneritems: BannerItem[];
  }