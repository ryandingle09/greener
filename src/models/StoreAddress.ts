import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import {Store} from "../models/Store";
import {StoreSubSector} from "../models/StoreSubSector";

@Entity('store_addresses')
export class StoreAddress {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    storeId: string

    @Column({nullable: true})
    address: string

    @Column({nullable: true})
    latitude: string

    @Column({nullable: true})
    longitude: string

    @Column({ type: 'jsonb', nullable: true })
    opening_hours: object
  
    @Column()
    @CreateDateColumn()
    created_at: Date;
  
    @Column()
    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(() => Store, store => store.storeaddresses)
    store: Store;

    @OneToMany(() => StoreSubSector, storesubsector => storesubsector.store)
    subsectors: StoreSubSector[];
}