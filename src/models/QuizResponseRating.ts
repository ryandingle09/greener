import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
  } from "typeorm";
  import {User} from "./User";
  import {QuizCategory} from "./QuizCategory";
  
  @Entity('quiz_response_rating')
  export class QuizResponseRating {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      userId: string

      @Column({nullable: true})
      quizcategoryId: string

      @Column({nullable: true, default: 0})
      badge_count: number

      @Column({nullable: true, type: "float"})
      rating: number
      
      @Column({nullable: true, type: "float"})
      low_rating: number
      
      @Column({nullable: true, type: "float"})
      high_rating: number
      
      @Column({nullable: true, type: "float"})
      transport: number
      
      @Column({nullable: true, type: "float"})
      flight: number
      
      @Column({nullable: true, type: "float"})
      calc_rating: number
      
      @Column({nullable: true, default: false})
      is_average: boolean

      @Column({nullable: true, type: "float"})
      over: number
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => QuizCategory)
      @JoinColumn([
        { name: "quizcategoryId", referencedColumnName: "id" },
      ])
      category: QuizCategory;

      // @ManyToOne(() => User)
      // @JoinColumn([
      //   { name: "userId", referencedColumnName: "id" },
      // ])
      // user: User;

  }