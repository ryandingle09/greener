import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  
  @Entity('classifications')
  export class Classification {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      anzic_code: string;

      @Column({nullable: true})
      anzic_description: string

      @Column({nullable: true})
      sector_id: string

      @Column({nullable: true})
      sub_code: string

      @Column({nullable: true, type: "float"})
      emission_factor_gc02: number

      @Column({nullable: true, type: "float"})
      emission_factor_tc02: number
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }