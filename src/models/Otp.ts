import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  import { MaxLength } from "class-validator";
  
  @Entity('otp_verifications')
  @Unique(["otp"])
  export class Otp {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      @MaxLength(6)
      otp: number

      @Column({default: false})
      used: boolean
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }