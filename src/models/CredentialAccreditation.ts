import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity('credential_accreditations')
  export class CredentialAccreditation {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      credential_id: string

      @Column()
      accreditation_id: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }