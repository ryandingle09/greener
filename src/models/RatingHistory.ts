import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity('rating_histories')
  export class RatingHistory {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      user_id: string

      @Column({nullable: true})
      store_name: string
      
      @Column({nullable: true, type: "float"})
      spend: number

      @Column({nullable: true, type: "float"})
      rating: number

      @Column({nullable: true, comment: 'Transaction Date'})
      calculation_date: Date;

      @Column({default: false})
      for_recommendation: boolean;

      @Column({nullable: true})
      category_code: string

      @Column({default: true})
      after_link: boolean;

      @Column({default: true})
      celebrated: boolean;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }