import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  
  @Entity('user_settings')
  export class UserSetting {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      user_id: string

      @Column({default: false})
      green_purchases_status: boolean;

      @Column({default: false})
      non_green_purchases_status: boolean;

      @Column({default: false})
      email_notification_status: boolean;

      @Column({default: false})
      location_services_status: boolean;

      @Column({default: false})
      face_id_status: boolean;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;
  }