import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Unique
} from "typeorm";
// import {StoreAccreditation} from "../models/StoreAccreditation";
  
@Unique(["name"])
@Entity('accreditations')
export class Accreditation {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({nullable: true})
    credential_id: string

    @Column()
    name: string

    @Column({nullable: true})
    description: string

    @Column({nullable: true})
    logo: string

    @Column({nullable: true})
    country: string

    @Column()
    @CreateDateColumn()
    created_at: Date;

    @Column()
    @UpdateDateColumn()
    updated_at: Date;

    // @ManyToOne(() => StoreAccreditation, accreditation => accreditation.accreditations)
    // accreditation: StoreAccreditation;

}