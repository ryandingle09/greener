import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  
  @Entity('jobs')
  export class Job {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      step: number;

      @Column({nullable: true})
      user_id: string;

      @Column({nullable: true})
      job_id: string;

      @Column({nullable: true})
      connection_id: string;

      @Column({nullable: true})
      fcm_token: string;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }