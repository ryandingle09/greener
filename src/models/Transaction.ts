import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity('transactions')
  @Unique(['transaction_id'])
  export class Transaction {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      basiq_id: string;

      @Column({nullable: true})
      transaction_id: string;

      @Column({nullable: true})
      basiq_name_encrypted: string;

      @Column({ type: "float", nullable: true} )
      amount: string;

      @Column({nullable: true})
      transaction_date: Date;

      @Column({nullable: true})
      account_id: string;

      @Column({nullable: true})
      institution: string;

      @Column({nullable: true})
      description: string;

      @Column({nullable: true})
      merchant_name: string;

      @Column({nullable: true})
      merchant_url: string;

      @Column({nullable: true})
      merchant_route_no: string;

      @Column({nullable: true})
      merchant_route: string;

      @Column({nullable: true})
      merchant_postal_code: string;

      @Column({nullable: true})
      merchant_suburb: string;

      @Column({nullable: true})
      merchant_state: string;

      @Column({nullable: true})
      merchant_country: string;

      @Column({nullable: true})
      merchant_formatted_address: string;

      @Column({nullable: true})
      merchant_geometry_lan: string;

      @Column({nullable: true})
      merchant_geometry_lon: string;

      @Column({nullable: true})
      sub_class: string;

      @Column({nullable: true})
      category_division: string;

      @Column({nullable: true})
      category_subdivision: string;

      @Column({nullable: true})
      category_group: string;

      @Column({nullable: true})
      category_class: string;

      @Column({nullable: true})
      logo_master: string;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }