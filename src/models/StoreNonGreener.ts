import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity('stores_non_greener')
  export class StoreNonGreener {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({nullable: true})
    name: string

    @Column({nullable: true})
    logo: string

    @Column({nullable: true})
    full_address: string

    @Column({nullable: true, default: 0})
    rating: number

    @Column({nullable: true})
    anzic_code: string

    @Column({nullable: true})
    contact_number: string

    @Column({ type: 'jsonb', nullable: true })
    opening_hours: object

    @Column({ nullable: true, default: false })
    is_greenr: boolean

    @Column({ nullable: true})
    url: string

    @Column({nullable: true})
    latitude: string

    @Column({nullable: true})
    longitude: string
  
    @Column()
    @CreateDateColumn()
    created_at: Date;
  
    @Column()
    @UpdateDateColumn()
    updated_at: Date;

  }