import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToOne,
    JoinColumn
  } from "typeorm";
  import {Store} from "./Store";
  import {SubSector} from "./SubSector";
  
  @Entity('store_sub_sectors')
  export class StoreSubSector {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      storeId: string

      @Column({nullable: true})
      subSectorId: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => Store, store => store.storeaddresses)
      store: Store;

    //   @OneToOne(() => SubSector)
    //   @JoinColumn()
    //   subsector: SubSector;

    //   @ManyToOne(() => SubSector, subsector => subsector.subsector)
    //   subsector: SubSector;

      @ManyToOne(() => SubSector)
      @JoinColumn(
          { name: "subSectorId", referencedColumnName: "id" }
      )
      info: SubSector;

  }