import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Unique,
    OneToMany
  } from "typeorm";
  import {BannerItem} from "../models/BannerItem";
  
  @Entity('banners')
  @Unique(["title"])
  export class Banner {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      sort_order: number

      @Column({nullable: true})
      title: string

      @Column({nullable: true})
      description: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @OneToMany(() => BannerItem, banneritems => banneritems.banner)
      banneritems: BannerItem[];
  }