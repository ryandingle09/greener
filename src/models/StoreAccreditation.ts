import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
  } from "typeorm";
  import {Store} from "../models/Store";
  import {Accreditation} from "../models/Accreditation";
  
  @Entity('store_accreditations')
  export class StoreAccreditation {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      storeId: string

      @Column()
      accreditationId: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => Store)
      @JoinColumn([
        { name: "storeId", referencedColumnName: "id" },
      ])
      store: Store;

      @ManyToOne(() => Accreditation)
      @JoinColumn([
        { name: "accreditationId", referencedColumnName: "id" },
      ])
      accreditations: Accreditation;

  }