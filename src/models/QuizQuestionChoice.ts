import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
  } from "typeorm";
  import {QuizQuestion} from "./QuizQuestion";
  
  @Entity('quiz_question_choices')
  export class QuizQuestionChoice {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      quizquestionId: string

      @Column({nullable: true})
      title: string

      @Column({nullable: true})
      value: string

      @Column({nullable: true})
      icon: string

      @Column({nullable: true})
      badge: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;;

      @ManyToOne(() => QuizQuestion)
      @JoinColumn([
        { name: "quizquestionId", referencedColumnName: "id" },
      ])
      choices: QuizQuestion;
  }