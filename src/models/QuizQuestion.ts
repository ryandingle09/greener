import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
    OneToMany
  } from "typeorm";
  import {QuizCategory} from "../models/QuizCategory";
  import {QuizQuestionChoice} from "../models/QuizQuestionChoice";
  
  @Entity('quiz_questions')
  export class QuizQuestion {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      quizcategoryId: string

      @Column({nullable: true})
      code: string

      @Column({nullable: true})
      title: string

      @Column({nullable: true})
      icon: string

      @Column({nullable: true, comment: 'number, multitple, yes_or_no'})
      type: string

      @Column({nullable: true, default: null,comment: 'if shown by answer value from prev question'})
      control: number

      @Column({nullable: true, default: null,comment: 'if questiom has a minimum answer for type number'})
      has_minimum: number

      @Column({nullable: true, default: null,comment: 'identify is badge will show if the answer is equal to value'})
      show_badge_if_value: number
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => QuizCategory)
      @JoinColumn([
        { name: "quizcategoryId", referencedColumnName: "id" },
      ])
      quizcategory: QuizCategory;

      @OneToMany(() => QuizQuestionChoice, choices => choices.choices)
      choices: QuizQuestionChoice[];
  }