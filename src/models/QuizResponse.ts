import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
  } from "typeorm";
  import {User} from "../models/User";
  import {QuizQuestion} from "../models/QuizQuestion";
  import {QuizCategory} from "../models/QuizCategory";
  import {QuizQuestionChoice} from "../models/QuizQuestionChoice";
  
  @Entity('quiz_responses')
  export class QuizResponse {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      userId: string

      @Column({nullable: true})
      quizcategoryId: string

      @Column({nullable: true})
      questionId: string

      @Column({nullable: true})
      choiceId: string

      @Column({nullable: true})
      answer: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => QuizCategory)
      @JoinColumn([
        { name: "quizcategoryId", referencedColumnName: "id" },
      ])
      category: QuizCategory;

      @ManyToOne(() => QuizQuestion)
      @JoinColumn([
        { name: "questionId", referencedColumnName: "id" },
      ])
      question: QuizQuestion;

      @ManyToOne(() => QuizQuestionChoice)
      @JoinColumn([
        { name: "choiceId", referencedColumnName: "id" },
      ])
      choice: QuizQuestionChoice;

      // @ManyToOne(() => User)
      // @JoinColumn([
      //   { name: "userId", referencedColumnName: "id" },
      // ])
      // user: User;

  }