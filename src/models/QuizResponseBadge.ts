import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  
  @Entity('quiz_response_badges')
  export class QuizResponseBadge {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      userId: string

      @Column({nullable: true})
      quizcategoryId: string

      @Column({nullable: true})
      questionId: string

      @Column({nullable: true})
      choiceId: string

      @Column({nullable: true})
      badge: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }