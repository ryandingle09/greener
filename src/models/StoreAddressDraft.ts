import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn
} from "typeorm";

@Entity('store_addresses_draft')
export class StoreAddressDraft {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    store_name: string

    @Column({nullable: true})
    address: string

    @Column({nullable: true})
    latitude: string

    @Column({nullable: true})
    longitude: string

    @Column({ type: 'jsonb', nullable: true })
    opening_hours: object
  
    @Column()
    @CreateDateColumn()
    created_at: Date;
  
    @Column()
    @UpdateDateColumn()
    updated_at: Date;
}