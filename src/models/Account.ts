import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";

  @Entity('accounts')
  export class Account {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true, type: "text"})
      user_id: string;

      @Column({nullable: true})
      account_id: string;

      @Column({nullable: true})
      account_name: string;

      @Column({nullable: true})
      bank_code: string;

      @Column({nullable: true, default: false})
      account_synced: boolean;

      @Column({nullable: true, default: false})
      is_join_account: boolean;

      @Column()
      @CreateDateColumn()
      created_at: Date;

      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }
