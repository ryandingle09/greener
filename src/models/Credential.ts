import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Unique
  } from "typeorm";
  
  @Unique(["name"])
  @Entity('credentials')
  export class Credential {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      name: string

      @Column({nullable: true})
      description: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }