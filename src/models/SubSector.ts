import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    Unique
  } from "typeorm";
  import {Sector} from "../models/Sector";
  // import {StoreSubSector} from "../models/StoreSubSector";
  
  @Unique(["sub_code"])
  @Entity('sub_sectors')
  export class SubSector {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      name: string

      @Column({nullable: true})
      sub_code: number

      @Column()
      sectorId: string

      @Column({nullable: true})
      description: string

      @Column({nullable: true})
      sort_order: number
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @ManyToOne(() => Sector, sector => sector.subsectors)
      sector: Sector;

    //   @OneToOne(() => StoreSubSector, storesubsector => storesubsector.info)
    //   subsector: StoreSubSector;

  }