import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity('mobile_codes')
  @Unique(["code"])
  export class MobileCode {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      name: string

      @Column({nullable: true})
      dial_code: string

      @Column({nullable: true})
      code: string

      @Column({nullable: true})
      flag: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }