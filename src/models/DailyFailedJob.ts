import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
  } from "typeorm";
  
  @Entity('daily_failed_jobs')
  export class DailyFailedJob {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column({nullable: true})
      details: string;

      @Column({nullable: true})
      user_id: string;

      @Column({nullable: true})
      job_id: string;

      @Column({nullable: true})
      bank_name: string;
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

  }