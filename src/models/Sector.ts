import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    Unique
  } from "typeorm";
  import {SubSector} from "../models/SubSector";
  
  @Unique(["name"])
  @Entity('sectors')
  export class Sector {

      @PrimaryGeneratedColumn('uuid')
      id: string;

      @Column()
      name: string

      @Column({nullable: true})
      sort_order: number

      @Column({nullable: true})
      description: string
    
      @Column()
      @CreateDateColumn()
      created_at: Date;
    
      @Column()
      @UpdateDateColumn()
      updated_at: Date;

      @OneToMany(() => SubSector, subSector => subSector.sector)
      subsectors: SubSector[];
  }