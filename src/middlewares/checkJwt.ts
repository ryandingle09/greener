import { Request, Response, NextFunction, response } from "express";
import * as jwt from "jsonwebtoken";

const env = process.env;

export const checkJwt = (request: Request, response: Response, next: NextFunction) => {

    let token = <string>request.headers["x-greener-auth-token"];
    let jwtPayload;

    try {
        jwtPayload = <any>jwt.verify(token, env.jwtSecret);
        response.locals.jwtPayload = jwtPayload;
    } catch (error) {
        return response.status(422).send({'message': 'Invalid token.'});
    }

    const { userId } = jwtPayload;
    const newToken = jwt.sign({ userId }, env.jwtSecret, {
        expiresIn: "1h",
    });

    response.setHeader("x-greener-auth-token", newToken);

    next();

}
