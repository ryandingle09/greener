import { Request, Response, NextFunction, response } from "express";
import { getRepository } from "typeorm";
import { User } from "../models/User";
import * as jwt from "jsonwebtoken";

const env = process.env;

export const checkRole = (roles: Array<string>) => {

    return async (request: Request, response: Response, next: NextFunction) => {

        let token = <string>request.headers["x-greener-auth-token"];
        let jwtPayload;

        try {
            jwtPayload = <any>jwt.verify(token, env.jwtSecret);
            response.locals.jwtPayload = jwtPayload;
        } catch (error) {
            return response.status(422).send({'message': 'Invalid token.'});
        }

        const { userId } = jwtPayload;

        let userRepository = getRepository(User);
        let user: User;

        try {
            user = await userRepository.findOneOrFail(userId);
        } catch (error) {
            return response.status(422).send({'message': "User not found."});
        }

        if(user.status == false) 
            return response.status(422).send({'message': 'User role is allowed.'});

        if (roles.indexOf(user.role) > -1) 
            next();
        else 
            return response.status(422).send({'message': 'User role is allowed.'});
    }

}