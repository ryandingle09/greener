import { Request, Response, NextFunction, response } from "express";

const env = process.env;

export const checkPassesToken = (request: Request, response: Response, next: NextFunction) => {

    let passes_token = <string>request.headers["x-greener-passes-token"];

    if(passes_token !== env.PASSES_TOKEN) {
        return response.status(404).send({'message': 'Page Not Found.'});
    }

    next();

}