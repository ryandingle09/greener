import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";

require('dotenv').config()

const fileUpload = require('express-fileupload');

// createConnection().then(async connection => {
createConnection().then(async() => {


    // create express app
    const app = express();

    app.use(fileUpload());

    app.use(cors());
    //app.use(cors({origin: 'http://127.0.0.1:8021'}));
    app.use(helmet());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.get('', function(request, response) {
        response.send('Hello World!');
    });

    app.use('/', routes);

    // start express server
    app.listen(3000);

    console.log("Express server has started on port 3000.");

}).catch(error => console.log(error));
