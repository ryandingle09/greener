import { Router } from "express";
import { MixPanelController }  from "../controllers/MixPanelController";
import { checkJwt } from "../middlewares/checkJwt";

const mixpanelRoutes = Router();

mixpanelRoutes.post("/track-event", [checkJwt], MixPanelController.trackEvent);
mixpanelRoutes.post("/track-branch-user-profile-event", [checkJwt], MixPanelController.setBranchUserProfileEvent);

export default mixpanelRoutes;