import { Router } from "express";
import { QuizCategoryController}  from "../controllers/QuizCategoryController";
import { QuizQuestionController}  from "../controllers/QuizQuestionController";
import { QuizResponseController}  from "../controllers/QuizResponseController";
import { checkJwt } from "../middlewares/checkJwt";

const quizRoutes = Router();

quizRoutes.get("/category/list", [checkJwt], QuizCategoryController.list);
// bannerRoutes.get("/category/search", [checkJwt], QuizCategoryController.search);
// bannerRoutes.post("/category/create", [checkJwt], QuizCategoryController.create);
// bannerRoutes.post("/category/update/:id", [checkJwt], QuizCategoryController.update);
// bannerRoutes.get("/category/detail/:id", [checkJwt], QuizCategoryController.detail);
// bannerRoutes.post("/category/delete/:id", [checkJwt], QuizCategoryController.delete);

quizRoutes.get("/question/list", [checkJwt], QuizQuestionController.list);
// bannerRoutes.get("/question/search", [checkJwt], QuizQuestionController.search);
// bannerRoutes.post("/question/create", [checkJwt], QuizQuestionController.create);
// bannerRoutes.post("/question/update/:id", [checkJwt], QuizQuestionController.update);
// bannerRoutes.get("/question/detail/:id", [checkJwt], QuizQuestionController.detail);
// bannerRoutes.post("/question/delete/:id", [checkJwt], QuizQuestionController.delete);

quizRoutes.get("/response/list", [checkJwt], QuizResponseController.list);
quizRoutes.post("/response/save-answer", [checkJwt], QuizResponseController.saveResponse);
quizRoutes.get("/response/get/rating", [checkJwt], QuizResponseController.rating);
quizRoutes.post("/response/save-badge", [checkJwt], QuizResponseController.saveEarnedBadge);
quizRoutes.get("/response/get/badges", [checkJwt], QuizResponseController.badges);

export default quizRoutes;