
import { Router } from "express";
// import { BasiqController}  from "../controllers/BasiqController";
import * as BasiqController  from "../controllers/BasiqController/index";
import { checkJwt } from "../middlewares/checkJwt";
import { checkPassesToken } from "../middlewares/checkPassesToken";

const basiqRoutes = Router();

basiqRoutes.get("/get/token", [checkJwt], BasiqController.Connection.getToken); // for getting admin token not user token
basiqRoutes.get("/create/connection", [checkJwt], BasiqController.Connection.createConnection); // for iniating test connection -- sandbox only
basiqRoutes.get("/accounts", [checkJwt], BasiqController.Account.getAccounts);
basiqRoutes.post("/accounts/create", [checkJwt], BasiqController.Account.createAccount);
basiqRoutes.get("/accounts-set-join", [checkJwt], BasiqController.Account.setJoinAccount);
basiqRoutes.get("/accounts-from-basiq", [checkJwt], BasiqController.Account.getAccountFromBasic);
basiqRoutes.get("/accounts/:accountId", [checkJwt], BasiqController.Account.getAccount);
basiqRoutes.get("/accounts-delete", [checkJwt], BasiqController.Account.deleteAccount);
basiqRoutes.get("/get/user", [checkJwt], BasiqController.Other.getUser);
basiqRoutes.get("/check/connection", [checkJwt], BasiqController.Job.checkConnection); // for checking connection from basiq and notify device
basiqRoutes.get("/sync/initial-transactions", [checkJwt], BasiqController.Account.syncInitialTransactions); // call upon linking bank account
basiqRoutes.get("/sync/user-accounts", [checkJwt], BasiqController.Account.syncUserAccounts); // call upon linking bank account as well
basiqRoutes.get("/check/bank-connected-status", [checkJwt], BasiqController.Account.getConnectedBankStatus); // check if user has already link an account/bank
basiqRoutes.get("/checkInitailJob", [checkJwt], BasiqController.Job.checkInitailJob); // check if user has already link an account/bank
basiqRoutes.get("/get/job-detail", [checkJwt], BasiqController.Job.getJobFromConnection); // get jobId

// public api for cronjob purpose
basiqRoutes.get("/sync/transactions", [checkPassesToken], BasiqController.Cron.syncDailyTransactions); //for cronjob
//basiqRoutes.get("/sync/accounts", [checkPassesToken], BasiqController.syncAccounts); //for cronjob
basiqRoutes.get("/sync/non-greener-stores", [checkPassesToken], BasiqController.Cron.syncNonGreenerStores); // for cronjob
basiqRoutes.get("/sync/price-level", [checkPassesToken], BasiqController.Cron.syncPriceLevel); // for cronjob
// basiqRoutes.get("/sync/store-addresses", [checkPassesToken], BasiqController.syncStoreAddress); // for cronjob and do not run if not needed cause it will generate cost to google places api
basiqRoutes.post("/search/store-addresses", [checkPassesToken], BasiqController.Other.addressSearchViaGmaps); // for stor address searcg dataase migration purpose - generate cost to google places api
// basiqRoutes.get("/sync/encrypt-user-id", [checkPassesToken], BasiqController.encryptUserIdsInAccount); // for encrypting user_ids in accounts
basiqRoutes.get("/sync/user-rating-to-hubspot", [checkPassesToken], BasiqController.Cron.HSGreenerRating); // for cronjob at 3am daily
basiqRoutes.get("/sync/user-weekly-rating-to-hubspot", [checkPassesToken], BasiqController.Cron.HSGreenerWeeklyRating); // for cronjob at 3am every monday/weekly
basiqRoutes.get("/check/job-connection", [checkPassesToken], BasiqController.Job.checkJobConnection); // for cronjob checking job connection from basiq and notify device
basiqRoutes.get("/convert-svg-to-png-images", [checkPassesToken], BasiqController.Cron.convertSVGtoPNGImages); // for cronjob checking job connection from basiq and notify device
basiqRoutes.get("/notify-users-need-bank-relogin", [checkPassesToken], BasiqController.Cron.notifyUsersNeedBankRelogin); // for cronjob 8am

export default basiqRoutes;
