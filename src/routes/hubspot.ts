
import { Router } from "express";
import { HubspotController}  from "../controllers/HubspotController";
import { checkJwt } from "../middlewares/checkJwt";

const hubspotRoutes = Router();

hubspotRoutes.get("/app/open/sync", [checkJwt], HubspotController.AppOpen);
hubspotRoutes.post("/track-branch-user-profile-event", [checkJwt], HubspotController.setBranchUserProfileEvent);

export default hubspotRoutes;