
import { Router } from "express";
import { HealthController}  from "../controllers/HealthController";

const healthRoutes = Router();

healthRoutes.get("/check", HealthController.check);

export default healthRoutes;