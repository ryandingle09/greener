
import { Router } from "express";
import { SettingController}  from "../controllers/SettingController";
import { checkJwt } from "../middlewares/checkJwt";
// import { checkRole } from "../middlewares/checkRole";
import { Setting } from "../validators/Setting";
const settingRoutes = Router();

settingRoutes.post("/account/update/:id", [checkJwt, Setting.accountUpdate], SettingController.accountUpdate);
settingRoutes.get("/account/detail/:id", [checkJwt], SettingController.accountDetail);

// personal screen endpoints
settingRoutes.get("/personal/detail", [checkJwt], SettingController.personalDetail);
settingRoutes.post("/personal/update/info", [checkJwt, Setting.updateInfo], SettingController.updateInfo);
settingRoutes.post("/personal/update/email", [checkJwt, Setting.updateEmail], SettingController.updateEmail);
settingRoutes.post("/personal/update/mobile", [checkJwt, Setting.updateMobile], SettingController.updateMobile);
settingRoutes.post("/personal/update/pin", [checkJwt, Setting.updatePin], SettingController.updatePin);
settingRoutes.post("/personal/update/face-id", [checkJwt], SettingController.updateFaceID);
settingRoutes.post("/personal/deactivate", [checkJwt], SettingController.deactivateAccount);
settingRoutes.post("/personal/update/setting", [checkJwt, Setting.updateSetting], SettingController.updateSetting);
settingRoutes.post("/personal/update/photo", [checkJwt], SettingController.updatePhoto);
settingRoutes.post("/personal/update/social-photo", [checkJwt, Setting.updatePhotoBySocial], SettingController.updatePhotoBySocial);
settingRoutes.post("/personal/update/onboard-status", [checkJwt], SettingController.updateOnboardStatus);
settingRoutes.post("/personal/update/device-ids", [checkJwt], SettingController.updateDeviceIds);

export default settingRoutes;