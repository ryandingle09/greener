
import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import { RatingController}  from "../controllers/RatingController";

const ratingRoutes = Router();

ratingRoutes.get("/halo", [checkJwt], RatingController.getHaloCalculation);
ratingRoutes.get("/past-week-activity", [checkJwt], RatingController.pastWeekActivity);
ratingRoutes.get("/top-recommendation", [checkJwt], RatingController.topRecommendation);
ratingRoutes.get("/top-swap", [checkJwt], RatingController.topSwap);
ratingRoutes.get("/kgc02e", [checkJwt], RatingController.getReforestation);
ratingRoutes.get("/greener-offset", [checkJwt], RatingController.getGreenerOffset);
ratingRoutes.get("/trees-planted", [checkJwt], RatingController.getTreesPlanted);
ratingRoutes.get("/impact-rating", [checkJwt], RatingController.getImpactRating);
ratingRoutes.get("/halo-and-linked-accounts", [checkJwt], RatingController.getHaloAndLinkedAccounts);

export default ratingRoutes;