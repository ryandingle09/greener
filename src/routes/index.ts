import { Router } from 'express';
import authRoutes from './auth';
import userRoutes from './user';
import settingRoutes from './setting';
import storeRoutes from './store';
import healthRoutes from './health';
// import yodleeRoutes from './yodlee';
import transactionRoutes from './transaction';
import ratingRoutes from './rating';
import basiqRoutes from './basiq';
import mixpanelRoutes from './mixpanel';
import bannerRoutes from './banner';
import quizRoutes from './quiz';
import hubspotRoutes from './hubspot';

import * as User from '../swagger-docs/user.swagger.json';
import * as Transaction from '../swagger-docs/transaction.swagger.json';
import * as Setting from '../swagger-docs/setting.swagger.json';
import * as Auth from '../swagger-docs/auth.swagger.json';
import * as Store from '../swagger-docs/store.swagger.json';
// import * as Yodlee from '../swagger-docs/yodlee.swagger.json';
import * as Health from '../swagger-docs/health.swagger.json';
import * as Rating from '../swagger-docs/rating.swagger.json';
import * as Basiq from '../swagger-docs/basiq.swagger.json';

const swaggerUi =  require('swagger-ui-express');
const routes = Router();

routes.use('/api/v1/doc/health', swaggerUi.serve, swaggerUi.setup(Health));
routes.use('/api/v1/doc/auth', swaggerUi.serve, swaggerUi.setup(Auth));
routes.use('/api/v1/doc/transaction', swaggerUi.serve, swaggerUi.setup(Transaction));
routes.use('/api/v1/doc/user', swaggerUi.serve, swaggerUi.setup(User));
routes.use('/api/v1/doc/setting', swaggerUi.serve, swaggerUi.setup(Setting));
routes.use('/api/v1/doc/store', swaggerUi.serve, swaggerUi.setup(Store));
// routes.use('/api/v1/doc/yodlee', swaggerUi.serve, swaggerUi.setup(Yodlee));
routes.use('/api/v1/doc/rating', swaggerUi.serve, swaggerUi.setup(Rating));
routes.use('/api/v1/doc/basiq', swaggerUi.serve, swaggerUi.setup(Basiq));

routes.use("/api/v1/health", healthRoutes);

routes.use("/api/v1/auth", authRoutes);
routes.use("/api/v1/user", userRoutes);
routes.use("/api/v1/setting", settingRoutes);
routes.use("/api/v1/store", storeRoutes);
// routes.use("/api/v1/yodlee/", yodleeRoutes);
routes.use("/api/v1/transaction", transactionRoutes);
routes.use("/api/v1/rating", ratingRoutes);
routes.use("/api/v1/basiq/", basiqRoutes);
routes.use("/api/v1/mixpanel/", mixpanelRoutes);
routes.use("/api/v1/banner/", bannerRoutes);
routes.use("/api/v1/quiz/", quizRoutes);
routes.use("/api/v1/hubspot/", hubspotRoutes);

export default routes;