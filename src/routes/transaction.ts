
import { Router } from "express";
import { TransactionController }  from "../controllers/TransactionController";
// import { TransactionCategoryDetailController }  from "../controllers/TransactionCategoryDetailController";
// import { TransactionHighLevelCategoryController }  from "../controllers/TransactionHighLevelCategoryController";
// import { TransactionMasterCategoryController }  from "../controllers/TransactionMasterCategoryController.ts.bak";
import { checkJwt } from "../middlewares/checkJwt";

const transactionRoutes = Router();

// transactionRoutes.get("/list", [checkJwt], TransactionController.list);
// transactionRoutes.post("/delete/:accountId", [checkJwt], TransactionController.delete);
// transactionRoutes.get("/detail-category/list", [checkJwt], TransactionCategoryDetailController.list);
// transactionRoutes.get("/high-level-category/list", [checkJwt], TransactionHighLevelCategoryController.list);
// transactionRoutes.get("/master-category/list", [checkJwt], TransactionMasterCategoryController.list);
transactionRoutes.get("/past-3months-activity", [checkJwt], TransactionController.past3MonthsActivity);
transactionRoutes.get("/detail", [checkJwt], TransactionController.transactionDetail);

export default transactionRoutes;