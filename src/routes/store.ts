
import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import { Credential } from "../validators/Credential";
import { CredentialController}  from "../controllers/CredentialController";
import { AccreditationController}  from "../controllers/AccreditationController";
import { SectorController}  from "../controllers/SectorController";
import { StoreController}  from "../controllers/StoreController";
import { SubSectorController } from "../controllers/SubSectorController";
import { StoreAddressController}  from "../controllers/StoreAddressController";
// import { StoreTypeController}  from "../controllers/StoreTypeController.ts.bak";
import { Sector } from "../validators/Sector";
import { Accreditation } from "../validators/Accreditation";
import { Store } from "../validators/Store";
// import { StoreType } from "../validators/StoreType";

const storeRoutes = Router();

storeRoutes.get("/credential/list", [checkJwt], CredentialController.list);
storeRoutes.get("/credential/search", [checkJwt], CredentialController.search);
storeRoutes.post("/credential/create", [checkJwt, Credential.create], CredentialController.create);
storeRoutes.post("/credential/update/:id", [checkJwt, Credential.update], CredentialController.update);
storeRoutes.get("/credential/detail/:id", [checkJwt], CredentialController.detail);
storeRoutes.post("/credential/delete/:id", [checkJwt], CredentialController.delete);

storeRoutes.get("/accreditation/list", [checkJwt], AccreditationController.list);
storeRoutes.get("/accreditation/search", [checkJwt], AccreditationController.search);
storeRoutes.post("/accreditation/create", [checkJwt, Accreditation.create], AccreditationController.create);
storeRoutes.post("/accreditation/update/:id", [checkJwt, Accreditation.update], AccreditationController.update);
storeRoutes.get("/accreditation/detail/:id", [checkJwt], AccreditationController.detail);
storeRoutes.post("/accreditation/delete/:id", [checkJwt], AccreditationController.delete);

storeRoutes.get("/sector/list", [checkJwt], SectorController.list);
storeRoutes.get("/sector/search", [checkJwt], SectorController.search);
storeRoutes.post("/sector/create", [checkJwt, Sector.create], SectorController.create);
storeRoutes.post("/sector/update/:id", [checkJwt, Sector.update], SectorController.update);
storeRoutes.get("/sector/detail/:id", [checkJwt], SectorController.detail);
storeRoutes.post("/sector/delete/:id", [checkJwt], SectorController.delete);

storeRoutes.get("/sector/sub/list", [checkJwt], SubSectorController.list);
storeRoutes.get("/sector/sub/search", [checkJwt], SubSectorController.search);
storeRoutes.post("/sector/sub/create", [checkJwt, Sector.create], SubSectorController.create);
storeRoutes.post("/sector/sub/update/:id", [checkJwt, Sector.update], SubSectorController.update);
storeRoutes.get("/sector/sub/detail/:id", [checkJwt], SubSectorController.detail);
storeRoutes.post("/sector/sub/delete/:id", [checkJwt], SubSectorController.delete);

storeRoutes.get("/recommended", [checkJwt], StoreController.recommended);
storeRoutes.get("/list", [checkJwt], StoreController.list);
storeRoutes.get("/search", [checkJwt], StoreController.search);
storeRoutes.get("/sector/:id", [checkJwt], StoreController.findBySector);
storeRoutes.post("/create", [checkJwt, Store.create], StoreController.create);
storeRoutes.post("/update/:id", [checkJwt, Store.update], StoreController.update);
storeRoutes.get("/detail/:id", [checkJwt], StoreController.detail);
storeRoutes.post("/delete/:id", [checkJwt], StoreController.delete);

storeRoutes.get("/address/list", [checkJwt], StoreAddressController.list);
storeRoutes.get("/address/search", [checkJwt], StoreAddressController.search);
// storeRoutes.post("/address/create", [checkJwt, Store.create], StoreController.create);
// storeRoutes.post("/address/update/:id", [checkJwt, Store.update], StoreController.update);
storeRoutes.get("/address/detail/:id", [checkJwt], StoreAddressController.detail);
// storeRoutes.post("/address/delete/:id", [checkJwt], StoreController.delete);

// storeRoutes.get("/type/list", [checkJwt], StoreTypeController.list);
// storeRoutes.get("/type/search", [checkJwt], StoreTypeController.search);
// storeRoutes.post("/type/create", [checkJwt, StoreType.create], StoreTypeController.create);
// storeRoutes.post("/type/update/:id", [checkJwt, StoreType.update], StoreTypeController.update);
// storeRoutes.get("/type/detail/:id", [checkJwt], StoreTypeController.detail);
// storeRoutes.post("/type/delete/:id", [checkJwt], StoreTypeController.delete);
// storeRoutes.get("/type/halo", [checkJwt], StoreTypeController.getHaloCalculation);
// storeRoutes.get("/type/past-week-activity", StoreTypeController.pastWeekActivity);

export default storeRoutes;