import { Router } from "express";
import { AuthController } from "../controllers/AuthController";
import { Auth } from "../validators/Auth";

const authRoutes = Router();

authRoutes.post("/login", [Auth.login], AuthController.login);
authRoutes.post("/register", [Auth.create], AuthController.create); 
authRoutes.post("/check-email", [Auth.checkEmail], AuthController.checkEmail); 
authRoutes.post("/check-mobile-number", [Auth.checkMobileNumber], AuthController.checkMobileNumber); 
authRoutes.post("/forgot-password", [Auth.forgotPassword], AuthController.forgotPassword);
authRoutes.post("/set-password/:token", [Auth.setPassword], AuthController.setPassword);
authRoutes.post("/refresh-token", [Auth.refreshToken], AuthController.refreshToken);
authRoutes.post("/generate-otp", [Auth.generateOtp], AuthController.generateOtp);
authRoutes.post("/verify-otp", [Auth.verifyOtp], AuthController.verifyOtp);
authRoutes.post("/set-pin", [Auth.setPin], AuthController.setPin);
authRoutes.post("/verify-pin", [Auth.verifyPin], AuthController.verifyPin);
authRoutes.post("/generate-otp-via-email", [Auth.checkEmail], AuthController.generateOtpViaEmail);
authRoutes.post("/verify-email", [Auth.checkEmail], AuthController.verifyEmail);
authRoutes.post("/verify-mobile", [Auth.checkMobileNumber], AuthController.verifyMobileNumber);
authRoutes.post("/verify-mobile-owner", [Auth.verifyMobileNumberOwner], AuthController.verifyMobileNumberOwner);
authRoutes.post("/update-pin", [Auth.updatePin], AuthController.updatePin);
authRoutes.post("/check-pin-status", [Auth.checkPinStatus], AuthController.checkPinStatus);

export default authRoutes;