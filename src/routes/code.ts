
import { Router } from "express";
import { CodeController}  from "../controllers/CodeController";
import { checkJwt } from "../middlewares/checkJwt";

const codeRoutes = Router();

codeRoutes.get("/mobile/list", [checkJwt], CodeController.list);
codeRoutes.get("/mobile/search", [checkJwt], CodeController.search);

export default codeRoutes;