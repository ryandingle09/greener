import { Router } from "express";
import { BannerController}  from "../controllers/BannerController";
import { checkJwt } from "../middlewares/checkJwt";

const bannerRoutes = Router();

bannerRoutes.get("/list", [checkJwt], BannerController.list);
bannerRoutes.get("/search", [checkJwt], BannerController.search);
bannerRoutes.post("/create", [checkJwt], BannerController.create);
bannerRoutes.post("/update/:id", [checkJwt], BannerController.update);
bannerRoutes.get("/detail/:id", [checkJwt], BannerController.detail);
bannerRoutes.post("/delete/:id", [checkJwt], BannerController.delete);

export default bannerRoutes;