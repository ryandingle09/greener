
import { Router } from "express";
import { UserController}  from "../controllers/UserController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const userRoutes = Router();

userRoutes.get("/search", [checkJwt, checkRole(["0"])], UserController.search);
userRoutes.get("/list", [checkJwt, checkRole(["0"])], UserController.list);
userRoutes.get("/detail/:id", [checkJwt, checkRole(["0"])], UserController.detail);
userRoutes.post("/create", [checkJwt, checkRole(["0"])], UserController.create); //admin creation account
userRoutes.post("/update/:id", [checkJwt, checkRole(["0"])], UserController.update);
// userRoutes.delete("/delete/:id",[checkJwt, checkRole(["ADMIN"])], UserController.remove);

export default userRoutes;