import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class Auth {

	static login = async(request: Request, response: Response, next: NextFunction) => {
		let { flow_type } = request.body;
		let validate;

		if(flow_type === 'social') {
			validate = new Validator(request.body, {
				email: 'required|email',
				pin: 'required',
				flow_type: 'required'
			});
		}else {
			validate = new Validator(request.body, {
				mobile_number: 'required',
				pin: 'required',
				flow_type: 'required'
			});
		}
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static create = async(request: Request, response: Response, next: NextFunction) => {
		let { flow_type } = request.body;
		let validate;

		if(flow_type === 'social') {
			validate = new Validator(request.body, {
				email: 'required|email',
				flow_type: 'required'
			});
		}else {
			validate = new Validator(request.body, {
				mobile_number: 'required',
				flow_type: 'required'
			});
		}
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static verifyPin = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            user_id: 'required|email',
			pin: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static setPin = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            user_id: 'required',
			pin: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static verifyOtp = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            otp: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static generateOtp = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            mobile_number: 'required',
            is_signup: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static refreshToken = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            user_id: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static setPassword = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            email: 'required|email',
            password: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static forgotPassword = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            email: 'required|email'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
    
    static checkEmail = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            email: 'required|email'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}
	
	static checkMobileNumber = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            mobile_number: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
	
	static verifyMobileNumberOwner = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            mobile_number: 'required',
            email: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }
	
	static updatePin = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            pin: 'required',
            mobile_number: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

	static checkPinStatus = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            identity: 'required',
            via: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

}