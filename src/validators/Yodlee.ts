import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class Yodlee {
    
    static create = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
            email: 'required|email',
            first_name: 'required',
			last_name: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

}