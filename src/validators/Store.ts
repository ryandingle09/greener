import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class Store {

	static create = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			name: 'required',
			logo: 'required',
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			name: 'required',
			logo: 'required',
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

}