import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class User {

	static create = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			email: 'required|email',
			first_name: 'required',
			last_name: 'required',
			password: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			address: 'required',
			first_name: 'required',
			last_name: 'required',
			birth_date: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

}