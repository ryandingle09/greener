import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class StoreType {

	static create = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			store_name: 'required',
			transaction_master_category_id: 'required',
			transaction_detial_category_id: 'required',
			base: 'required',
			better_base: 'required',
			greenr_base: 'required',
			misc_other_value_base: 'required',
			bring_own_bag_base: 'required',
			walk_to_shop_base: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

    static update = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			store_name: 'required',
			transaction_master_category_id: 'required',
			transaction_detial_category_id: 'required',
			base: 'required',
			better_base: 'required',
			greenr_base: 'required',
			misc_other_value_base: 'required',
			bring_own_bag_base: 'required',
			walk_to_shop_base: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
    }

}