import {NextFunction, Request, Response} from "express";

const { Validator } = require('node-input-validator');

export class Setting {

	static accountUpdate = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			email: 'required|email',
			pin: 'required',
			mobile_number: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}
	
	static updateEmail = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			email: 'required|email',
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}
	
	static updateMobile = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			mobile_number: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}
	
	static updatePin = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			pin: 'required|minLength:6|maxLength:6',
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}

	static updateSetting = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			items: 'required',
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}

	static updateInfo = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			first_name: 'required',
			last_name: 'required',
			address: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}

	static updatePhoto = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			photo: 'required|mime:jpg,png,jpeg,svg'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}

	static updatePhotoBySocial = async(request: Request, response: Response, next: NextFunction) => {

        let validate = new Validator(request.body, {
			photo_url: 'required'
		});
		
		validate.check().then((matched) => {
			if (!matched) {
				return response.status(422).send(validate.errors);
			}
		});
        
        next();
	}

}