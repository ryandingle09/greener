# Greener Wallet Backend Application

# REQUIREMENTS
1. NodeJS v12.+ and 6.9.+
2. TypeOrm with Express TS https://typeorm.io/
3. postgres
4. Swagger

Steps to run this project:

Create a `.env` file and copy the content from `.env.example` file in the root directory and make sure to add the value in the variables to used in the configuration of the application.

1. Run `npm i` command to install dependencies
2. Setup database settings inside `ormconfig.json` file
3. Run `npm run dev` command to run app in dev mode to enable creation of the db table schema or schema update.
4. Run `npm run migration:run` command to run database migration seed file for test user and admin /src/migration.
5. Run `npm run dev` command to run app in dev mode.


Other Commands to Run this Project:

1. Run `npm run tsc` command to build distribution build package.
2. Run `npm run start` command to run build package.
3. Run `npm run prod` command to create build and start the appliaction automatically.
4. Run `npm run migration:run` command to run database migration seed file.

Docker Deployment:
1. build the image
sudo docker build -t greenr/greenr-app:0.0.1 .

2. run the image in the container
sudo docker run -it --restart=always --name greenr-app -d -p 3000:3000 greenr/greenr-app:0.0.1

3. stop container
sudo docker container stop greenr-app

API ENDPOINT SWAGGER DOCUMENTATION:
1. /api/v1/doc/health
2. /api/v1/doc/yodlee - not active/remove
3. /api/v1/doc/auth
4. /api/v1/doc/setting
5. /api/v1/doc/store
6. /api/v1/doc/user
7. /api/v1/doc/transaction
8. /api/v1/doc/code
9. /api/v1/doc/rating
10. /api/v1/doc/basiq

API Endpoints:

1. Authentication - /api/v1/auth - no middleware
- /login - POST - PARAMS['email_or_mobile_number','pin','flow_type'] - `flow_type` = `social` or `mobile`
- /register - POST - PARAMS['email_or_mobile_number','pin','first_name(optional)','last_name(optional)','address','is_from_apple','apple_id','is_from_fb','is_from_google','flow_type'] - `flow_type` = `social` or `mobile`
- /check-email - POST - PARAMS['email']
- /check-mobile-number - POST - PARAMS['mobile_number']
- /forgot-password - POST - PARAMS['email'] - no updated
- /set-password/:token - POST - PARAMS['email_password'] - no updated
- /refresh-token - POST - PARAMS['user_id']
- /generate-otp - POST - PARAMS['mobile_number','is_signup=yes/no']
- /verify-otp - POST - PARAMS['otp']
- /set-pin - POST - PARAMS['pin','user_id']
- /verify-pin - POST - PARAMS['pin','user_id']
- /verify-email - POST - PARAMS['email']
- /verify-mobile - POST - PARAMS['mobile_number']
- /generate-otp-via-email - POST - PARAMS['email']
- /update-pin - POST - PARAMS['pin','mobile_number']
- /verify-mobile-owner - POST - PARAMS['email','mobile_number]

2. Users - /api/v1/user - must be authenticated using the x-greener-auth-token header - Used for admin purpose api
- /search?query /search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination
- /list?page=2 - GET PARAMS['page'] - for pagination
- /detail/:id - GET - PARAMS['id']
- /create - POST - PARAMS['first_name','last_name','email','password']
- /update/:id - POST - PARAMS['first_name','last_name','birth_date','address']

3. Settings - /api/v1/setting - must be authenticated using the x-greener-auth-token header - Used for customer/client side api
- /account/detail/:id - GET - params['id'] -- old
- /account/update/:id - GET - PARAMS['id'] - POST['email','mobile_number','pin'] -- old
- /personal/detail - GET
- /personal/update/email - POST - params['email']
- /personal/update/info - POST - params['first_name','last_name','address]
- /personal/update/mobile - POST - params['mobile_number']
- /personal/update/pin - POST - params['pin']
- /personal/update/face-id - POST - Finalizing
- /personal/deactivate - POST
- /personal/update/face-id - POST params['status'] - boolean true or false
- /personal/update/setting - POST - params['items with value multi dimentional array/object with key item and key value'] - values should only boolean and the key in ('green_purchases','non_green_purchases','email_notification','location_services')
- /personal/update/photo - POST params['photo'] - formdata
- /personal/update/social-photo - POST params['photo_url']

4. Store - /api/v1/store - must be authenticated using the x-greener-auth-token header
- /list /list?page=2 - GET PARAMS['page','is_greener(optional/0_or_1_value_only)'] - for pagination and sorting greener or not stores
- /search?query=test&page=2 - GET PARAMS['query','page','is_greener(optional/0_or_1_value_only)'] - query string value and pagination, sorting greener or not stores
- /create - POST - PARAMS['name','description']
- /sector/:id - GET - params['id']
- /detail/:id - GET - params['id']
- /update/:id - POST - PARAMS['name','description'] - GET PARAMS['id]
- /delete/:id - POST - params['id']

- /address/list /address/list?page=2 - GET PARAMS['page'] - for pagination
- /address/search?address=test&page=2 - GET PARAMS['address','page'] - query string value and pagination
- /address/search?latitude=10.3039&longitude=-9.0944 - GET PARAMS['latitude','longitude','range'] - query string value for lat and long, range default value is 5000 = 5km
- /address/detail/:id - GET - params['id']

- /sector/list /list?page=2 - GET PARAMS['page'] - for pagination
- /sector/search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination
- /sector/create - POST - PARAMS['name','description'] - not active
- /sector/detail/:id - GET - params['id']
- /sector/update/:id - POST - PARAMS['name','description'] - GET PARAMS['id] - not active
- /search/delete/:id - POST - params['id'] - not active

- /credential/list /list?page=2 - GET PARAMS['page'] - for pagination
- /credential/search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination
- /credential/create - POST - PARAMS['name','description'] - not active
- /credential/detail/:id - GET - params['id']
- /credential/update/:id - POST - PARAMS['name','description'] - GET PARAMS['id] - not active
- /credential/delete/:id - POST - params['id'] - not active

- /accreditation/list /list?page=2 - GET PARAMS['page'] - for pagination
- /accreditation/search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination
- /accreditation/create - POST - PARAMS['name','description','logo','country'] - not active
- /accreditation/detail/:id - GET - params['id']
- /accreditation/update/:id - POST - PARAMS['name','description','logo','country'] - GET PARAMS['id] - not active
- /accreditation/delete/:id - POST - params['id'] - not active

- /type/list /list?page=2 - GET PARAMS['page'] - for pagination  - not active/removed
- /type/search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination  - not active/removed
- /type/create - POST - PARAMS['store_name','transaction_master_category_id','transaction_detial_category_id','base','better_base','greenr_base','misc_other_value_base','bring_own_bag_base','walk_to_shop_base']  - not active/removed
- /type/detail/:id - GET - params['id']  - not active/removed
- /type/update/:id - POST - PARAMS['store_name','transaction_master_category_id','transaction_detial_category_id','base','better_base','greenr_base','misc_other_value_base','bring_own_bag_base','walk_to_shop_base'] - GET PARAMS['id]  - not active/removed
- /type/delete/:id - POST - params['id']  - not active/removed
- /type/past-week-activity - GET  - not active/removed
- /type/halo - GET  - not active/removed

5. Yodlee Integration - /api/v1/yodlee - must be authenticated using the x-greener-auth-token header - not active/removed
- /get/token - POST - PARAMS['user_id']   - not active/removed
- /transaction/categories - GET  - not active/removed
- /transaction - GET  - not active/removed
- /accounts - GET  - not active/removed
- /accounts/:accountId - GET - params['accountId']  - not active/removed
- /accounts/delete/:accountId - POST - params['accountId']  - not active/removed
- /register - POST - params['email','first_name','last_name']  - no middleware  - not active/removed
- /sync/transactions - Publicly accessible for cronjob purpose only  - not active/removed
- /sync/accounts - Publicly accessible for cronjob purpose only  - not active/removed
- /sync/initial-transactions - headers['x-greener-auth-token'] - call upon success bank linking  - not active/removed
- /sync/user-accounts - headers['x-greener-auth-token'] - call upon success bank linking  - not active/removed

6. Health Check - /api/v1/health
- /check - GET

7. Transaction - /api/v1/transaction - must be authenticated using the x-greener-auth-token header  - not active/removed
- /list /list?page=2 - GET PARAMS['page'] - for pagination  - not active/removed
- /detail-category/list /category-detail/list?page=2 - GET PARAMS['page'] - for pagination  - not active/removed
- /high-level-category/list /high-level-category/list?page=2 - GET PARAMS['page'] - for pagination  - not active/removed
- /master-category/list /master-category/list?page=2 - GET PARAMS['page'] - for pagination  - not active/removed

8. Code - /api/v1/code - must be authenticated using the x-greener-auth-token header
- /mobile/list /list?page=2 - GET PARAMS['page'] - for pagination
- /mobile/search?query=test&page=2 - GET PARAMS['query','page'] - query string value and pagination

9. Rating - /api/v1/rating - must be authenticated using the x-greener-auth-token header
- /halo - GET - returns 90 days and 7 days total calculation of emissions
- /top-swap - GET - returns top 3 stores for swap  - not active/removed
- /top-recommendation - GET - returns 5 stores for recommendation
- /past-week-activity - GET PARAMS['type=is_greener'] - query string is present if selected fro non greener/recommendation - return for past week activity for greener or for recommendation stores in the past 7 days

10. Basiq - /api/v1/basiq - must be authenticated using the x-greener-auth-token header
- /get/token - POST - PARAMS['user_id'] 
- /accounts - GET - no needed to provide
- /accounts/:accountId - GET - params['accountId']
- /accounts/delete/:accountId - POST - params['accountId']
- /accounts/create - POST - params['account_id','account_name','bank_code']
- /sync/transactions - Publicly accessible for cronjob purpose only
- /sync/accounts - Publicly accessible for cronjob purpose only - not active/removed
- /sync/initial-transactions - GET - params['accounts(comma-seperated)'] - headers['x-greener-auth-token'] - call upon success bank linking
- /sync/user-accounts - headers['x-greener-auth-token'] - call upon success bank linking  - not active/removed
- /sync/non-greener-stores - GET - Publicly accessible for cronjob purpose only
- /sync/price-level - GET - Publicly accessible for cronjob purpose only
- /sync/store-addresses - GET - Publicly accessible for cronjob purpose only and do not run if not needed cause it will generate cost to google places api
- /basiq/get/user - GET
- /sync/user-rating-to-hubspot - GET - Publicly accessible for cronjob purpose only

11. Banner - /api/v1/banner - must be authenticated using the x-greener-auth-token header
- /list /list?page=2 - POST - PARAMS['category(optional-filter-id)'] - for pagination 
- /search - GET - PARAMS['query'] 
- /create - POST - PARAMS['title','description] 
- /detail/:id - GET - PARAMS['id'] 
- /update/:id - POST - PARAMS['title','description]  -  GET - PARAMS['id']
- /delete/:id - POST - PARAMS['id'] 