
###################################################################################################
#### Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
####
#### Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
#### except in compliance with the License. A copy of the License is located at
####
####     http://aws.amazon.com/apache2.0/
####
#### or in the "license" file accompanying this file. This file is distributed on an "AS IS"
#### BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#### License for the specific language governing permissions and limitations under the License.
###################################################################################################

###################################################################################################
#### This configuration file shows an example of running a cron job on all linux instances in the
#### environment.
#### 
#### In the example, the script "/usr/local/bin/myscript.sh" is run from the cron file
#### "/etc/cron.d/mycron" once a minute running "date" and sending the output to "/tmp/date".
####
#### The "commands" section cleans up the backup file. ".bak" files are created when
#### "/etc/cron.d/mycron" already exists during deployment.
####
###################################################################################################

option_settings:
    - namespace: aws:elb:policies
      option_name: ConnectionSettingIdleTimeout
      value: 3600
    - namespace: aws:elbv2:loadbalancer
      option_name: IdleTimeout
      value: 3600

files:
    "/tmp/cronjob":
        mode: "000644"
        owner: root
        group: root
        content: |
            0 * * * * root /usr/local/bin/basiqjob.sh
            0 2 * * * root /usr/local/bin/transaction.sh
            ### 30 * * * * root /usr/local/bin/transaction.sh 
            0 3 * * * root /usr/local/bin/nongreener.sh
            0 4 * * * root /usr/local/bin/svgtopng.sh
            0 1 28 * * root /usr/local/bin/pricelevel.sh
            0 3 * * * root /usr/local/bin/hsrating.sh
            0 3 * * 1 root /usr/local/bin/hsweeklyrating.sh
            ### 0 1 * * * root /usr/local/bin/account.sh
            ### 0 1 * * 0 root /usr/local/bin/pricelevel.sh
            ### 0 1 28 * * root /usr/local/bin/updateaddress.sh

    "/usr/local/bin/basiqjob.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # run every hour to check for jobs in the jobs table for bank linking failed process update
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/check/job-connection > /home/ec2-user/basiqjob.log

            exit 0

    "/usr/local/bin/hsweeklyrating.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync update address every monday of the week at 3am
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/user-weekly-rating-to-hubspot > /home/ec2-user/hsweeklyrating.log

            exit 0

    "/usr/local/bin/hsrating.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync update address every 15th of the month at 1am
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/user-rating-to-hubspot > /home/ec2-user/hsrating.log

            exit 0

    "/usr/local/bin/updateaddress.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync update address every 15th of the month at 1am
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/store-addresses > /home/ec2-user/updateaddress.log

            exit 0

    "/usr/local/bin/pricelevel.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync pricelevel every week at 1am
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/price-level > /home/ec2-user/pricelevel.log

            exit 0

    "/usr/local/bin/nongreener.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync nongreener daily
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/non-greener-stores > /home/ec2-user/nongreener.log

            exit 0

    "/usr/local/bin/svgtopng.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync nongreener daily
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/convert-svg-to-png-images > /home/ec2-user/convert-svg-to-png-images.log

            exit 0

    "/usr/local/bin/transaction.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync transactions daily
            curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/transactions > /home/ec2-user/transactions.log

            exit 0

    "/usr/local/bin/account.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash

            date > /tmp/date
            # Your actual script content

            # sync accounts daily
            # curl -v -H 'x-greener-passes-token: UGFzc3dvcmQxMjM0NTY3ODkxMC4hUHJvZHVjdGlvbg==' http://localhost/api/v1/basiq/sync/accounts > /home/ec2-user/accounts.log

            exit 0

container_commands:
    enable_cron: 
        command: "mv /tmp/cronjob /etc/cron.d/cronjob"
        leader_only: true

commands:
    remove_old_cron:
        command: "rm -f /etc/cron.d/cronjob.bak"
    set_time_zone:
        command: ln -f -s /usr/share/zoneinfo/Australia/Sydney /etc/localtime